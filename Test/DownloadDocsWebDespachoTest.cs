﻿using System;
using System.Diagnostics;
using System.Net;
using DownloadDocuments;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestAutomation
{
    [TestClass]
    public class DownloadDocsWebDespachoTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            try
            {
                DespachoWeb webApp = new DespachoWeb();
                CookieContainer cookieContainer = webApp.Login();
                cookieContainer = webApp.Download(cookieContainer);
                cookieContainer = webApp.Download(cookieContainer);

            }
            catch (Exception ex)
            {

                Trace.WriteLine($"{ex.Message} - {ex.StackTrace}");
            }


        }

        [TestMethod]
        public void LoginTest()
        {
            string userName = "dcosta@lear.com";
            string password = "costa874";

            try
            {
                CookieContainer cookies = new CookieContainer();
                DespachoWeb webApp = new DespachoWeb();
                string url = "http://bank2.alpha2000.com.ar/WebUtilsLogin/Login/";
                string postData = "Usuario=" + userName + "&Password=" + password + "&ControladorRedirigir=Despachos&AccionRedirigir=Index&Proyecto=Alpha.Despachos.Web&ValidarLogin=true&Bank=false";
                string referer = "http://bank2.alpha2000.com.ar/";
                string contentType = "application/x-www-form-urlencoded; charset=UTF-8";
                string accept = "*/*";
                bool isLogged = webApp.Login2(url, postData, referer, contentType, accept, cookies);
                if (isLogged)
                {
                    url = "http://bank2.alpha2000.com.ar/Despachos/DescargarPDFDigitalizado";
                    postData = "{\"NroDespacho\":\"20001IC04006495N\",\"Familia\":2}";
                    referer = "http://bank2.alpha2000.com.ar/Despachos/Index";
                    contentType = "application/json; charset=UTF-8";
                    accept = "application/json, text/javascript, */*; q=0.01";
                    WebAppHttpResponse resp = webApp.Download2(url, postData, referer, contentType, accept, cookies);
                    
                }

            }
            catch (Exception ex)
            {
                Trace.WriteLine($"{ex.Message} - {ex.StackTrace}");
            }

        }

    }
}
