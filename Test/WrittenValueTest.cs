﻿using System;
using System.Diagnostics;
using LetterCreation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestAutomation
{
    [TestClass]
    public class WrittenValueTest
    {
        [TestMethod]
        public void TestWriteSpanish()
        {
            string value = WrittenValue.WriteValueSpanish(1456.05m);
            Debug.WriteLine(value);
            value = WrittenValue.WriteValueSpanish(989852.34m);
            Debug.WriteLine(value);
            value = WrittenValue.WriteValueSpanish(25);
            Debug.WriteLine(value);
            value = WrittenValue.WriteValueSpanish(31);
            Debug.WriteLine(value);
            value = WrittenValue.WriteValueSpanish(45236978);
            Debug.WriteLine(value);
            value = WrittenValue.WriteValueSpanish(9845236978.65m);
            Debug.WriteLine(value);
            value = WrittenValue.WriteValueSpanish(100000000);
            Debug.WriteLine(value); //ciento millones
            value = WrittenValue.WriteValueSpanish(1000000);
            Debug.WriteLine(value); //un millón
            value = WrittenValue.WriteValueSpanish(10000000);
            Debug.WriteLine(value); //diez millones
            value = WrittenValue.WriteValueSpanish(1000000000);
            Debug.WriteLine(value); //mil millones
            value = WrittenValue.WriteValueSpanish(10000000000);
            Debug.WriteLine(value); //diez mil millones
            value = WrittenValue.WriteValueSpanish(1100000000);
            Debug.WriteLine(value); //diez mil millones
            value = WrittenValue.WriteValueSpanish(1580000000000);
            Debug.WriteLine(value); //un billón quinientos ochenta mil millones
            value = WrittenValue.WriteValueSpanish(999999999);
            Debug.WriteLine(value); //nuevecientos noventa y nueve millones nuevecientos noventa y nueve mil nuevecientos noventa y nueve
            if (value != null)
            {

            }

        }
    }
}
