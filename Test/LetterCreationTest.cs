﻿using System;
using System.Diagnostics;
using LetterCreation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestAutomation
{
    [TestClass]
    public class LetterCreationTest
    {
        private Config.ConfigManager config = null;

        [TestInitialize]
        public void Initialize()
        {
            string configFile = @"..\..\..\Data\Config\config.json";
            if (config == null)
            {
                config = new Config.ConfigManager();
                config.Init(configFile);
            }
        }

        [TestMethod]
        public void TestFxPulseIsNotLinkedCompanyDocument()
        {

            FxPulse fxPulseDoc = new FxPulse(config);
            string output = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\Data\Config\FxTest.pdf";
            string template = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\Data\Config\@CODFORN_@PLANT_FX PULSE - PAGOS - DDJJ Complementaria Com A 6770 Nov 5-19 nueva - TEMPLATE - Copy.docx";
            FxData data = new FxData();
            data.Currency = "USD";
            data.Value = 125.65M;
            data.IsLinkedCompany = false;
            
            fxPulseDoc.CreateFxPulseDocument(output, data, template);

        }

        [TestMethod]
        public void TestFxPulseIsLinkedCompanyDocument()
        {

            FxPulse fxPulseDoc = new FxPulse(config);
            string output = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\Data\Config\FxTest.pdf";
            string template = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\Data\Config\@CODFORN_@PLANT_FX PULSE - PAGOS - DDJJ Complementaria Com A 6770 Nov 5-19 nueva - TEMPLATE - Copy.docx";
            FxData data = new FxData();
            data.Currency = "USD";
            data.Value = 125.65M;
            data.IsLinkedCompany = true;

            fxPulseDoc.CreateFxPulseDocument(output, data, template);

        }

        [TestMethod]
        public void TestDeclaracionJuraQtyDocuments()
        {

            Initialize();

            int qty = 1;
            DeclaracionJurada declaracion = new DeclaracionJurada(config);
            int result = declaracion.GetQtyDocuments(qty);

            qty = 17;
            result = declaracion.GetQtyDocuments(qty);
            Trace.WriteLine(result);

            qty = 18;
            result = declaracion.GetQtyDocuments(qty);
            Trace.WriteLine(result);

            qty = 34;
            result = declaracion.GetQtyDocuments(qty);
            Trace.WriteLine(result);

            qty = 38;
            result = declaracion.GetQtyDocuments(qty);
            Trace.WriteLine(result);

            qty = 68;
            result = declaracion.GetQtyDocuments(qty);
            Trace.WriteLine(result);

            qty = 70;
            result = declaracion.GetQtyDocuments(qty);
            Trace.WriteLine(result);

        }

    }
}
