﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AzureServices;
using System.Diagnostics;
using Lear.RpaMS.Model.ModelLayer.CloudServices;
using Lear.RpaMS.Data.DataServiceLayer.CloudServices;

namespace TestAutomation
{
    [TestClass]
    public class ComputerVisionClientTest
    {

        private Config.ConfigManager config = null;

        private void Initialize()
        {
            string configFile = @"..\..\..\Data\Config\config.json";
            config = new Config.ConfigManager();
            config.Init(configFile);

            Lear.RpaMS.AppLayer.AppSettings.Instance.ConnectionString = config.Get().connectionString;
        }

                     
        [TestMethod]
        public void TestRecognizeText()
        {

            try
            {
                this.Initialize();

                string inputFile = @"C:\RPA\Dev\#Projects\AutorizacaoPagamentoFaturaArgentina\Data\#Download\ESCOBAR\800025\19073IC04149369P\Temp\19073IC04149369P_Page_5.jpg";
                //ComputerVisionClient computerVisionClient = new ComputerVisionClient(this.config);
                //RecognitionTextResult result = computerVisionClient.RecognizeText(inputFile);
                //string[] lines = RecognizeTextHelper.GetGetRecognizeTextResultLinesText(result);
                //if (lines != null && lines.Length > 0)
                //{
                //    string text = String.Join("-", lines);
                //    Trace.WriteLine(text);
                //}

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message + " - " + ex.StackTrace);
            }
        }


        public void TestRecognizeTextError()
        {
            try
            {
                this.Initialize();


            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message + " - " + ex.StackTrace);
            }
        }

        [TestMethod]
        public void TestReadText()
        {

            try
            {
                this.Initialize();

                string inputFile = @"C:\RPA\Dev\#Projects\01 - AutorizacaoPagamentoFaturaArgentina\Data\#Download\20001IC04009301C\Temp\20001IC04009301C_Page_7.jpg";
                ServiceResource azureService = ServiceResourceService.LoadByCode("AzureReadTextFREIF");
                ComputerVisionClient computerVisionClient = new ComputerVisionClient(azureService);
                ReadTextResult result = computerVisionClient.ReadText(inputFile);
                string[] lines = RecognizeTextHelper.GetReadAnalyzeResultLinesText(result);
                if (lines != null && lines.Length > 0)
                {
                    string text = String.Join("-", lines);
                    Trace.WriteLine(text);
                }

            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.Message + " - " + ex.StackTrace);
            }
        }



        //        {StatusCode: 202, ReasonPhrase: 'Accepted', Version: 1.1, Content: System.Net.Http.StreamContent, Headers:
        //{
        //  Pragma: no-cache
        //  Operation-Location: https://freif-ai-vision.cognitiveservices.azure.com/vision/v2.0/textOperations/be2bc7bf-cc65-4355-9838-b9672df8a6bd
        //  apim-request-id: a7730fe3-36ff-4516-9164-0dbcd7dca62b
        //  Strict-Transport-Security: max-age=31536000; includeSubDomains; preload
        //  x-content-type-options: nosniff
        //  Cache-Control: no-cache
        //  Date: Thu, 06 Feb 2020 13:53:26 GMT
        //  X-AspNet-Version: 4.0.30319
        //  X-Powered-By: ASP.NET
        //  Content-Length: 0
        //  Expires: -1

        //    }
        //}


    }
}