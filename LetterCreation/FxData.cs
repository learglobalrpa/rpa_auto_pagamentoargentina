﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterCreation
{
    public class FxData
    {
        public string Currency { get; set; }
        public decimal Value { get; set; }
        public bool IsLinkedCompany { get; set; }
        public string ImportsOption { get; set; }
    }
}
