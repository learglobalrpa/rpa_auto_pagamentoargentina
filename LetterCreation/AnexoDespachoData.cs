﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterCreation
{
    public class AnexoDespachoData
    {
        public string Currency { get; set; }
        public decimal ValorTotal { get; set; }

        public List<AnexoDespachoDataItem> items { get; set; }

        public class AnexoDespachoDataItem
        {
            public string Despacho { get; set; }
            public string DataFechamento { get; set; }
            public decimal Valor { get; set; }
        }

    }



}
