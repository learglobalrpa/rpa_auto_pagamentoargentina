﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace LetterCreation
{
    public class AnexoDespacho
    {

        private Config.ConfigManager config = null;
        private const int TOTAL_ROWS = 1;
        private const int START_ROWS = 7;

        public AnexoDespacho(Config.ConfigManager config)
        {
            this.config = config;
        }

        public string[] CreateAnexoDespachoWorksheet(string output, AnexoDespachoData data, string template = null)
        {
            int qtyDocs = this.GetQtyDocuments(data.items.Count);
            string[] filesCreated = new string[qtyDocs];

            int currentItem = 0;
            string templateFile = template;

            for (int indDoc = 0; indDoc < qtyDocs; indDoc++)
            {
                Excel.Application excelApp = new Excel.Application();
                Excel.Workbook excelBook = excelApp.Workbooks.Open(templateFile);
                excelApp.DisplayAlerts = false;
                //excelBook.Activate();
                Excel.Worksheet excelSheet = excelBook.Sheets[1];
                                
                //Monto Operación
                string valorTotal = excelSheet.Cells[3, 1].Value2;
                valorTotal = valorTotal.Replace("<VLTOTAL>", $"{data.Currency} {data.ValorTotal.ToString()}");
                excelSheet.Cells[3, 1].Value2 = valorTotal;

                int numberOfItems = data.items.Count();
                for (int i = 0; i < TOTAL_ROWS; i++)
                {
                    string despacho = "";
                    string dataFechamento = "";
                    string valor = "";
                    if ((currentItem + 1) <= numberOfItems)
                    {
                        despacho = data.items[currentItem].Despacho;
                        if (!string.IsNullOrEmpty(data.items[currentItem].DataFechamento))
                        {
                            DateTime dataEmbarque = DateTime.Parse(data.items[currentItem].DataFechamento);
                            dataFechamento = dataEmbarque.ToString("dd/MM/yyyy");
                        } else
                        {
                            dataFechamento = data.items[currentItem].DataFechamento;
                        }

                        valor = data.items[currentItem].Valor.ToString();
                    }

                    excelSheet.Cells[START_ROWS + i, 2].Value2 = despacho;
                    Excel.Range rg = (Excel.Range)excelSheet.Cells[START_ROWS + i, 3];
                    rg.NumberFormat = "@";                    
                    rg.Value2 = dataFechamento;
                    excelSheet.Cells[START_ROWS + i, 4].Value2 = valor;
                    
                    currentItem++;
                }
                
                excelBook.ExportAsFixedFormat(Excel.XlFixedFormatType.xlTypePDF, output);
                excelApp.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApp);

                filesCreated[indDoc] = output;
            }

            return filesCreated;
        }


        public int GetQtyDocuments(int totalItems)
        {
            int result;
            int quotient = Math.DivRem(totalItems, TOTAL_ROWS, out result);
            if (result == 0)
            {
                return quotient;
            }
            return quotient + 1;
        }

    }
}
