﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterCreation
{
    /// <summary>
    /// https://ivanmeirelles.wordpress.com/2012/10/27/escrever-valores-por-extenso-em-c/#:~:text=Por%20exemplo%3A%20R%24%20140%2C,.999.999.999%2C99.
    /// https://www.languagesandnumbers.com/como-contar-em-espanhol/pt/spa/
    /// </summary>
    public class WrittenValue
    {

        public static string WriteValueSpanish(decimal value)
        {
            if (value <= 0 | value >= 1000000000000000)
                return "Valor não suportado pelo sistema.";
            else
            {
                string strValor = value.ToString("000000000000000.00");
                string valor_por_extenso = string.Empty;

                for (int i = 0; i <= 15; i += 3)
                {
                    valor_por_extenso += WritePart(Convert.ToDecimal(strValor.Substring(i, 3)));

                    // Rules for plural values
                    // ###########################################################################################
                    if (valor_por_extenso.Contains("uno mil"))
                    {
                        valor_por_extenso = valor_por_extenso.Replace("uno mil", "mil");
                    }
                    if (valor_por_extenso.Contains("cien millones"))
                    {
                        valor_por_extenso = valor_por_extenso.Replace("cien millones", "ciento millones");
                    }
                    if (valor_por_extenso.Contains("uno un millón"))
                    {
                        valor_por_extenso = valor_por_extenso.Replace("uno un millón", "un millón");
                    }
                    if (valor_por_extenso.Contains("uno mil millones"))
                    {
                        valor_por_extenso = valor_por_extenso.Replace("uno mil millones", "mil millones");
                    }
                    // ###########################################################################################

                    // TRILHAO
                    if (i == 0 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(0, 3)) == 1)
                            valor_por_extenso += " trillón" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty);
                        else if (Convert.ToInt32(strValor.Substring(0, 3)) > 1)
                            valor_por_extenso += " trillones" + ((Convert.ToDecimal(strValor.Substring(3, 12)) > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty);
                    }
                    // BILHAO
                    else if (i == 3 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(3, 3)) == 1)
                            valor_por_extenso += " mil millones ";
                        else if (Convert.ToInt32(strValor.Substring(3, 3)) > 1)
                            valor_por_extenso += " billones ";
                    }
                    // MILHAO
                    else if (i == 6 & valor_por_extenso != string.Empty)
                    {
                        if (Convert.ToInt32(strValor.Substring(6, 3)) == 1)                            
                            valor_por_extenso += " un millón ";
                        else if (Convert.ToInt32(strValor.Substring(6, 3)) > 1)
                            valor_por_extenso += " millones ";
                    }
                    else if (i == 9 & valor_por_extenso != string.Empty)
                        if (Convert.ToInt32(strValor.Substring(9, 3)) > 0)
                            valor_por_extenso += " mil ";

                    if (i == 12)
                    {
                        //if (valor_por_extenso.Length > 8)
                        //    if (valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "mil millones" | valor_por_extenso.Substring(valor_por_extenso.Length - 6, 6) == "millón")
                        //        valor_por_extenso += " ";
                        //    else
                        //        if (valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "billones" | valor_por_extenso.Substring(valor_por_extenso.Length - 7, 7) == "millones" | valor_por_extenso.Substring(valor_por_extenso.Length - 8, 7) == "trillones")
                        //        valor_por_extenso += " ";
                        //    else
                        //            if (valor_por_extenso.Substring(valor_por_extenso.Length - 8, 8) == "trillones")
                        //        valor_por_extenso += " ";

                        if (Convert.ToInt64(strValor.Substring(0, 15)) == 1)
                            valor_por_extenso += " real";
                        else if (Convert.ToInt64(strValor.Substring(0, 15)) > 1)
                            valor_por_extenso += " reales";

                        if (Convert.ToInt32(strValor.Substring(16, 2)) > 0 && valor_por_extenso != string.Empty)
                            valor_por_extenso += " con ";
                    }

                    if (i == 15)
                        if (Convert.ToInt32(strValor.Substring(16, 2)) == 1)
                            valor_por_extenso += " centavo";
                        else if (Convert.ToInt32(strValor.Substring(16, 2)) > 1)
                            valor_por_extenso += " centavos";
                }
                return valor_por_extenso.Replace("  ", " ");
            }
        }

        private static string WritePart(decimal value)
        {
            if (value <= 0)
                return string.Empty;
            else
            {
                string montagem = string.Empty;
                if (value > 0 & value < 1)
                {
                    value *= 100;
                }
                string strValor = value.ToString("000");
                int a = Convert.ToInt32(strValor.Substring(0, 1));
                int b = Convert.ToInt32(strValor.Substring(1, 1));
                int c = Convert.ToInt32(strValor.Substring(2, 1));

                if (a == 1) montagem += (b + c == 0) ? "cien" : "ciento";
                else if (a == 2) montagem += "doscientos";
                else if (a == 3) montagem += "trescientos";
                else if (a == 4) montagem += "cuatrocientos";
                else if (a == 5) montagem += "quinientos";
                else if (a == 6) montagem += "seiscientos";
                else if (a == 7) montagem += "setecientos";
                else if (a == 8) montagem += "ochocientos";
                else if (a == 9) montagem += "nuevecientos";

                if (b == 1)
                {
                    if (c == 0) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "diez";
                    else if (c == 1) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "once";
                    else if (c == 2) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "doce";
                    else if (c == 3) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "trece";
                    else if (c == 4) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "catorce";
                    else if (c == 5) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "quince";
                    else if (c == 6) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "dieciséis";
                    else if (c == 7) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "diecisiete";
                    else if (c == 8) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "dieciocho";
                    else if (c == 9) montagem += ((a > 0) ? " " + SpanishHelper.GetAnd() + " " : string.Empty) + "diecinueve";
                }
                else if (b == 2)
                {
                    if (c > 0)
                    {
                        montagem += " veinti";
                    }
                    else
                    {
                        montagem += " veinte";
                    }
                }
                else if (b == 3) montagem += " treinta";
                else if (b == 4) montagem += " cuarenta";
                else if (b == 5) montagem += " cincuenta";
                else if (b == 6) montagem += " sesenta";
                else if (b == 7) montagem += " setenta";
                else if (b == 8) montagem += " ochenta";
                else if (b == 9) montagem += " noventa";

                if (strValor.Substring(1, 1) != "1" & c != 0 & montagem != string.Empty & b != 2) montagem += " " + SpanishHelper.GetAnd() + " ";

                if (strValor.Substring(1, 1) != "1")
                    if (c == 1)
                    {
                        if (b == 2)
                        {
                            montagem += "ún";
                        }
                        else
                        {
                            montagem += "uno";
                        }
                    }
                    else if (c == 2)
                    {
                        if (b == 2)
                        {
                            montagem += "dós";
                        }
                        else
                        {
                            montagem += "dos";
                        }
                    }
                    else if (c == 3)
                    {
                        if (b == 2)
                        {
                            montagem += "trés";
                        }
                        else
                        {
                            montagem += "tres";
                        }
                    }
                    else if (c == 4) montagem += "cuatro";
                    else if (c == 5) montagem += "cinco";
                    else if (c == 6)
                    {
                        if (b == 2)
                        {
                            montagem += "séis";
                        } else
                        {
                            montagem += "seis";
                        }
                    }
                    else if (c == 7) montagem += "siete";
                    else if (c == 8) montagem += "ocho";
                    else if (c == 9) montagem += "nueve";

                return montagem;
            }

        }
    }
}
