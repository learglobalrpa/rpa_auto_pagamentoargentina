﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace LetterCreation
{
    public class DeclaracionJurada
    {

        private Config.ConfigManager config = null;
        private const int TOTAL_ROWS = 17;

        public DeclaracionJurada(Config.ConfigManager config)
        {
            this.config = config;
        }

        public string[] CreateDeclarationDocument(string output, DeclarationData data, string template = null)
        {
            int qtyDocs = this.GetQtyDocuments(data.items.Count);
            string[] filesCreated = new string[qtyDocs];

            int currentItem = 0;

            object templateFile = template;
            //if (String.IsNullOrEmpty(template))
            //{
            //    templateFile = Path.Combine(config.Get().mainFolder, config.Get().configFolder, config.Get().outputDocuments.declaracionJuradaTemplate);
            //} else
            //{
            //    templateFile = template;
            //}

            for (int indDoc = 0; indDoc < qtyDocs; indDoc++)
            {
                Word.Application wordApp = new Word.Application();
                wordApp.Visible = false;
                Word.Document doc = null;
                doc = wordApp.Documents.Open(ref templateFile);
                doc.Activate();

                //Find and replace
                WordManipulation.FindAndReplace(wordApp, "<DAY>", DateTime.Today.Day);
                WordManipulation.FindAndReplace(wordApp, "<MONTH>", SpanishHelper.GetMonthName(DateTime.Today.Month));
                WordManipulation.FindAndReplace(wordApp, "<SHORTYEAR>", DateTime.Today.Year.ToString().Substring(2, 2));
                WordManipulation.FindAndReplace(wordApp, "<VLTOTAL>", data.ValorTotal);

                int numberOfItems = data.items.Count();
                for (int i = 0; i < TOTAL_ROWS; i++)
                {
                    string despacho = "";
                    string dataFechamento = "";
                    string valor = "";
                    if ((currentItem + 1) <= numberOfItems)
                    {
                        despacho = data.items[currentItem].Despacho;
                        dataFechamento = data.items[currentItem].DataFechamento;
                        valor = data.items[currentItem].Valor.ToString();
                    }

                    WordManipulation.FindAndReplace(wordApp, $"<DEPACHO{i+1}>", despacho);
                    WordManipulation.FindAndReplace(wordApp, $"<DATA{i+1}>", dataFechamento);
                    WordManipulation.FindAndReplace(wordApp, $"<VALOR{i+1}>", valor);

                    currentItem++;
                }

                if (indDoc > 0)
                {

                }

                doc.SaveAs2(output, Word.WdSaveFormat.wdFormatPDF);
                wordApp.Quit(Word.WdSaveOptions.wdDoNotSaveChanges);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(wordApp);

                filesCreated[indDoc] = output;
            }

            return filesCreated;
        }


        public int GetQtyDocuments(int totalItems)
        {
            int result;
            int quotient = Math.DivRem(totalItems, TOTAL_ROWS, out result);
            if (result == 0)
            {
                return quotient;
            } 
            return quotient + 1;                    
        }

    }
}
