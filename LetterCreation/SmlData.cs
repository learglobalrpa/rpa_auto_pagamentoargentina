﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterCreation
{
    public class SmlData
    {

        public string Currency { get; set; }
        public decimal Value { get; set; }
        public bool IsLinkedCompany { get; set; }
        public string ImportsOption { get; set; }

        public string SupplierName { get; set; }
        public string SupplierNRLegalEntity { get; set; }
        public string SupplierBankInformation { get; set; }
        public string SupplierBankAgency { get; set; }
        public string SupplierBankAccount { get; set; }


    }
}
