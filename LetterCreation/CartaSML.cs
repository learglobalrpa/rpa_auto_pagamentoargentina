﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace LetterCreation
{
    public class CartaSML
    {

        public void CreateSmlDocument(string output, SmlData data, string template = null)
        {
            Word.Application wordApp = null;
            Word.Document doc = null;

            try
            {
                object templateFile = template;
                wordApp = new Word.Application();
                wordApp.Visible = false;
                doc = wordApp.Documents.Open(ref templateFile);

                string chkFieldName = "chkNoTiene";
                if (data.IsLinkedCompany)
                {
                    chkFieldName = "chkSiTiene";
                }

                string chkImportsOptionsField = data.ImportsOption;

                int controls = doc.ContentControls.Count;
                for (int i = 1; i <= controls; i++)
                {
                    var control = doc.ContentControls[i];
                    if (control.Type == Word.WdContentControlType.wdContentControlCheckBox)
                    {
                        if (control.Tag != null)
                        {
                            if (control.Tag.Equals(chkFieldName) ||
                                control.Tag.Equals(chkImportsOptionsField))
                            {
                                control.Checked = true;

                            }
                        }
                    }
                }

                //Find and replace
                WordManipulation.FindAndReplace(wordApp, "<DAY>", DateTime.Today.Day);
                WordManipulation.FindAndReplace(wordApp, "<MONTH>", SpanishHelper.GetMonthName(DateTime.Today.Month));
                WordManipulation.FindAndReplace(wordApp, "<SHORTYEAR>", DateTime.Today.Year.ToString().Substring(2, 2));
                WordManipulation.FindAndReplace(wordApp, "<CURRENCY>", data.Currency);
                WordManipulation.FindAndReplace(wordApp, "<AMOUNT>", data.Value.ToString("F", CultureInfo.CreateSpecificCulture("es-ES")));
                WordManipulation.FindAndReplace(wordApp, "<WRITE_AMOUNT>", WrittenValue.WriteValueSpanish(data.Value));

                //Supplier data
                WordManipulation.FindAndReplace(wordApp, "<SUPPLIER_NAME>", data.SupplierName);
                WordManipulation.FindAndReplace(wordApp, "<SUPPLIER_NRLegalEntity>", data.SupplierNRLegalEntity);
                WordManipulation.FindAndReplace(wordApp, "<SUPPLIER_BANK_INFORMATION>", data.SupplierBankInformation);
                WordManipulation.FindAndReplace(wordApp, "<SUPPLIER_BANK_AGENCY>", data.SupplierBankAgency);
                WordManipulation.FindAndReplace(wordApp, "<SUPPLIER_BANK_ACCOUNT>", data.SupplierBankAccount);

                doc.SaveAs2(output, Word.WdSaveFormat.wdFormatPDF);
            }
            finally
            {
                if (wordApp != null)
                {
                    wordApp.Quit(Word.WdSaveOptions.wdDoNotSaveChanges);
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(wordApp);
                }
                if (doc != null)
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(doc);
                }
            }

        }

    }
}
