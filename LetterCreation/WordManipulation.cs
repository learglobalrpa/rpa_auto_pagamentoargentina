﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace LetterCreation
{
    class WordManipulation
    {

        public static void FindAndReplace(Word.Application workApp, object textToFind, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildcards = false;
            object matchSoundsLike = false;
            object matchAllWordForms = false;
            object forward = true;
            object wrap = 1;
            object format = false;
            object replace = 2;
            object matchKashida = false;
            object matchDiacritics = false;
            object matchAlefHamza = false;
            object matchControl = false;

            workApp.Selection.Find.Execute(ref textToFind, 
                                            ref matchCase, 
                                            ref matchWholeWord, 
                                            ref matchWildcards, 
                                            ref matchSoundsLike, 
                                            ref matchAllWordForms, 
                                            ref forward,
                                            ref wrap,
                                            ref format, 
                                            ref replaceWithText, 
                                            ref replace,
                                            ref matchKashida, 
                                            ref matchDiacritics,
                                            ref matchAlefHamza, 
                                            ref matchControl);
        }

    }
}
