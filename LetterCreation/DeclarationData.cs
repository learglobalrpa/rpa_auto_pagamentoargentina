﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LetterCreation
{
    public class DeclarationData
    {

        public decimal ValorTotal { get; set; }

        public List<DeclarationDataItem> items { get; set; }

        public class DeclarationDataItem
        {
            public string Despacho { get; set; }
            public string DataFechamento { get; set; }
            public decimal Valor { get; set; }
        }

    }



}
