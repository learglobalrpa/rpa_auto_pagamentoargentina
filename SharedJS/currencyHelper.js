const currency = require("currency.js");

class CurrencyHelper {

    static getValueARS(value) {
        return currency(value, { separator: ".", decimal: "," }).format();
    }

}

module.exports = CurrencyHelper;