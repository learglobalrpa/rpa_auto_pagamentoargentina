const moment = require("moment");

class MomentHelper {

    static getDateAndTime() {
        moment.locale("pt-br");
        return moment().format('L') + " " + moment().format('LTS')
    }

    static getDateAsFolderName() {
        moment.locale("pt-br");
        return moment().format("YYYYMMDD");
    }


    static convertDate(dateStr, formatSource, formatTarget) {
        let newDate = moment(dateStr, formatSource);
        return newDate.format(formatTarget);
    }

    static subtractDays(_daysNo) {
        const date = moment().subtract(_daysNo, 'days');
        return date;
    }

    static addDays(_daysNo) {
        const date = moment().add(_daysNo, 'days');
        return date;
    }

    static formatDate(_date, _format) {
        return _date.format(_format);
    }

}

module.exports = MomentHelper;