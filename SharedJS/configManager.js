const fs = require('fs');
const configFile = "../Data/Config/config.json";

var config = null;

class ConfigManager {


  init(_configFile) {
    let rawdata = null;
    var encoding = {
      encoding: 'utf8'
    };
    if (_configFile) {
      rawdata = fs.readFileSync(_configFile);
      //rawdata = fs.readFileSync(_configFile, encoding);
    } else {
      //rawdata = fs.readFileSync(configFile, encoding);
      rawdata = fs.readFileSync(configFile);
    }
    config = JSON.parse(rawdata);
  }

  get(property) {

    const getImpl = function (object, property) {
      let t = this,
        elems = Array.isArray(property) ? property : property.split('.'),
        name = elems[0],
        value = object[name];
      if (elems.length <= 1) {
        return value;
      }
      // Note that typeof null === 'object'
      if (value === null || typeof value !== 'object') {
        return undefined;
      }
      return getImpl(value, elems.slice(1));
    };

    return getImpl(config, property);
  }

  getInputFileName() {
    const mainFolder = this.get("mainFolder");
    const inputFolder = this.get("inputFolder");
    const inputFile = this.get("excelLiberados");
    return mainFolder + "\\" + inputFolder + "\\" + inputFile;
  }

  getOutputFolder() {
    const outputFolder = this.get("outputFolder");
    return "..\\" + outputFolder;
  }

  getDownloadDocumentsFolder() {
    const downloadFolder = this.get("downloadDocumentsFolder");
    return "..\\" + downloadFolder;
  }

}

module.exports = ConfigManager;