const sql = require('mssql');

let config = null;

class ProjectDB {

    init(configManager) {
        config = configManager;
    }

    async loadByCode(code) {
        let query = "SELECT ID, Code, GitSourceUrl, MainPath, MaxRetryNumber ";
        query += "FROM RPA_Project ";
        query += "WHERE Code = @Code";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('Code', sql.VarChar, code);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset[0];
        }
        return null;
    }

    async loadParameters(projectID) {
        let query = "SELECT ID, ProjectID, Name, Value ";
        query += "FROM RPA_ProjectParameter ";
        query += "WHERE ProjectID = @ProjectID";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('ProjectID', sql.Int, projectID);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset;
        }
        return null;
    }


}

module.exports = ProjectDB;