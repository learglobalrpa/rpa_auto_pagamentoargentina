const os = require('os');
const sql = require('mssql');

let config = null;

class AppSettingsDB {

    init(configManager) {
        config = configManager;
    }

    async loadByCode(code) {
        let query = "SELECT ID, AppTypeID, Code, Name, IsHeadless, Url, MainPath, StartCommand ";
        query += "FROM RPA_AppSettings ";
        query += "WHERE Code = @Code";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('Code', sql.VarChar, code);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset[0];
        }
        return null;
    }

    async loadParameters(appSettingsID) {
        let query = "SELECT ID, AppSettingsID, Name, Value ";
        query += "FROM RPA_AppSettingsParameter ";
        query += "WHERE AppSettingsID = @AppSettingsID";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('AppSettingsID', sql.Int, appSettingsID);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset;
        }
        return null;
    }

    async getCredential(appSettingsID) {
        let query = "SELECT ID, AppSettingsID, UserCode, UserID, UserPwd ";
        query += "FROM RPA_AppSettingsCredential ";
        query += "WHERE AppSettingsID = @AppSettingsID AND UserCode = @UserCode";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('AppSettingsID', sql.Int, appSettingsID);
        request.input('UserCode', sql.VarChar, os.userInfo().username);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset[0];
        }
        return null;
    }


}

module.exports = AppSettingsDB;