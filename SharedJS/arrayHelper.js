

class ArrayHelper {

    static deleteItemByValue(array, value) {
        for (let index = 0; index < array.length; index++) {
            const element = array[index];
            if (element == value) {
                array.splice(index, 1);
            }            
        }
    }

}

module.exports = ArrayHelper;