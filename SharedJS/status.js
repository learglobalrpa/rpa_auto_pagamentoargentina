class Status {

    static get NotFound() {
        return "Nao encontrado para download";
    }

    static get NotFoundID() {
        return 1;
    }
    
    static get DownloadOk() {
        return  "OK";
    }

    static get DownloadOkID() {
        return  2;
    }

    static get MoreThanOneDocument() {
        return  "Erro: Mais de um documento encontrado";
    }

    static get MoreThanOneDocumentID() {
        return  3;
    }

    static get NotFoundInvoice() {
        return  "Erro: Invoice nao encontrada no arquivo Familia 2";
    }

    static get NotFoundInvoiceID() {
        return 4;
    }

    static get SentToBankOk() {
        return  "Enviado para banco com sucesso";
    }

    static get SentToBankOkID() {
        return 7;
    }

    static get SentToBankError() {
        return  "Erro: Não foi possível enviar documento para banco";
    }

    static get SentToBankErrorID() {
        return 8;
    }

}

module.exports = Status;