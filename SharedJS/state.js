class State {

    static get WaitingShipmmentNo() {
        return "Aguardando numero do despacho";
    } 

    static get WaitingShipmmentNoID() {
        return 1;
    } 

    static get WaitingDocumentDownload() {
        return  "Aguardando download";
    }

    static get WaitingDocumentDownloadID() {
        return 2;
    }

    static get DownloadOk() {
        return "Download realizado com sucesso";
    }

    static get DownloadOkID() {
        return 3;
    }

    static get CheckManually() {
        return "Verificar Manualmente";
    }

    static get CheckManuallyID() {
        return 4;
    }

    static get WaitingBankDocument() {
        return "Aguardando documento para banco";
    }

    static get WaitingBankDocumentID() {
        return 5;
    }

    static get DocumentCreated() {
        return  "Documento gerado com sucesso";
    }

    static get DocumentCreatedID() {
        return  6;
    }
    
    static get DocumentSentToBank() {
        return  "Documento enviado para banco com sucesso";
    }

    static get DocumentSentToBankID() {
        return  7;
    }

    static get WaitingDocumentSentToBank() {
        return  "Aguardando envio do documento para o banco";
    }

    static get WaitingDocumentSentToBankID() {
        return  8;
    }

}

module.exports = State;