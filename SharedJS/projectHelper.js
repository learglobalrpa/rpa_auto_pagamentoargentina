class ProjectHelper {

    static getParameterByName(parameters, paramName) {
        if (parameters && paramName) {            
            const parameter = parameters.find(element => element.Name == paramName);
            return parameter;
        }
        return null;
    }

}

module.exports = ProjectHelper;