'use strict';

const ConfigManager = require("../../SharedJS/configManager");
const MomentHelper = require("../../SharedJS/momentHelper");
const ComexWebApp = require("./comexWebApp");
const AppSettingsDB = require('../../SharedJS/db/appSettingsDB');
const ProjectDB = require('../../SharedJS/db/projectDB');

let config = null;
let comexWeb = null;
let despachoWebApp = null;
let despachoWebCredential = null;
let project = null;

const initAppSettings = async() => {
    const appSettingsDB = new AppSettingsDB();
    appSettingsDB.init(config);
    despachoWebApp = await appSettingsDB.loadByCode("CitiFxWebAppArgentina");
    despachoWebApp.Parameters = await appSettingsDB.loadParameters(despachoWebApp.ID);
    despachoWebCredential = await appSettingsDB.getCredential(despachoWebApp.ID);
}

const initProject = async() => {
    const projectDB = new ProjectDB();
    projectDB.init(config);
    const projectCode = config.get("projectCode");
    project = await projectDB.loadByCode(projectCode);
    project.Parameters = await projectDB.loadParameters(project.ID);
}

const initComexWeb = async () => {
    comexWeb = new ComexWebApp();
    comexWeb.setAppSettings(despachoWebApp);
    comexWeb.setAppCredential(despachoWebCredential);
    comexWeb.setProject(project);
    await comexWeb.init(config);
    await comexWeb.login();
}

const initSearchStatusBank = async () => {
    await comexWeb.goToStatusBank();
};

(async() => {

    try {
        console.log(MomentHelper.getDateAndTime());

        config = new ConfigManager();
        config.init("../../Data/Config/config.json");

        const args = process.argv.slice(2);
        const outputFileJson = args[0];
        if (!outputFileJson) {
            console.log("Argument not found 'OutputFileJson'");
            return;
        }

        await initAppSettings();
        console.log("App Settings loaded...");
        await initProject();
        console.log("Project [" + config.get("projectCode") + "] loaded...");
        await initComexWeb();
        await initSearchStatusBank();
        await comexWeb.setDateAndSearch();
        await comexWeb.downloadReport();
        await comexWeb.extractSearchResults(outputFileJson);
        //Wait a while to complete the download
        await new Promise(resolve => setTimeout(resolve, 1000 * 20));

        if (comexWeb != null) {
            comexWeb.close();
        }
        
        console.log("Process finished!!")

    } catch (error) {
        console.log("Error: " + error.message + " - " + error.stack);
        if (comexWeb != null) {
            comexWeb.close();
        }
    }

})();