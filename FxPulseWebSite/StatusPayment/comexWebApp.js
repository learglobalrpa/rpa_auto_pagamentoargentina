const fs = require('fs');
const puppeteer = require("../../node_modules/puppeteer");
const AppSettingsHelper = require("./../../SharedJS/appSettingsHelper");
const ProjectHelper = require("./../../SharedJS/projectHelper");
const MomentHelper = require("./../../SharedJS/momentHelper");

let browser = null;
let page = null;
let config = null;
let appSettings = null;
let appSettingsCredential = null;
let project = null;

class ComexWebApp {

    setBrowser(_browser) {
        browser = _browser;
    }

    setPage(_page) {
        page = _page;
    }

    setConfig(_config) {
        config = _config;
    }

    setAppSettings(despachoWebApp) {
        appSettings = despachoWebApp;
    }

    setAppCredential(despachoWebCredential) {
        appSettingsCredential = despachoWebCredential;
    }

    setProject(_project) {
        project = _project;
    }

    async init(configManager) {
        config = configManager;
        browser = await puppeteer.launch({ headless: appSettings.IsHeadless, args: ['--start-maximized'] });
        page = await browser.newPage();
        await page.setViewport({ width: 1366, height: 768 });
        const url = appSettings.Url;
        await page.goto(url, { waitUntil: 'load', timeout: 0 });
        page.setDefaultNavigationTimeout(5400000);
    }

    async login() {
        const user = appSettingsCredential.UserID;
        const pwd = appSettingsCredential.UserPwd;
        await page.type('#userid', user);
        await page.type('#viewPass', pwd);
        await page.click('#buttonSave');

        await page.waitForNavigation();
        await new Promise(resolve => setTimeout(resolve, 2000));

        await this.goToComexCiti();
        await this.acceptAfterLogin();

        await this.selectCompany();
    }

    async goToComexCiti() {
        const parameter = AppSettingsHelper.getParameterByName(appSettings.Parameters, "UrlComex");
        const url = parameter.Value;
        await page.goto(url, { waitUntil: 'load', timeout: 0 });
    }

    async acceptAfterLogin() {
        await new Promise(resolve => setTimeout(resolve, 1000 * 10));
        //Find and click button Accept
        var btnAccept = await page.$("#disclaimerForm\\:btnContinuar");
        if (btnAccept != null) {
            await btnAccept.evaluate((element) => {
                element.click();
            });
        }
        await page.waitForNavigation();
    }

    async selectCompany() {
        const parameter = AppSettingsHelper.getParameterByName(appSettings.Parameters, "CompanyCode");
        const company = parameter.Value;

        await new Promise(resolve => setTimeout(resolve, 1000 * 2));
        await page.evaluate((company) => {
            let ele = document.querySelector("#empresaForm\\:empresaSelectorInput");
            if (ele) {
                ele.value = company;
            }
            ele = document.querySelector("#empresaForm\\:empresaSelectorselValue");
            if (ele) {
                ele.autocomplete = company;
                let companyValues = company.split("-");
                ele.value = companyValues[0].trim();
            }
        }, company);

        await page.focus("#empresaForm\\:empresaSelectorInput");
        await page.click("#empresaForm\\:empresaSelectorInput");
        await page.keyboard.press('Enter');
        await new Promise(resolve => setTimeout(resolve, 1000 * 10));        
    }

    async goToStatusBank() {
        await new Promise(resolve => setTimeout(resolve, 1000 * 3));
        let selectorInput = "#menuForm\\:tdu-super-maker-online-estadoOperacionesBanco > span.rf-ddm-itm-lbl";
        await page.evaluate((_selectorInput) => {
            let ele = document.querySelector(_selectorInput);
            if (ele) {
                ele.click();
            }
        }, selectorInput);
        await page.waitForNavigation();
        await new Promise(resolve => setTimeout(resolve, 1000 * 8));
    }

    async setDateAndSearch() {
        console.log("Typing date and search..");
        const parameter = ProjectHelper.getParameterByName(project.Parameters, "FxPulseDayToSearch");
        const daysNo = parameter.Value;
        let searchDate = MomentHelper.subtractDays(daysNo);
        await page.type("#searchForm\\:calendarFromInputDate", MomentHelper.formatDate(searchDate, "L"));
        await page.keyboard.press('Enter');
        console.log("Waiting for navigation..");
        await page.waitForSelector("#listItemsForm\\:generarReporteExcel", {
            visible: true
        });
        await new Promise(resolve => setTimeout(resolve, 1000 * 5));
        console.log("Navigation finished..");
    }    

    async setSelectField(selectorInput, selectorselValue, value) {
        await page.evaluate((_selectorInput, _selectorselValue, _value) => {
            let ele = document.querySelector(_selectorInput);
            if (ele) {
                ele.value = _value;
            }
            ele = document.querySelector(_selectorselValue);
            if (ele) {
                ele.autocomplete = _value;
                ele.value = _value;
            }
        }, selectorInput, selectorselValue, value);

        await page.focus(selectorInput);
        await page.click(selectorInput);
        await page.keyboard.press('Enter');
        await new Promise(resolve => setTimeout(resolve, 1000 * 1));
    }

    async downloadReport() {
        await new Promise(resolve => setTimeout(resolve, 1000 * 5));
        await page.click("#listItemsForm\\:generarReporteExcel");
        await new Promise(resolve => setTimeout(resolve, 1000 * 1));
    }

    async extractSearchResults(outputFile) {
        console.log("Extracting data table started..");
        const buttonNextSelector = "#listItemsForm\\:j_idt393";
        let rows = await this.getDataTable();
        if (rows != null && rows.length > 0) {
            let elementButton = await page.$eval(buttonNextSelector, el => el.outerHTML);
            while (elementButton && !elementButton.includes('onclick="return false;" disabled="disabled"')) {
                await page.click(buttonNextSelector);
                await new Promise(resolve => setTimeout(resolve, 1000 * 12));
                const _rows = await this.getDataTable();
                rows.push(..._rows);
                elementButton = await page.$eval(buttonNextSelector, el => el.outerHTML);
            }
            this.writeOutputFile(outputFile, rows);
        }
        console.log("Extracting data table FINISHED.");
    }

    async getDataTable() {
        const data = await page.evaluate(() => {
            const trs = Array.from(document.querySelectorAll('#listItemsForm\\:table\\:tbn tr'));
            return trs.map(tr => tr.innerText);
        });
        if (data) {
            let rows = [];
            for (let index = 0; index < data.length; index++) {
                const row = data[index];
                const values = row.split("\n");
                let newRow = {};
                newRow.Reference = values[1];
                newRow.CustomerReference = values[3];
                newRow.Ticket = values[5];
                newRow.AdminissionDate = values[7];
                newRow.Concept = values[9];
                newRow.StatusPayment = values[11];
                newRow.Currency = values[13];
                newRow.Amount = values[15];
                newRow.StatusName = values[17];
                newRow.Information = values[19];
                rows.push(newRow);
            }            
            return rows;
        }        
        return [];
    }
    
    writeOutputFile(fileName, items) {
        const data = JSON.stringify(items);
        fs.writeFileSync(fileName, data);
    }

    async close() {
        if (browser != null) {
            await browser.close();
        }
    }

}


module.exports = ComexWebApp;