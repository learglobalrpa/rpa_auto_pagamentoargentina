'use strict';

const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../../../SharedJS/configManager");
const fs = require("fs");
const ComexWebApp = require("../comexWebApp");
const AppSettingsDB = require('../../../SharedJS/db/appSettingsDB');

let browser = null;
let page = null;
let config = null;
let comexWeb = null;

let despachoWebApp = null;
let despachoWebCredential = null;

const initAppSettings = async() => {
    const appSettingsDB = new AppSettingsDB();
    appSettingsDB.init(config);
    despachoWebApp = await appSettingsDB.loadByCode("CitiFxWebAppArgentina");
    despachoWebApp.Parameters = await appSettingsDB.loadParameters(despachoWebApp.ID);
    despachoWebCredential = await appSettingsDB.getCredential(despachoWebApp.ID);
}

const init = async() => {
    browser = await puppeteer.launch({headless: false, args: ['--start-maximized']});
    const browserWSEndpoint = browser.wsEndpoint();
    console.log(browserWSEndpoint);
    
    fs.writeFile("session.txt", browserWSEndpoint, (err) => {
        if (err) console.log(err);
        console.log("Successfully Written to File.");
    });

    
    page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 600 });
    
    config = new ConfigManager();
    config.init("../../Data/Config/config.json");

    await initAppSettings();

    const url = despachoWebApp.Url;
    await page.goto(url, { waitUntil: 'load', timeout: 0 });
    page.setDefaultNavigationTimeout(240000);

    comexWeb = new ComexWebApp();
    comexWeb.setBrowser(browser);
    comexWeb.setPage(page);
    comexWeb.setConfig(config);
    comexWeb.setAppSettings(despachoWebApp);
    comexWeb.setAppCredential(despachoWebCredential);
    
    await comexWeb.login();
    
    browser.disconnect();
};


(async() => {    
    await init();

    console.log("Browser started!!")
})();