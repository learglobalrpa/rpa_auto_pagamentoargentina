'use strict';

const fs = require("fs");
const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../../../SharedJS/configManager");
const MomentHelper = require("../../../SharedJS/momentHelper");
const ComexWebApp = require("../comexWebApp");
const AppSettingsDB = require('../../../SharedJS/db/appSettingsDB');
const ProjectDB = require('../../../SharedJS/db/projectDB');

let config = null;
let comexWeb = null;
let browser = null;
let page = null;

let despachoWebApp = null;
let despachoWebCredential = null;
let project = null;

const initAppSettings = async() => {
    const appSettingsDB = new AppSettingsDB();
    appSettingsDB.init(config);
    despachoWebApp = await appSettingsDB.loadByCode("CitiFxWebAppArgentina");
    despachoWebApp.Parameters = await appSettingsDB.loadParameters(despachoWebApp.ID);
    despachoWebCredential = await appSettingsDB.getCredential(despachoWebApp.ID);
}

const initProject = async() => {
    const projectDB = new ProjectDB();
    projectDB.init(config);
    const projectCode = config.get("projectCode");
    project = await projectDB.loadByCode(projectCode);
    project.Parameters = await projectDB.loadParameters(project.ID);
}

const initComexWeb = async () => {
    const browserWSEndpoint = await new Promise(function(resolve, reject) {
        fs.readFile("session.txt", "utf-8", (err, data) => {
            resolve(data);
        });
    });    
    // Use the endpoint to reestablish a connection
    browser = await puppeteer.connect({browserWSEndpoint, width: 1366, height: 600}, {});
    page = (await browser.pages())[0];
    await page.setViewport({ width: 1366, height: 600 });

    comexWeb = new ComexWebApp();
    comexWeb.setBrowser(browser);
    comexWeb.setPage(page);
    comexWeb.setConfig(config);
    comexWeb.setAppSettings(despachoWebApp);
    comexWeb.setAppCredential(despachoWebCredential);
    comexWeb.setProject(project);
}

const initSearchStatusBank = async () => {
    await comexWeb.goToStatusBank();
};

(async() => {

    try {
        console.log(MomentHelper.getDateAndTime());

        config = new ConfigManager();
        config.init("../../Data/Config/config.json");

        const args = process.argv.slice(2);
        const outputFileJson = args[0];

        if (!outputFileJson) {
            console.log("Argument not found 'OutputFileJson'");
            return;
        }
        
        await initAppSettings();
        console.log("App Settings loaded...");
        await initProject();
        console.log("Project [" + config.get("projectCode") + "] loaded...");
        await initComexWeb();
        //console.log(await page.content());
        //await initSearchStatusBank();
        //console.log(await page.content());
        await comexWeb.setDateAndSearch();
        await comexWeb.downloadReport();
        await comexWeb.extractSearchResults(outputFileJson);

        console.log("Process finished!!")

        browser.disconnect();

    } catch (error) {
        console.log("Error: " + error.message + " - " + error.stack);
        if (browser != null) {
            browser.disconnect();
        }
    }

})();