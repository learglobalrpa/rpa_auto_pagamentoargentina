﻿using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StartAutomationFxPulse
{
    class Program
    {

        private static readonly string PROCESS_CODE = "PagArgentinaStatusBank";

        static void Main(string[] args)
        {
            Console.WriteLine("<<< INICIADO AUTOMACAO PAGAMENTOS ARGENTINA >>>");
            Console.WriteLine("######## OBTER STATUS ENVIO PARA CITI BANK FX PULSE #########");
            Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Console.WriteLine("Desenvolvido por: RPA Team Brazil");
            Console.WriteLine("Autor: Fabricio J. Reif - freif@lear.com");
            Console.WriteLine("Versao: " + assembly.GetName().Version.ToString());

            try
            {
                // Change current culture
                CultureInfo culture = CultureInfo.CreateSpecificCulture("pt-BR");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                Initialize(args);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Erro inesperado: " + ex.Message + " - " + ex.StackTrace);
            }

            Console.WriteLine("Finished!!");
        }


        private static void Initialize(string[] args)
        {
            string inputConfigFile = null;
            if (args != null && args.Length > 0)
            {
                inputConfigFile = args[0];
            }
            Config.ConfigManager config = new Config.ConfigManager();
            config.Init(inputConfigFile);

            //#########################################################################################
            // Set App global settings
            Lear.RpaMS.AppLayer.AppSettings.Instance.ConnectionString = config.Get().connectionString;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName = Environment.MachineName;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser = new User() { UserName = Environment.UserName };
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentTranslationLanguage = TranslationLanguage.PORTUGUESE;
            // Load project settings
            Project project = ProjectService.LoadProjectByCode(config.Get().projectCode);
            if (project == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Project '{config.Get().projectCode}' not found in RPA_Project Table.");
                return;
            }
            project.Parameters = ProjectService.LoadParameters(project.Id);
            project.Processes = ProjectService.LoadProcesses(project.Id);
            project.VerifyDynamicVariables();
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject = project;
            ProjectProcess projectProcess = project.GetProcessByCode(PROCESS_CODE);
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProcess = projectProcess;
            //#########################################################################################

            string folder = Assembly.GetExecutingAssembly().Location;
            folder = System.IO.Path.GetDirectoryName(folder);

            AutomationFlowFxPulse.FlowManagerFxPulse flow = new AutomationFlowFxPulse.FlowManagerFxPulse(config);
            flow.AutomationFolder = folder;
            flow.OnMessageHandler += Flow_OnMessageHandler;
            flow.Start();

        }

        private static void Flow_OnMessageHandler(string msgForCaller, AutomationFlowFxPulse.FlowManagerFxPulse.MessageEventArgs e)
        {
            if (e.type == AutomationFlowFxPulse.FlowManagerFxPulse.MessageType.Informative)
            {
                Console.WriteLine(msgForCaller);
            }
            else if (e.type == AutomationFlowFxPulse.FlowManagerFxPulse.MessageType.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;

            }
            else if (e.type == AutomationFlowFxPulse.FlowManagerFxPulse.MessageType.BusinessRule)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

    }
}
