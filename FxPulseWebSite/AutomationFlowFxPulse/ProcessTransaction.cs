﻿using Lear.RpaMS.Model.ModelLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionData;

namespace AutomationFlowFxPulse
{
    public class ProcessTransaction
    {

        private Config.ConfigManager config = null;
        private string _inputFolder;
        private int _importRoutineID;
        private int _executionID;
        private List<ItemStatusFxPulse> items = null;
        private DataTable fxPulseTable = null;

        public string InputFolder
        {
            get { return _inputFolder; }
            set { _inputFolder = value; }
        }

        public int ImportRoutineID
        {
            get { return _importRoutineID; }
            set { _importRoutineID = value; }
        }

        public int ImportExecutionID
        {
            get { return _executionID; }
            set { _executionID = value; }
        }

        public ProcessTransaction(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void Process()
        {
            this.ReadJson();
            this.ReadExcel();
            this.JoinJsonAndDataTable();
            if (fxPulseTable != null && fxPulseTable.Rows.Count > 0)
            {
                this.WriteTable();
            }
        }

        private void ReadJson()
        {
            string fileOutputJSON = Path.Combine(_inputFolder, FxPulseConstants.JSON_FILE_NAME);
            if (File.Exists(fileOutputJSON))
            {
                string jsonText = File.ReadAllText(fileOutputJSON);
                items = JsonConvert.DeserializeObject<List<ItemStatusFxPulse>>(jsonText);
            }
        }

        private void ReadExcel()
        {
            string excelFxPulseFilePattern = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.GetParameterByName("FxPulseStatusDownloadPattern").Value;
            string[] filesInput = Directory.GetFiles(_inputFolder, excelFxPulseFilePattern);
            if (filesInput != null && filesInput.Length > 0)
            {
                fxPulseTable = ExcelFxPulseManager.ReadExcel(filesInput[0]);                
            }
        }

        private void JoinJsonAndDataTable()
        {
            if (items != null && fxPulseTable != null)
            {
                StatusReviewBankData statusBankData = new StatusReviewBankData(config);
                List<StatusBase> listStatus = statusBankData.LoadAll();

                foreach (DataRow row in fxPulseTable.Rows)
                {
                    row["Company"] = 5000; //Argentina
                    string reference = Convert.ToString(row["Reference"]);
                    //Search reference in the items list
                    ItemStatusFxPulse fxPulseItem = items.Find(item => item.Reference == reference);
                    if (fxPulseItem != null)
                    {
                        int reviewStatusBankID = this.GetStatusBankReviewID(listStatus, fxPulseItem.StatusName);
                        row["BankReviewStatusID"] = reviewStatusBankID;
                        row["CustomerReference"] = fxPulseItem.CustomerReference;
                        row["Ticket"] = fxPulseItem.Ticket;
                        DateTime date = DateTime.Parse(fxPulseItem.AdminissionDate);
                        row["AdmissionDate"] = date;
                        row["Concept"] = fxPulseItem.Concept;
                        row["StatusPayment"] = fxPulseItem.StatusPayment;
                        row["Currency"] = fxPulseItem.Currency;
                        row["Amount"] = Convert.ToDouble(fxPulseItem.Amount);
                        row["Information"] = fxPulseItem.Information;
                        row["UserCodeInsert"] = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser.UserName;
                        row["MachineName"] = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName;
                        row["DataImportRoutineID"] = this.ImportRoutineID;
                        row["DataImportExecutionID"] = this.ImportExecutionID;
                    } else
                    {
                        row["UserCodeInsert"] = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser.UserName;
                        row["MachineName"] = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName;
                        row["DataImportRoutineID"] = this.ImportRoutineID;
                        row["DataImportExecutionID"] = this.ImportExecutionID;
                    }
                }
            }
        }

        private int GetStatusBankReviewID(
            List<StatusBase> listStatus,
            string statusName)
        {
            StatusBase status = listStatus.Find(item => item.Name == statusName);
            if (status != null)
            {
                return status.Id;
            }
            return 0;
        }

        private void WriteTable()
        {
            ImportPaymentInBank importData = new ImportPaymentInBank(config);
            importData.ImportData(fxPulseTable, this.ImportExecutionID);
        }

    }
}
