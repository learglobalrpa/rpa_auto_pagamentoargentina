﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFlowFxPulse
{
    public class ItemStatusFxPulse
    {
        public string Reference;
        public string CustomerReference;
        public string Ticket;
        public string AdminissionDate;
        public string Concept;
        public string StatusPayment;
        public string Currency;
        public string Amount;
        public string StatusName;
        public string Information;

    }
}
