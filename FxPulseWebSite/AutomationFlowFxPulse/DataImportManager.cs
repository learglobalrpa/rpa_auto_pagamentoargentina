﻿using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFlowFxPulse
{
    public class DataImportManager
    {

        public int ImportRoutineID;


        public int StartExecution()
        {
            string dataRoutineCode = "RPA_AP_PaymentInBank";            
            DataImportRoutine importRoutine = DataImportRoutineService.LoadRoutineByCode(dataRoutineCode);
            if (importRoutine == null)
            {
                throw new Exception($"Routine Code '{dataRoutineCode}' not found in Table RPA_DataImportRoutine.");
            }
            ImportRoutineID = importRoutine.Id;
            int executionID = DataImportExecutionService.StartExecution(importRoutine.Id, "");
            return executionID;
        }

        public void EndExecution(int executionID, StatusExecution state, string message)
        {
            DataImportExecutionService.EndExecution(executionID, state, message);
        }

    }
}
