﻿using Lear.RpaMS.Common.Library.FileSystem;
using RunExtractData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TransactionData;

namespace AutomationFlowFxPulse
{
    public class FlowManagerFxPulse
    {

        private Config.ConfigManager config = null;
        private int maxRetryNumber = -1;
        private int attempts = 0;
        private string _automationFolder;

        public enum MessageType
        {
            Informative = 1,
            Error = 2,
            BusinessRule = 3
        }

        public string AutomationFolder
        {
            get { return _automationFolder; }
            set { _automationFolder = value; }
        }

        public delegate void MessageHandler(string msgForCaller, MessageEventArgs e);
        public event MessageHandler OnMessageHandler;

        public class MessageEventArgs : EventArgs
        {
            public MessageType type;
        }

        public FlowManagerFxPulse(Config.ConfigManager config)
        {
            this.config = config;
        }

        private void RaiseInformativeMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.Informative;
            OnMessageHandler(message, e);
        }

        private void RaiseErrorMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.Error;
            OnMessageHandler(message, e);
        }

        private void RaiseBusinessRuleMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.BusinessRule;
            OnMessageHandler(message, e);
        }

        public void Start()
        {
            try
            {
                if (maxRetryNumber < 0)
                {
                    maxRetryNumber = Convert.ToInt32(Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.MaxRetryNumber);
                    attempts = 1;
                }

                RaiseInformativeMessage("CitiFX Pulse Web Site: Automation Flow Started: " + DateTime.Now.ToString());
                RaiseInformativeMessage("Attempt: " + attempts);
                this.Init();


            }
            catch (SystemError ex)
            {
                RaiseErrorMessage(ex.Message + " -" + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.Start();
                }
            }
            catch (BusinessRuleException ex)
            {

                throw;
            }
            catch (Exception ex)
            {
                RaiseErrorMessage(ex.Message + " - " + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.Init();
                }
            }

        }

        private void Init()
        {
            DataImportManager importManager = new DataImportManager();
            int executionID = 0;
            try
            {
                executionID = importManager.StartExecution();

                string inputFolder = _automationFolder + @"\Input";
                if (!Directory.Exists(inputFolder))
                {
                    Directory.CreateDirectory(inputFolder);
                }
                string fileOutputJSON = Path.Combine(inputFolder, FxPulseConstants.JSON_FILE_NAME);
                if (File.Exists(fileOutputJSON))
                {
                    File.Delete(fileOutputJSON);
                }

                this.CleanDownloadFiles(inputFolder);
                this.RunStatusBank(fileOutputJSON);
                this.MoveExtractedData(inputFolder);

                ProcessTransaction processTransaction = new ProcessTransaction(config);
                processTransaction.InputFolder = inputFolder;
                processTransaction.ImportRoutineID = importManager.ImportRoutineID;
                processTransaction.ImportExecutionID = executionID;
                processTransaction.Process();

                importManager.EndExecution(executionID,
                        Lear.RpaMS.Model.ModelLayer.StatusExecution.SUCCESS,"");
            }
            catch(Exception ex)
            {
                try
                {
                    importManager.EndExecution(executionID,
                        Lear.RpaMS.Model.ModelLayer.StatusExecution.ERROR,
                        ex.Message);
                }
                catch (Exception)
                {}

                throw ex;
            }            

        }

        private void CleanDownloadFiles(string inputFolder)
        {
            //Delete existing download files
            string downloadUserLocalFolder = this.GetDownloadLocalFolder();
            string downloadFilePattern = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.GetParameterByName("FxPulseStatusDownloadPattern").Value;
            string[] filesDownload = Directory.GetFiles(downloadUserLocalFolder, downloadFilePattern);
            string[] filesInput = Directory.GetFiles(inputFolder, downloadFilePattern);
            string[] files = filesDownload.Concat(filesInput).ToArray();
            if (files != null && files.Length > 0)
            {
                foreach (var file in files)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception)
                    {}
                }
            }
        }

        private void RunStatusBank(string fileOutput)
        {
            RaiseInformativeMessage(" ===============================================================");
            RaiseInformativeMessage("Iniciado execucao da rotina para obter STATUS ENVIO arquivo para CITI BANK FX PULSE");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            string filename = "node.exe";
            string arguments = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProcess.Path + @"\StatusPayment\index.js" + " \"" + fileOutput + "\"";
            string workingDirectory = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProcess.Path + @"\StatusPayment";
            string output = ExecuteProcessManager.Run(filename, arguments, workingDirectory);
            stopwatch.Stop();
            RaiseInformativeMessage("Finalizado rotina para obter STATUS ENVIO arquivo para CITI BANK FX PULSE: " + stopwatch.Elapsed);
            RaiseInformativeMessage("Retorno da execucao:");
            RaiseInformativeMessage(output);
            RaiseInformativeMessage("===============================================================");
            if (output.Contains("Error:"))
            {
                throw new SystemError();
            }
        }

        private void MoveExtractedData(string inputFolder)
        {
            string downloadUserLocalFolder = this.GetDownloadLocalFolder();
            string downloadFilePattern = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.GetParameterByName("FxPulseStatusDownloadPattern").Value;
            string[] filesDownload = Directory.GetFiles(downloadUserLocalFolder, downloadFilePattern);
            if (filesDownload != null && filesDownload.Length > 0)
            {
                foreach (string file in filesDownload)
                {
                    ZipFile.ExtractToDirectory(file, inputFolder);
                }
            }
        }

        private string GetDownloadLocalFolder()
        {
            return KnownFolders.GetPath(KnownFolder.Downloads);
        }



    }
}
