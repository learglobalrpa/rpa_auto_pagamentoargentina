﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace AutomationFlowFxPulse
{
    public class ExcelFxPulseManager
    {

        public static DataTable ReadExcel(string fileName)
        {
            Excel.Application excelApp = new Excel.Application();
            Excel.Workbook excelBook = excelApp.Workbooks.Open(fileName);
            Excel.Worksheet excelSheet = excelBook.Sheets[1];
            Excel.Range excelRange = excelSheet.UsedRange;
            
            int rows = excelRange.Rows.Count;
            int cols = excelRange.Columns.Count;

            //Adjust to read the correct rows
            rows -= 4;

            Excel.Range range = excelSheet.Range[excelSheet.Cells[5, 1], excelSheet.Cells[rows, cols]];
            object[,] holder = range.Value2;

            DataTable table = GetDataTable();

            for (int i = 1; i <= rows - 1; i++)
            {
                if ((holder.Length / cols) < i)
                {
                    break;
                }
                DataRow row = table.NewRow();
                row["Reference"] = Convert.ToString(holder[i, 2]);
                row["DealNumber"] = Convert.ToString(holder[i, 11]);
                table.Rows.Add(row);
            }

            //release com objects to fully kill excel process from running in the background
            if (excelRange != null)
            {
                Marshal.ReleaseComObject(excelRange);
            }
            if (excelSheet != null)
            {
                Marshal.ReleaseComObject(excelSheet);
            }
            if (excelBook != null)
            {
                //close and release
                excelBook.Close();
                Marshal.ReleaseComObject(excelBook);
            }
            if (excelApp != null)
            {
                //quit and release
                excelApp.Quit();
                Marshal.ReleaseComObject(excelApp);
            }

            return table;            
        }

        private static DataTable GetDataTable()
        {
            DataTable data = new DataTable();
            data.Columns.Add("Company", typeof(int));
            data.Columns.Add("BankReviewStatusID", typeof(int));
            data.Columns.Add("Reference", typeof(string));
            data.Columns.Add("CustomerReference", typeof(string));
            data.Columns.Add("Ticket", typeof(string));            
            data.Columns.Add("AdmissionDate", typeof(DateTime));
            data.Columns.Add("Concept", typeof(string));
            data.Columns.Add("StatusPayment", typeof(string));
            data.Columns.Add("Currency", typeof(string));
            data.Columns.Add("Amount", typeof(double));
            data.Columns.Add("Information", typeof(string));
            data.Columns.Add("DealNumber", typeof(string));
            data.Columns.Add("UserCodeInsert", typeof(string));
            data.Columns.Add("MachineName", typeof(string));
            data.Columns.Add("DataImportRoutineID", typeof(int));
            data.Columns.Add("DataImportExecutionID", typeof(int));
            return data;
        }




    }
}
