﻿using Newtonsoft.Json;
using System;

namespace Config
{
    public class ConfigManager
    {

        private const string CONFIG_FILE = @"..\Data\Config\config.json";

        private Configuration config = null;

        public void Init(string configFile)
        {
            string json = null;
            if (String.IsNullOrEmpty(configFile))
            {
                json = System.IO.File.ReadAllText(CONFIG_FILE);
            } else
            {
                json = System.IO.File.ReadAllText(configFile);
            }
            config = JsonConvert.DeserializeObject<Configuration>(json);
        }        

        public Configuration Get()
        {
            return config;
        }        

    }
}
