﻿namespace Config
{
    public class Configuration
    {
        public string connectionString { get; set; }
        public string projectCode { get; set; }

        public bool GenerateDocuments { get; set; }

    }    

}
