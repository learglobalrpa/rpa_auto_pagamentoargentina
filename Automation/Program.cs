﻿using Lear.RpaMS.Common.Library;
using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Model.ModelLayer;
using RunExtractData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StartAutomation
{
    class Program
    {

        static void Main(string[] args)
        {

            Console.WriteLine("<<< INICIADO AUTOMACAO PAGAMENTOS ARGENTINA >>>");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Console.WriteLine("Desenvolvido por: RPA Team Brazil");
            Console.WriteLine("Autor: Fabricio J. Reif - freif@lear.com");
            AssemblyInfo assemblyInfo = new AssemblyInfo(assembly);
            //Console.WriteLine("Versao: " + assembly.GetName().Version.ToString());
            Console.WriteLine("Versao: " + assemblyInfo.GetAssemblyVersionWithDate());

            try
            {
                // Change current culture
                CultureInfo culture = CultureInfo.CreateSpecificCulture("pt-BR");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                Initialize(args);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Erro inesperado: " + ex.Message + " - " + ex.StackTrace);
            }

            Console.WriteLine("Finished!!");
        }


        private static void Initialize(string[] args)
        {
            string inputConfigFile = null;
            string genDocs = "/GenerateDocuments=true";
            if (args != null && args.Length > 0)
            {
                inputConfigFile = args[0];

                if (args.Length > 1)
                {
                    genDocs = args[1];
                }
            }
            Config.ConfigManager config = new Config.ConfigManager();
            config.Init(inputConfigFile);
            config.Get().GenerateDocuments = true;
            // Check if needs to generate documents. Default = true
            if (genDocs.ToLower().Contains("false"))
            {
                config.Get().GenerateDocuments = false;
            }

            //#########################################################################################
            // Set App global settings
            Lear.RpaMS.AppLayer.AppSettings.Instance.ConnectionString = config.Get().connectionString;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName = Environment.MachineName;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser = new User() { UserName = Environment.UserName };
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentTranslationLanguage = TranslationLanguage.PORTUGUESE;
            // Load project settings
            Project project = ProjectService.LoadProjectByCode(config.Get().projectCode);
            if (project == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Project '{config.Get().projectCode}' not found in RPA_Project Table.");
                return;
            }
            project.Parameters = ProjectService.LoadParameters(project.Id);
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject = project;
            //#########################################################################################


            AutomationFlow.FlowManager flow = new AutomationFlow.FlowManager(config);
            flow.OnMessageHandler += Flow_OnMessageHandler;
            flow.Start(); 

        }

        private static void Flow_OnMessageHandler(string msgForCaller, AutomationFlow.FlowManager.MessageEventArgs e)
        {
            if (e.type == AutomationFlow.FlowManager.MessageType.Informative)
            {
                Console.WriteLine(msgForCaller);
            } else if (e.type == AutomationFlow.FlowManager.MessageType.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;

            } else if (e.type == AutomationFlow.FlowManager.MessageType.BusinessRule)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }                        

    }
}
