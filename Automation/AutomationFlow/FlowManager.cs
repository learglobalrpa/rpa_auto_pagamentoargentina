﻿using RunExtractData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TransactionData;
using Lear.RpaMS.Model.ModelLayer.Queue;
using Newtonsoft.Json;

namespace AutomationFlow
{
    public class FlowManager
    {

        private Config.ConfigManager config = null;
        private int maxRetryNumber = -1;
        private int attempts = 0;

        private string currentDocumentFolder = null;

        public enum MessageType
        {
            Informative = 1,
            Error = 2,
            BusinessRule = 3
        }

        public delegate void MessageHandler(string msgForCaller, MessageEventArgs e);
        public event MessageHandler OnMessageHandler;

        public class MessageEventArgs : EventArgs
        {
            public MessageType type;
        }

        public FlowManager(Config.ConfigManager config)
        {
            this.config = config;
        }

        private void RaiseInformativeMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.Informative;
            OnMessageHandler(message, e);
        }

        private void RaiseErrorMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.Error;
            OnMessageHandler(message, e);
        }

        private void RaiseBusinessRuleMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.BusinessRule;
            OnMessageHandler(message, e);
        }

        public void Start()
        {
            try
            {
                if (maxRetryNumber < 0)
                {
                    maxRetryNumber = Convert.ToInt32(Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.MaxRetryNumber);
                    attempts = 1;
                }

                RaiseInformativeMessage("Automation Flow Started: " + DateTime.Now.ToString());
                RaiseInformativeMessage("Attempt: " + attempts); ;
                this.Init();

            }
            catch (SystemError ex)
            {
                RaiseErrorMessage(ex.Message + " -" + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.Start();
                }
            }
            catch (BusinessRuleException ex)
            {

                throw;
            }
            catch (Exception ex)
            {
                RaiseErrorMessage(ex.Message + " - " + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.Init();
                }
            }

        }

        public void StartWithQueueItem(QueueItem queueItem)
        {
            try
            {
                if (maxRetryNumber < 0)
                {
                    maxRetryNumber = Convert.ToInt32(Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.MaxRetryNumber);
                    attempts = 1;
                }

                RaiseInformativeMessage("Automation Flow Queue Item Started: " + DateTime.Now.ToString());
                RaiseInformativeMessage("Attempt: " + attempts); ;
                this.InitWithQueue(queueItem);

            }
            catch (SystemError ex)
            {
                RaiseErrorMessage(ex.Message + " -" + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.StartWithQueueItem(queueItem);
                }
            }
            catch (BusinessRuleException ex)
            {

                throw;
            }
            catch (Exception ex)
            {
                RaiseErrorMessage(ex.Message + " - " + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.InitWithQueue(queueItem);
                }
            }

        }

        private void Init()
        {
            this.CreateMainDirectoryStructure();

            TransactionDataManager dataManager = new TransactionDataManager(config);
            List<TransactionItem> items = dataManager.GetTransactionSqlData(null);
            RaiseInformativeMessage("Number of transactions: " + items.Count);
            this.RunExtractData(items, null);
            //Load again after obtained shipping number and departure date
            items = dataManager.GetTransactionSqlData(null);

            this.GetTransactionData(items);
        }

        private void InitWithQueue(QueueItem queueItem)
        {
            this.CreateMainDirectoryStructure();

            int[] ids = JsonConvert.DeserializeObject<int[]>(queueItem.TransactionItem);
            TransactionDataManager dataManager = new TransactionDataManager(config);
            List<TransactionItem> items = dataManager.GetTransactionSqlData(ids);
            RaiseInformativeMessage("Number of transactions: " + items.Count);
            this.RunExtractData(items, queueItem);
            //Load again after obtained shipping number and departure date
            items = dataManager.GetTransactionSqlData(ids);

            this.GetTransactionData(items);
        }

        private void GetTransactionData(List<TransactionItem> items)
        {
            //Get list os plants
            var plants = from plant in items
                         group plant by plant.Planta into g
                         select g.Key;

            RaiseInformativeMessage("Number of Plants: " + plants.Count());

            foreach (string plant in plants)
            {
                try
                {
                    RaiseInformativeMessage("----------------------------------------------------");
                    RaiseInformativeMessage($"Getting documents for plant {plant} started.");
                    // Create folder for current plant
                    string plantFolder = Path.Combine(this.currentDocumentFolder, plant);
                    if (!Directory.Exists(plantFolder))
                    {
                        Directory.CreateDirectory(plantFolder);
                    }

                    List<TransactionItem> itemsByPlant = (from item in items
                                                          where item.Planta == plant
                                                          select item).ToList<TransactionItem>();                    

                    this.ProcessTransaction(plant, plantFolder, itemsByPlant);
                }
                catch (BusinessRuleException ex)
                {
                    RaiseBusinessRuleMessage($"Business Rule: {ex.Message} - {ex.StackTrace}");
                }
                catch (Exception ex)
                {
                    RaiseErrorMessage($"Error: {ex.Message} - {ex.StackTrace}");
                }
            }
        }

        private void ProcessTransaction(string plant, string plantFolder, List<TransactionItem> items)
        {
            //Get list os suppliers
            var suppliers = from item in items
                            where item.Planta == plant
                            group item by item.CodForn into g
                            select g.Key;
            RaiseInformativeMessage($"Number of suppliers: {suppliers.Count()}");

            ProcessTransaction process = new ProcessTransaction(this.config);

            foreach (string supplier in suppliers)
            {
                string supplierFolder = Path.Combine(plantFolder, supplier);
                if (!Directory.Exists(supplierFolder))
                {
                    Directory.CreateDirectory(supplierFolder);
                }

                List<TransactionItem> itemsBySupplier = (from item in items
                                                         where item.CodForn == supplier
                                                         select item).ToList<TransactionItem>();

                process.Process(new ProcessItem
                {
                    Plant = plant,
                    Supplier = supplier,
                    SupplierFolder = supplierFolder,
                    Items = itemsBySupplier
                });
            }

        }


        public void EndProcess()
        {

        }

        public void RunExtractData(List<TransactionItem> items, QueueItem queueItem)
        {            

            if (items == null || items.Count == 0)
            {
                RaiseInformativeMessage("Nenhum item em ABERTO em PAGAMENTOS EM ANDAMENTO!");
                return;
            }

            //Filter by items that need search for tracking number
            //items = items.FindAll(item => String.IsNullOrEmpty(item.Status) 
            //|| item.PaymentStateID == State.WaitingShipmmentNoID);
            //if (items == null || items.Count == 0)
            //{
            //    RaiseInformativeMessage("Nenhum item Aguardando Despacho!!");
            //    return;
            //}

            RaiseInformativeMessage("===============================================================");
            RaiseInformativeMessage("Iniciado execucao da rotina para obter dados do despacho");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            string filename = "node.exe";
            string arguments = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.MainPath + @"\ExtractData\ExtrairDespachos\index.js";
            if (queueItem != null)
            {
                arguments += " \"" + queueItem.Id + "\"";
            }
            string workingDirectory = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.MainPath + @"\ExtractData\ExtrairDespachos";
            string output = ExecuteProcessManager.Run(filename, arguments, workingDirectory);
            stopwatch.Stop();
            RaiseInformativeMessage("Finalizado rotina para obter dados do despacho: " + stopwatch.Elapsed);
            RaiseInformativeMessage("Retorno da execucao:");
            RaiseInformativeMessage(output);
            RaiseInformativeMessage("===============================================================");
            if (output.Contains("Error:"))
            {
                throw new SystemError();
            }

        }

        private void CreateMainDirectoryStructure()
        {            
            string documentFolder = ProcessHelper.GetOutputFolder(config);
            if (!Directory.Exists(documentFolder))
            {
                Directory.CreateDirectory(documentFolder);
            }
            this.currentDocumentFolder = documentFolder;
        }

    }
}
