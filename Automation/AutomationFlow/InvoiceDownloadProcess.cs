﻿using DownloadDocuments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TransactionData;
using System.IO;
using Newtonsoft.Json;
using Lear.RpaMS.Model.ModelLayer.RPA;
using Lear.RpaMS.Model.ModelLayer.App;

namespace AutomationFlow
{
    public class InvoiceDownloadProcess
    {

        private Config.ConfigManager config = null;

        public InvoiceDownloadProcess(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void Execute(ProcessItem processItem,
            AppSettings despachoWebApp,
            AppSettingsCredential despachoWebCredential)
        {
            Console.WriteLine(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
            Console.WriteLine("Process to Download Documents started...");

            //Filter items for this supplier and download documents for Despacho
            var itemsDownload = from item in processItem.Items
                                where item.CodForn == processItem.Supplier && item.Planta == processItem.Plant
                                  && item.PaymentStateID == State.WaitingDocumentDownloadID
                                group item by item.Despacho into g
                                select new { Despacho = g.Key, Items = g.ToList() };

            Console.WriteLine($"Total Items to Download: {itemsDownload.Count()} Fornecedor: {processItem.Supplier}");

            if (itemsDownload.Count() > 0)
            {
                List<DocumentFileType> docs = this.GetDocumentoDownload();

                CookieContainer cookies = new CookieContainer();
                bool isLogged = this.LoginDespachoWeb(cookies, despachoWebCredential);
                if (!isLogged)
                {
                    throw new Exception("Error: Cannot login to Despachos Web.");
                }

                Parallel.ForEach(itemsDownload, (currentItem) =>
                {
                    try
                    {
                        Console.WriteLine($"Donwloading files to despacho: {currentItem.Despacho} ....");

                        Parallel.ForEach(docs, (docType) =>
                        {
                            try
                            {
                                TransactionItem item = currentItem.Items.FirstOrDefault();
                                string result = this.DownloadDocuments(item, docType, cookies);
                                foreach (TransactionItem itemTransaction in currentItem.Items)
                                {
                                    if (result == "OK")
                                    {
                                        this.UpdateDownloadStatus(itemTransaction, docType.Family, Status.DownloadOkID);
                                    } else if (result == "#NOTFOUND")
                                    {
                                        this.UpdateDownloadStatus(itemTransaction, docType.Family, Status.NotFoundID);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine($"Error trying to download documentt Familia {docType.Family} for Despacho {currentItem.Despacho}: {ex.Message} - {ex.StackTrace}");
                            }
                        });
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error trying to download files to despacho: {ex.Message} - {ex.StackTrace}");
                    }
                });
            }
        }

        public void UpdateDownloadedFiles(ProcessItem processItem)
        {
            //Filter items for this supplier and download documents for Despacho
            var itemsToUpdate = from item in processItem.Items
                                where item.CodForn == processItem.Supplier && item.Planta == processItem.Plant
                                  && item.PaymentStateID != State.WaitingShipmmentNoID
                                  && item.PaymentStateID != State.WaitingDocumentDownloadID
                                  && item.PaymentStateID != State.DocumentCreatedID
                                  && item.PaymentStateID != State.DocumentSentToBankID
                                group item by item.Despacho into g
                                select new { Despacho = g.Key, Items = g.ToList() };

            if (itemsToUpdate.Count() > 0)
            {
                Update updateTransaction = new Update(config);
                DocumentManager docManager = new DocumentManager(this.config);

                foreach (var currrenItem in itemsToUpdate)
                {
                    string downloadFolder = ProcessHelper.GetDonwloadFolder(config, currrenItem.Items.FirstOrDefault());

                    //Check INVOICE
                    Document documentInvoice = null;
                    string fileName = this.GetDownloadFileName(currrenItem.Items.FirstOrDefault(), 2);
                    bool invoiceFound = false;
                    if (File.Exists(fileName))
                    {
                        invoiceFound = true;
                        // Create document into database
                        documentInvoice = docManager.CreateDocument(currrenItem.Items.FirstOrDefault(),
                            fileName,
                            Lear.RpaMS.Model.ModelLayer.DocumentTypeListCode.Invoice);
                    }
                    //Check Bill of Lading (CTe / Conhecimento de Embarque)
                    fileName = this.GetDownloadFileName(currrenItem.Items.FirstOrDefault(), 3);
                    Document documentBillOfLading = null;
                    bool billOfLadingFound = false;
                    if (File.Exists(fileName))
                    {
                        billOfLadingFound = true;
                        // Create document into database
                        documentBillOfLading = docManager.CreateDocument(currrenItem.Items.FirstOrDefault(),
                            fileName,
                            Lear.RpaMS.Model.ModelLayer.DocumentTypeListCode.BillOfLading);
                    }

                    foreach (TransactionItem item in currrenItem.Items)
                    {
                        if (invoiceFound)
                        {
                            item.StatusDocumentoInvoice = Status.DownloadOk;
                            item.IdStatusDocumentoInvoice = Status.DownloadOkID;
                        } 
                        if (billOfLadingFound)
                        {
                            item.StatusDocumentoEmbarque = Status.DownloadOk;
                            item.IdStatusDocumentoEmbarque = Status.DownloadOkID;
                        }

                        if (item.IdStatusDocumentoInvoice == Status.DownloadOkID &&
                            item.IdStatusDocumentoEmbarque == Status.DownloadOkID)
                        {
                            item.Situacao = State.DownloadOk;
                            item.PaymentStateID = State.DownloadOkID;

                        } else if (item.IdStatusDocumentoInvoice == Status.NotFoundID ||
                                   item.IdStatusDocumentoEmbarque == Status.NotFoundID)
                        {
                            item.Situacao = State.CheckManually;
                            item.PaymentStateID = State.CheckManuallyID;
                        }
                    }

                    //Update the status for download files
                    updateTransaction.UpdateItemDB(currrenItem.Items);
                    //Assign documents for each item
                    List<Document> documents = new List<Document>();
                    documents.Add(documentInvoice);
                    documents.Add(documentBillOfLading);
                    updateTransaction.AssignDocuments(currrenItem.Items, documents);
                }
            }
        }

        private bool LoginDespachoWeb(CookieContainer cookies, AppSettingsCredential despachoWebCredential)
        {            
            string userName = despachoWebCredential.UserID;
            string password = despachoWebCredential.UserPwd;
            DespachoWeb webApp = new DespachoWeb();
            string url = "http://bank2.alpha2000.com.ar/WebUtilsLogin/Login/";
            string postData = "Usuario=" + userName + "&Password=" + password + "&ControladorRedirigir=Despachos&AccionRedirigir=Index&Proyecto=Alpha.Despachos.Web&ValidarLogin=true&Bank=false";
            string referer = "http://bank2.alpha2000.com.ar/";
            string contentType = "application/x-www-form-urlencoded; charset=UTF-8";
            string accept = "*/*";
            return webApp.Login2(url, postData, referer, contentType, accept, cookies);
        }

        private string DownloadDocuments(TransactionItem item, DocumentFileType fileType, CookieContainer cookies)
        {
            string downloadFolder = ProcessHelper.GetDonwloadFolder(config, item);
            if (!Directory.Exists(downloadFolder))
            {
                Directory.CreateDirectory(downloadFolder);
            }
            string fileName = this.GetDownloadFileName(item, fileType.Family);
            if (File.Exists(fileName))
            {
                Console.WriteLine($"File {fileName} has alerady been downloaded!");
                return "OK";
            }

            DespachoWeb webApp = new DespachoWeb();
            string url = "http://bank2.alpha2000.com.ar/Despachos/DescargarPDFDigitalizado";
            string postData = "{\"NroDespacho\":\"" + item.Despacho + "\",\"Familia\":" + fileType.Family + "}";
            string referer = "http://bank2.alpha2000.com.ar/Despachos/Index";
            string contentType = "application/json; charset=UTF-8";
            string accept = "application/json, text/javascript, */*; q=0.01";

            Console.WriteLine($"Download file {Path.GetFileName(fileName)} started...");

            WebAppHttpResponse resp = webApp.Download2(url, postData, referer, contentType, accept, cookies);
            if (resp != null && !String.IsNullOrEmpty(resp.PageSource))
            {
                DocumentResult docResult = JsonConvert.DeserializeObject<DocumentResult>(resp.PageSource);
                if (docResult.Existe)
                {
                    File.WriteAllBytes(fileName, Convert.FromBase64String(docResult.ArchivoBase64));
                    Console.WriteLine($"Download file {fileName} realizado com sucesso!");
                    return "OK";
                }
                else
                {
                    return "#NOTFOUND";
                }
            }
            return "ERROR: HTTP Response was not found.";
        }

        private List<DocumentFileType> GetDocumentoDownload()
        {
            List<DocumentFileType> docs = new List<DocumentFileType>();
            docs.Add(new DocumentFileType()
            {
                FileType = "INVOICE",
                Family = 2
            });
            docs.Add(new DocumentFileType()
            {
                FileType = "CTE",
                Family = 3
            });
            return docs;
        }

        private string GetDownloadFileName(TransactionItem item, int family)
        {
            string downloadFolder = ProcessHelper.GetDonwloadFolder(config, item);
            string fileName = Path.Combine(downloadFolder, $"{item.Despacho}_Familia {family}.pdf");
            return fileName;
        }

        private void UpdateDownloadStatus(TransactionItem item, int family, int status)
        {
            if (family == 2)
            {
                item.IdStatusDocumentoInvoice = status;

            }
            else if (family == 3)
            {
                item.IdStatusDocumentoEmbarque = status;
            }
        }

    }
}
