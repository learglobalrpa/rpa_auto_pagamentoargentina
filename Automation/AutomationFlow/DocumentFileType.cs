﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomationFlow
{
    public class DocumentFileType
    {
        public string FileType { get; set; }
        public int Family { get; set; }
        public string StatusDocColumnName { get; set; }

    }
}
