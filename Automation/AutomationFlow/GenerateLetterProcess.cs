﻿using ExtractPdfData;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.RPA;
using LetterCreation;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionData;

namespace AutomationFlow
{
    public class GenerateLetterProcess
    {
        private static int VENDOR_CROSS_TABLE = 2;
        private Config.ConfigManager config = null;

        private bool EnableDocument6401 = false;
        private DateTime? Document6401DateFrom = null;
        private DateTime? Document6401DateTo = null;
        private string Document6401Location;

        public GenerateLetterProcess(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void Execute(ProcessItem processItem)
        {
            Console.WriteLine("******************************************************************************");
            Console.WriteLine("Process to Generate Documents started...");

            //Filter items for this supplier and can generate letters
            var itemsGenerateLetter = from item in processItem.Items
                                      where item.CodForn == processItem.Supplier && item.Planta == processItem.Plant
                                        && (item.PaymentStateID == State.WaitingBankDocumentID
                                        || item.PaymentStateID == State.DocumentCreatedID)
                                        && item.PaymentStateID != State.DocumentSentToBankID
                                      group item by item.Despacho into g
                                      select new { Despacho = g.Key, Items = g.ToList() };

            int total = itemsGenerateLetter.Count();
            Console.WriteLine($"Total documents to create: {total}");
            int currentItem = 1;
            Update transactionUpdate = new Update(config);
            DocumentManager docManager = new DocumentManager(config);

            //Load settings for Document 6401
            this.LoadAutoSettings();

            foreach (var currentDespacho in itemsGenerateLetter)
            {
                try
                {
                    Console.WriteLine($"Generating document for 'Despacho': {currentDespacho.Despacho}");
                    string fxDocument = this.CreateFxDocument(currentDespacho.Items);
                    string[] declaracionDocs = null;
                    declaracionDocs = this.CreateDeclaracionDocs(currentDespacho.Despacho, currentDespacho.Items);
                    //Check if bill of lading exists
                    string billOfLading = this.GetBillOfLadingDocument(currentDespacho.Despacho, currentDespacho.Items);
                    if (String.IsNullOrEmpty(billOfLading))
                    {
                        throw new BusinessRuleException("Bill of Lading not found!");
                    }
                    //Check if invoice documents exists
                    string[] invoices = this.GetInvoiceDocuments(currentDespacho.Despacho, currentDespacho.Items);
                    if (invoices.Count() == 0)
                    {
                        throw new BusinessRuleException("Invoices not found!");
                    }

                    //SML Document
                    string smlDocument = this.CreateSmlDocument(currentDespacho.Items);

                    //Create unique PDF
                    string[] filesToPDF = new string[declaracionDocs.Length + invoices.Length + (EnableDocument6401 ? 4 : 3)];
                    filesToPDF[0] = fxDocument;
                    filesToPDF[1] = smlDocument;
                    declaracionDocs.CopyTo(filesToPDF, 2);
                    filesToPDF[2 + declaracionDocs.Length] = billOfLading;
                    invoices.CopyTo(filesToPDF, 2 + declaracionDocs.Length + declaracionDocs.Length);
                                        
                    //Check if needs to add Document 6401
                    if (EnableDocument6401)
                    {
                        string document6401 = this.GetDocument6401(currentDespacho.Items);
                        if (string.IsNullOrEmpty(document6401))
                        {
                            throw new BusinessRuleException("Comprovante 6401 não encontrado!");
                        }
                        filesToPDF[filesToPDF.Length - 1] = document6401;
                    }
                    
                    string output = Path.Combine(processItem.SupplierFolder, $"{processItem.Supplier}_{currentDespacho.Despacho}_{DateTime.Now.ToString("yyyy-MM-dd")}.pdf");
                    if (File.Exists(output))
                    {
                        File.Delete(output);
                    }
                    PdfManipulation.CreatePDF(output, filesToPDF);

                    // Create document into database
                    Document importPaymentDocument = docManager.CreateDocument(currentDespacho.Items.FirstOrDefault(),
                        output,
                        DocumentTypeListCode.ImportPaymentCitiBank);

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Document created successfully: {output}");
                    Console.ForegroundColor = ConsoleColor.White;

                    foreach (TransactionItem itemTransaction in currentDespacho.Items)
                    {
                        itemTransaction.Situacao = State.DocumentCreated;
                        itemTransaction.PaymentStateID = State.DocumentCreatedID;
                        itemTransaction.StatusCarta = Status.LetterOk;
                        itemTransaction.LetterDocStatusID = Status.LetterOkID;
                        itemTransaction.LetterDocFileName = output;
                    }

                    Update update = new Update(config);
                    update.UpdateItemDB(currentDespacho.Items);
                    //Assign documents for each item
                    List<Document> documents = new List<Document>();
                    documents.Add(importPaymentDocument);
                    update.AssignDocuments(currentDespacho.Items, documents, true);
                }
                catch (BusinessRuleException ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{ex.Message}");
                    Console.ForegroundColor = ConsoleColor.White;

                    foreach (TransactionItem itemTransaction in currentDespacho.Items)
                    {
                        itemTransaction.Situacao = State.CheckManually;
                        itemTransaction.PaymentStateID = State.CheckManuallyID;
                        itemTransaction.StatusCarta = Status.LetterError;
                        itemTransaction.LetterDocStatusID = Status.LetterErrorID;
                        itemTransaction.Message = ex.Message;
                    }
                    Update update = new Update(config);
                    update.UpdateItemDB(currentDespacho.Items);                    
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"An unexpected error has occurred trying to generate documents: {ex.Message} - {ex.StackTrace}");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                currentItem++;
            }

        }


        private string CreateFxDocument(List<TransactionItem> items)
        {
            TransactionItem itemTransaction = items.FirstOrDefault();

            string downloadFolder = ProcessHelper.GetDonwloadFolder(config, itemTransaction);
            string output = Path.Combine(downloadFolder, $"FX_PULSE_{itemTransaction.Planta}_{itemTransaction.CodForn}_{itemTransaction.Despacho}_{DateTime.Now.ToString("yyyyMMdd")}.pdf");                                          
            if (File.Exists(output))
            {
                File.Delete(output);
            }
            decimal totalValue = 0;
            foreach (TransactionItem item in items)
            {
                totalValue += item.ValorME;
            }


            //Create FX Document
            FxData data = new FxData();
            data.Currency = itemTransaction.Moeda;
            data.Value = totalValue;

            if (itemTransaction.Fornecedor.ToLower().StartsWith("lear"))
            {
                data.IsLinkedCompany = true;
            } else
            {
                data.IsLinkedCompany = false;
            }
            // Data de Despacho
            DateTime dataEmbarque = DateTime.Parse(itemTransaction.DataEmbarque);
            DateTime dateLimit = DateTime.Parse("30/06/2020");
            if (DateTime.Compare(dataEmbarque, dateLimit) <= 0)
            {
                data.ImportsOption = "chkImportsOption1";
            }
            else
            {
                data.ImportsOption = "chkImportsOption2";
            }

            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter templetaFileName = project.GetParameterByName("FxFileTemplate");
            string template = Path.Combine(ProcessHelper.GetTemplateFolder(), templetaFileName.Value);
            string tempTemplate = Path.Combine(downloadFolder, "Temp", Path.GetFileName(template));
            if (!File.Exists(tempTemplate))
            {
                File.Copy(template, tempTemplate);
            }
            FxPulse fxPulse = new FxPulse(config);
            fxPulse.CreateFxPulseDocument(output, data, tempTemplate);
            try
            {
                File.Delete(tempTemplate);
            }
            catch (Exception){}

            return output;
        }

        private string CreateSmlDocument(List<TransactionItem> items)
        {

            List<TransactionItem> itemsBRL = items.FindAll(x => x.Moeda.ToUpper() == "BRL");
            if (itemsBRL == null || itemsBRL.Count == 0)
            {
                return null;
            }

            TransactionItem itemTransaction = itemsBRL.FirstOrDefault();

            string downloadFolder = ProcessHelper.GetDonwloadFolder(config, itemTransaction);
            string output = Path.Combine(downloadFolder, $"Carta_SML_{itemTransaction.Planta}_{itemTransaction.CodForn}_{itemTransaction.Despacho}_{DateTime.Now.ToString("yyyyMMdd")}.pdf");
            if (File.Exists(output))
            {
                File.Delete(output);
            }
            decimal totalValue = 0;
            foreach (TransactionItem item in itemsBRL)
            {
                totalValue += item.ValorME;
            }

            //Create SML Document (SISTEMA DE PAGOS / COBRO EN MONEDA LOCAL)
            SmlData data = new SmlData();
            data.Currency = itemTransaction.Moeda;
            data.Value = totalValue;

            if (itemTransaction.Fornecedor.ToLower().StartsWith("lear"))
            {
                data.IsLinkedCompany = true;
            }
            else
            {
                data.IsLinkedCompany = false;
            }
            // Data de Despacho
            DateTime dataEmbarque = DateTime.Parse(itemTransaction.DataEmbarque);
            DateTime dateLimit = DateTime.Parse("30/06/2020");
            if (DateTime.Compare(dataEmbarque, dateLimit) <= 0)
            {
                data.ImportsOption = "chkImportsOption1";
            }
            else
            {
                data.ImportsOption = "chkImportsOption2";
            }

            //Get data for Vendor
            DataTable vendorData = VendorCrossTableService.LoadVendorCrossTable(VENDOR_CROSS_TABLE, itemTransaction.CodForn);
            if (vendorData == null || vendorData.Rows.Count == 0)
            {
                throw new BusinessRuleException($"Erro ao gerar Carta SML. Fornecedor '{itemTransaction.CodForn}' nao encontrado na tabela RPA_VendorCrossTable.");
            }

            string supplierName = vendorData.Rows[0]["SupplierName"].ToString();
            string supplierNRLegalEntity = vendorData.Rows[0]["SupplierNRLegalEntity"].ToString();
            string supplierBankInformation = vendorData.Rows[0]["SupplierBankInformation"].ToString();
            string supplierBankAgency = vendorData.Rows[0]["SupplierBankAgency"].ToString();
            string supplierBankAccount = vendorData.Rows[0]["supplierBankAccount"].ToString();

            data.SupplierName = supplierName;
            data.SupplierNRLegalEntity = supplierNRLegalEntity;
            data.SupplierBankInformation = supplierBankInformation;
            data.SupplierBankAgency = supplierBankAgency;
            data.SupplierBankAccount = supplierBankAccount;

            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter templetaFileName = project.GetParameterByName("SmlFileTemplate");
            string template = Path.Combine(ProcessHelper.GetTemplateFolder(), templetaFileName.Value);
            string tempTemplate = Path.Combine(downloadFolder, "Temp", Path.GetFileName(template));
            if (!File.Exists(tempTemplate))
            {
                File.Copy(template, tempTemplate);
            }
            CartaSML cartaSML = new CartaSML();
            cartaSML.CreateSmlDocument(output, data, tempTemplate);
            try
            {
                File.Delete(tempTemplate);
            }
            catch (Exception) { }

            return output;
        }

        private string[] CreateDeclaracionDocs(string despacho, List<TransactionItem> items)
        {
            string[] docs = null;
            TransactionItem currentTransactionItem = items.FirstOrDefault();
            DateTime dataEmbarque = DateTime.Parse(currentTransactionItem.DataEmbarque);
            DateTime dateLimit = DateTime.Parse("01/11/2019");
            if (DateTime.Compare(dataEmbarque, dateLimit) <= 0)
            {
                docs = this.CreateDeclaracionJurada(despacho, items);
            } else
            {
                docs = this.CreateAnexoDespacho(despacho, items);
            }
            return docs;
        }

        private string[] CreateDeclaracionJurada(string despacho, List<TransactionItem> items)
        {
            TransactionItem item = items.FirstOrDefault();
            string downloadFolder = ProcessHelper.GetDonwloadFolder(config, item);
            string output = Path.Combine(downloadFolder, $"DECLARACION JURADA_{item.Planta}_{item.CodForn}_{item.Despacho}_{DateTime.Now.ToString("yyyyMMdd")}.pdf");
            if (File.Exists(output))
            {
                File.Delete(output);
            }

            decimal valorTotal = 0;
            foreach (var itemTransaction in items)
            {
                valorTotal += itemTransaction.ValorME;
            }

            DeclarationData data = new DeclarationData();
            data.ValorTotal = valorTotal;
            data.items = new List<DeclarationData.DeclarationDataItem>();
            data.items.Add(new DeclarationData.DeclarationDataItem()
            {
                Despacho = despacho,
                DataFechamento = item.DataEmbarque,
                Valor = valorTotal
            });

            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter templetaFileName = project.GetParameterByName("DeclaracionJuradaTemplate");
            string template = Path.Combine(ProcessHelper.GetTemplateFolder(), templetaFileName.Value);

            string tempTemplate = Path.Combine(downloadFolder, "Temp", Path.GetFileName(template));
            if (!File.Exists(tempTemplate))
            {
                File.Copy(template, tempTemplate);
            }

            DeclaracionJurada declaracionDoc = new DeclaracionJurada(config);
            string[] docs = declaracionDoc.CreateDeclarationDocument(output, data, tempTemplate);
            try
            {
                File.Delete(tempTemplate);
            }
            catch (Exception) { }

            return docs;
        }

        private string[] CreateAnexoDespacho(string despacho, List<TransactionItem> items)
        {
            TransactionItem item = items.FirstOrDefault();
            string downloadFolder = ProcessHelper.GetDonwloadFolder(config, item);
            string output = Path.Combine(downloadFolder, $"ANEXO DESPACHOS DE IMPORTACION_{item.Planta}_{item.CodForn}_{item.Despacho}_{DateTime.Now.ToString("yyyyMMdd")}.pdf");
            if (File.Exists(output))
            {
                File.Delete(output);
            }

            decimal valorTotal = 0;
            foreach (var itemTransaction in items)
            {
                valorTotal += itemTransaction.ValorME;
            }

            AnexoDespachoData data = new AnexoDespachoData();
            data.Currency = item.Moeda;
            data.ValorTotal = valorTotal;
            data.items = new List<AnexoDespachoData.AnexoDespachoDataItem>();
            data.items.Add(new AnexoDespachoData.AnexoDespachoDataItem()
            {
                Despacho = despacho,
                DataFechamento = item.DataEmbarque,
                Valor = valorTotal
            });

            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter templetaFileName = project.GetParameterByName("AnexoDespachosTemplate");
            string template = Path.Combine(ProcessHelper.GetTemplateFolder(), templetaFileName.Value);
            string tempTemplate = Path.Combine(downloadFolder, "Temp", Path.GetFileName(template));
            if (!File.Exists(tempTemplate))
            {
                File.Copy(template, tempTemplate);
            }

            AnexoDespacho anexoDespacho = new AnexoDespacho(config);
            string[] docs = anexoDespacho.CreateAnexoDespachoWorksheet(output, data, tempTemplate);
            try
            {
                File.Delete(tempTemplate);
            }
            catch (Exception) { }

            return docs;
        }


        private string GetBillOfLadingDocument(string despacho, List<TransactionItem> items)
        {
            TransactionItem item = items.FirstOrDefault();
            item.LoadDocuments();
            Document document = item.GetDocument(DocumentTypeListCode.BillOfLading);
            if (document != null)
            {
                return document.FullName;
            }            
            return null;
        }

        private string[] GetInvoiceDocuments(string despacho, List<TransactionItem> items)
        {
            List<string> invoices = new List<string>();

            foreach (TransactionItem itemTransaction in items)
            {
                itemTransaction.LoadDocuments();
                // Get the invoice document
                Document invoiceDocument = itemTransaction.GetDocument(DocumentTypeListCode.Invoice);
                if (invoiceDocument.Pages == null || invoiceDocument.Pages.Count == 0)
                {
                    // Load all pages for this invoice document
                    invoiceDocument.Pages = PaymentItemService.LoadPaymentInProgressDocumentPages(
                            itemTransaction.Id,
                            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.Id,
                            invoiceDocument.Id);
                }                
                // Get all pages that contains the invoice number reference
                List<DocumentPage> invoicePages = invoiceDocument.GetPagesByValue(
                    DocumentValueTypeListCode.InvoiceNumber,
                    itemTransaction.RefImportacao);

                if (invoicePages != null && invoicePages.Count > 0)
                {
                    foreach (var page in invoicePages)
                    {
                        invoices.Add(page.FullName);
                    }
                }                
            }
            return invoices.ToArray();
        }

        private void LoadAutoSettings()
        {
            AutoSettings settings = AutoSettingsService.LoadSettingsByCode("EnableDocument6401");
            if (settings != null)
            {
                if (string.IsNullOrEmpty(settings.Value))
                {
                    EnableDocument6401 = false;
                }
                else
                {
                    EnableDocument6401 = Convert.ToBoolean(settings.Value);
                }
            }

            //If document 6401 is enabled, load the other settings
            if (EnableDocument6401)
            {
                settings = AutoSettingsService.LoadSettingsByCode("Document6401DateFrom");
                if (settings != null)
                {
                    if (!string.IsNullOrEmpty(settings.Value))
                    {
                        Document6401DateFrom = Convert.ToDateTime(settings.Value);
                    }
                }
                settings = AutoSettingsService.LoadSettingsByCode("Document6401DateTo");
                if (settings != null)
                {
                    if (!string.IsNullOrEmpty(settings.Value))
                    {
                        Document6401DateTo = Convert.ToDateTime(settings.Value);
                    }
                }
                settings = AutoSettingsService.LoadSettingsByCode("Document6401Location");
                if (settings != null)
                {
                    if (!string.IsNullOrEmpty(settings.Value))
                    {
                        Document6401Location = settings.Value;
                    }
                }
            }
        }

        private string GetDocument6401(List<TransactionItem> items)
        {
            foreach (var item in items)
            {
                if (item.DataDocumento >= Document6401DateFrom && 
                    item.DataDocumento <= Document6401DateTo)
                {
                    //Find 'comprovante' by supplier code
                    string documentName = Path.Combine(Document6401Location, item.CodForn + ".pdf");
                    if (File.Exists(documentName))
                    {
                        return documentName;
                    }
                }
            }
            return null;
        }

    }
}
