﻿using ExtractPdfData;
using Lear.RpaMS.Data.DataServiceLayer.Document;
using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.CloudServices;
using Lear.RpaMS.Model.ModelLayer.Ocr;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TransactionData;

namespace AutomationFlow
{
    public class InvoiceProcess
    {
        private Config.ConfigManager config = null;

        public InvoiceProcess(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void Execute(ProcessItem processItem, 
            OcrProjectDocument ocrInvoice,
            ServiceResource azureService)
        {
            Console.WriteLine("######################################################################################");
            Console.WriteLine("Process to identify invoices started...");

            //filter items for this supplier and need to get information from invoices
            var itemsBySupplier = from item in processItem.Items
                                  where item.CodForn == processItem.Supplier && item.Planta == processItem.Plant &&
                                  item.PaymentStateID != State.WaitingBankDocumentID &&
                                  item.PaymentStateID != State.DocumentCreatedID &&
                                  item.PaymentStateID != State.DocumentSentToBankID &&
                                  !String.IsNullOrEmpty(item.Despacho)
                                  group item by item.Despacho into g
                                  select new { Despacho = g.Key, Items = g.ToList() };

            Console.WriteLine($"Supplier: {processItem.Supplier}");
            int total = itemsBySupplier.Count();
            Console.WriteLine($"Total Items: {total}");
            int currentItem = 1;
            Update transactionUpdate = new Update(config);

            List<DocumentPageDataItem> documentInvoices = null;
            if (total > 0)
            {
                var item = itemsBySupplier.FirstOrDefault();
                string familia2File = item.Despacho + "_Familia 2.pdf";
                DocumentValueData data = new DocumentValueData(this.config);
                documentInvoices = data.LoadPageValuesByFileName(familia2File);
            }

            foreach (var itemByDespacho in itemsBySupplier)
            {
                try
                {
                    Console.WriteLine($"'Despacho': {itemByDespacho.Despacho} {currentItem}/{total} - {DateTime.Now.ToString()}");
                    string documentsFolders = ProcessHelper.GetDonwloadFolder(this.config, itemByDespacho.Items.FirstOrDefault());
                    if (!Directory.Exists(documentsFolders))
                    {
                        throw new BusinessRuleException($"BusErr PROCT040 - Folder '{documentsFolders}' does not exists");
                    }
                    List<FileOcrData> invoices = this.GetInvoiceDocumentsSQL(
                        documentsFolders, 
                        itemByDespacho.Items.FirstOrDefault(),
                        ocrInvoice,
                        azureService);

                    foreach (var item in itemByDespacho.Items)
                    {
                        if (invoices == null || invoices.Count() == 0)
                        {
                            Console.WriteLine("Error: Invoice not found.");
                            //Invoice not found
                            item.StatusDocumentoInvoice = Status.NotFoundInvoice;
                            item.IdStatusDocumentoInvoice = Status.NotFoundInvoiceID;
                            item.Situacao = State.CheckManually;
                            item.PaymentStateID = State.CheckManuallyID;
                        }
                        else
                        {
                            //Try to extract invoice number
                            InvoiceManipulation.ExtractInvoiceNumberSQL(invoices, ocrInvoice);

                            Console.WriteLine("Invoices Found: " + invoices.Count());
                            //Invoices found
                            var invoicesFound = from inv in invoices
                                                where inv.OcrDataFound == true
                                                select inv;
                            Console.WriteLine("Invoice Numbers Found: " + invoicesFound.Count());

                            if (invoices.Count() == 1)
                            {
                                Console.WriteLine("Ok: Invoice found.");
                                //If only one file was found
                                item.Situacao = State.WaitingBankDocument;
                                item.PaymentStateID = State.WaitingBankDocumentID;
                            }
                            else
                            {
                                //Check if the current invoice was found
                                FileOcrData currentInvoice = invoices.Find((ele) =>
                                {
                                    long invoiceOcr, invoiceItem;
                                    bool isNumberOcr = long.TryParse(ele.InvoiceNumber, out invoiceOcr);
                                    bool isNumberItem = long.TryParse(item.RefImportacao, out invoiceItem);
                                    if (isNumberOcr && isNumberItem)
                                    {
                                        return invoiceOcr.Equals(invoiceItem);
                                    }
                                    return ele.InvoiceNumber == item.RefImportacao;
                                });
                                if (currentInvoice != null && currentInvoice.InvoiceNumber == item.RefImportacao)
                                {
                                    Console.WriteLine("Ok: Invoice found.");
                                    //If only one file was found
                                    item.Situacao = State.WaitingBankDocument;
                                    item.PaymentStateID = State.WaitingBankDocumentID;
                                }
                                else
                                {
                                    //Try to find the invoice in the Database because
                                    //the user could have set it manually
                                    Console.WriteLine("Trying to find INVOICE in the Database.");
                                    bool foundInvoiceDB = this.FindInvoiceDatabase(documentInvoices, item);
                                    if (foundInvoiceDB)
                                    {
                                        Console.WriteLine("Ok: Invoice found in the Database.");
                                        //If only one file was found
                                        item.StatusDocumentoInvoice = Status.DownloadOk;
                                        item.IdStatusDocumentoInvoice = Status.DownloadOkID;
                                        item.Situacao = State.WaitingBankDocument;
                                        item.PaymentStateID = State.WaitingBankDocumentID;
                                    }
                                    else
                                    {
                                        Console.WriteLine("Error: Invoice not found.");
                                        //Invoice not found
                                        item.StatusDocumentoInvoice = Status.NotFoundInvoice;
                                        item.IdStatusDocumentoInvoice = Status.NotFoundInvoiceID;
                                        item.Situacao = State.CheckManually;
                                        item.PaymentStateID = State.CheckManuallyID;
                                    }

                                }
                            }
                        }
                    }

                    //Update status of all items for this despacho
                    transactionUpdate.UpdateItemDB(itemByDespacho.Items);

                    // Insert Invoice Pages
                    TransactionItem itemInProgress = itemByDespacho.Items.FirstOrDefault();
                    if (itemInProgress.Documents == null || itemInProgress.Documents.Count == 0)
                    {
                        itemInProgress.Documents = PaymentItemService.LoadPaymentInProgressDocuments(
                            itemInProgress.Id,
                            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.Id);

                        Document invoiceDocument = itemInProgress.GetDocument(DocumentTypeListCode.Invoice);
                        if (invoiceDocument != null)
                        {
                            foreach (FileOcrData fileOcr in invoices)
                            {
                                DocumentPage page = new DocumentPage();
                                page.Id = DocumentService.GetOrCreatePage(invoiceDocument,
                                    Path.GetFileName(fileOcr.FileName),
                                    fileOcr.FileName);
                                if (fileOcr.OcrDataFound)
                                {
                                    DocumentService.GetOrCreatePageData(page,
                                        DocumentValueTypeListCode.InvoiceNumber,
                                        fileOcr.InvoiceNumber);
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"An unexpected error has occurred: {ex.Message} - {ex.StackTrace}");
                    Console.ForegroundColor = ConsoleColor.White;
                }

                currentItem++;
            }
        }

        //private List<FileOcrData> GetInvoiceDocuments(string folder, TransactionItem item)
        //{
        //    bool extractDataIfExists = false;

        //    List<FileOcrData> invoices = InvoiceManipulation.FindInvoices(folder,
        //                item,
        //                config,
        //                config.GetOcr().ocrTexts.invoices,
        //                extractDataIfExists);

        //    for (int i = 0; i < invoices.Count(); i++)
        //    {
        //        string inputFile = invoices[i].FileName;
        //        string[] values = Path.GetFileNameWithoutExtension(inputFile).Split('_');
        //        int pageNumber = int.Parse(values[2]);
        //        string invoiceName = $"INVOICE_{item.Despacho}_{pageNumber}.pdf";
        //        string output = Path.Combine(folder, invoiceName);

        //        if (File.Exists(output) && !extractDataIfExists)
        //        {
        //            invoices[i].FileName = output;
        //            continue;
        //        }

        //        PdfManipulation.ConvertImageToPDF(inputFile, output);
        //        invoices[i].FileName = output;
        //    }
        //    return invoices;
        //}

        private List<FileOcrData> GetInvoiceDocumentsSQL(string folder, 
            TransactionItem item, 
            OcrProjectDocument ocrInvoice,
            ServiceResource azureService)
        {
            bool extractDataIfExists = false;

            List<FileOcrData> invoices = InvoiceManipulation.FindInvoices(folder,
                        item,
                        config,
                        ocrInvoice.GetRulesDefinitionAsArray(),
                        extractDataIfExists,
                        azureService);

            for (int i = 0; i < invoices.Count(); i++)
            {
                string inputFile = invoices[i].FileName;
                string[] values = Path.GetFileNameWithoutExtension(inputFile).Split('_');
                int pageNumber = int.Parse(values[2]);
                string invoiceName = $"INVOICE_{item.Despacho}_{pageNumber}.pdf";
                string output = Path.Combine(folder, invoiceName);

                if (File.Exists(output) && !extractDataIfExists)
                {
                    invoices[i].FileName = output;
                    continue;
                }

                PdfManipulation.ConvertImageToPDF(inputFile, output);
                invoices[i].FileName = output;
            }
            return invoices;
        }


        private bool FindInvoiceDatabase(
            List<DocumentPageDataItem> documentInvoices,
            TransactionItem item) {

            bool invoiceFound = false;
            foreach (var documentValue in documentInvoices)
            {
                if (documentValue.Value.Equals(item.RefImportacao))
                {
                    invoiceFound = true;
                    break;
                }
            }
            return invoiceFound;
        }

    }
}
