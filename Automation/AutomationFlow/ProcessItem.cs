﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionData;

namespace AutomationFlow
{
    public class ProcessItem
    {

        public string Plant { get; set; }
        public string Supplier { get; set; }
        public string SupplierFolder { get; set; }

        public List<TransactionItem> Items { get; set; }

    }
}
