﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using TransactionData;
using ExtractPdfData;
using Lear.RpaMS.Model.ModelLayer.Ocr;
using Lear.RpaMS.Data.DataServiceLayer.Ocr;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.CloudServices;
using Lear.RpaMS.Data.DataServiceLayer.CloudServices;
using Lear.RpaMS.Model.ModelLayer.App;
using Lear.RpaMS.Data.DataServiceLayer.App;

namespace AutomationFlow
{
    public class ProcessTransaction
    {
        private Config.ConfigManager config = null;

        private OcrProject ocr = null;
        private OcrProjectDocument ocrInvoice = null;
        private ServiceResource azureService = null;
        private AppSettings despachoWebSettings = null;
        private AppSettingsCredential despachoWebCredential = null;

        public ProcessTransaction(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void Process(ProcessItem processItem)
        {
            InitAppSettings();

            //Try to download documents
            InvoiceDownloadProcess downloadProcess = new InvoiceDownloadProcess(config);
            try
            {
                downloadProcess.Execute(processItem, despachoWebSettings, despachoWebCredential);
            }
            catch (Exception ex)
            {
                Console.WriteLine("An exception has occurred when trying to download documents:" + ex.Message + " - " + ex.StackTrace);
            }
            try
            {
                //Try to update downloaded files
                downloadProcess.UpdateDownloadedFiles(processItem);
            }
            catch (Exception ex)
            {
                Console.WriteLine("An exception has occurred when trying to UPDATE STATUS download documents:" + ex.Message + " - " + ex.StackTrace);
            }

            this.InitOcr();
            this.InitAzureServices();

            try
            {
                //Try to find invoice from documents downloaded from COMEX web site.
                InvoiceProcess invoiceProcess = new InvoiceProcess(config);
                invoiceProcess.Execute(processItem, ocrInvoice, azureService);
            }
            catch (Exception ex)
            {
                Console.WriteLine("An exception has occurred when trying to FINDING Invoices:" + ex.Message + " - " + ex.StackTrace);
            }

            if (this.config.Get().GenerateDocuments)
            {
                Console.WriteLine("..................................................");
                Console.WriteLine("Creating 'FX' and 'Declaration' Documents");

                try
                {
                    GenerateLetterProcess generateLetterProcess = new GenerateLetterProcess(config);
                    generateLetterProcess.Execute(processItem);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An exception has occurred when trying to CREATE Documents:" + ex.Message + " - " + ex.StackTrace);
                }
            } else
            {
                Console.WriteLine("GenerateDocuments = False. No documents will be generated.");
            }


        }

        private void InitOcr()
        {         
            if (ocr == null)
            {
                ocr = OcrProjectService.LoadByProject(Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.Id);
                ocr.Documents = OcrProjectService.LoadDocuments(ocr.Id);
                ocrInvoice = ocr.GetDocumentByType(DocumentTypeListCode.Invoice);
            }
            if (ocr == null)
            {
                throw new BusinessRuleException("Ocr Project nao encontrado. Favor realizar configuracao na tabela RPA_OcrProject.");
            }
        }

        private void InitAzureServices()
        {
            if (azureService == null)
            {
                Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
                ProjectParameter parameter = project.GetParameterByName("AzureRecognizeTextService");
                azureService = ServiceResourceService.LoadByCode(parameter.Value);
            }
            if (azureService == null)
            {
                throw new BusinessRuleException("Azure Cloud Services nao encontrado. Favor realizar configuracao na tabela RPA_CloudServices.");
            }
        }

        private void InitAppSettings()
        {
            if (despachoWebSettings == null)
            {
                despachoWebSettings = AppSettingsService.LoadByCode("DespachoWebAppArgentina");
            }
            if (despachoWebCredential == null)
            {
                despachoWebCredential = AppSettingsService.GetCredential(
                    despachoWebSettings.Id,
                    Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser.UserName);
            }
        }

    }
}
