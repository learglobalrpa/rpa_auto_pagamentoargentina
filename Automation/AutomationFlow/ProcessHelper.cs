﻿using Lear.RpaMS.Data.DataServiceLayer.RPA;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TransactionData;

namespace AutomationFlow
{
    public static class ProcessHelper
    {

        public static string GetDonwloadFolder(Config.ConfigManager config, TransactionItem item)
        {
            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter parameter = project.GetParameterByName("DirectoryDownloadFile");
            FileDirectory directory = FileDirectoryService.LoadDirectoryByCode(parameter.Value);
            if (directory == null)
            {
                throw new Exception($"Erro: Diretorio para download nao encontrado: {parameter.Value} ");
            }
            string folder = Path.Combine(directory.Path, item.Despacho);
            return folder;            
        }

        public static string GetOutputFolder(Config.ConfigManager config)
        {
            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter parameter = project.GetParameterByName("DirectoryOutput");
            FileDirectory directory = FileDirectoryService.LoadDirectoryByCode(parameter.Value);
            if (directory == null)
            {
                throw new Exception($"Erro: Diretorio para OUTPUT nao encontrado: {parameter.Value} ");
            }
            return Path.Combine(directory.Path, DateTime.Now.ToString("yyyyMM"));            
        }

        public static string GetOutputFolderByPlantAndSupplier(Config.ConfigManager config, TransactionItem item)
        {
            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter parameter = project.GetParameterByName("DirectoryOutput");
            FileDirectory directory = FileDirectoryService.LoadDirectoryByCode(parameter.Value);
            if (directory == null)
            {
                throw new Exception($"Erro: Diretorio para OUTPUT nao encontrado: {parameter.Value} ");
            }
            string folder = Path.Combine(directory.Path, DateTime.Now.ToString("yyyyMM"));
            folder = Path.Combine(folder, item.Planta, item.CodForn);
            return folder;            
        }

        public static string GetTemplateFolder()
        {
            Project project = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject;
            ProjectParameter parameter = project.GetParameterByName("DirectoryTemplate");
            FileDirectory directory = FileDirectoryService.LoadDirectoryByCode(parameter.Value);
            if (directory == null)
            {
                throw new Exception($"Erro: Diretorio para TEMPLATE nao encontrado: {parameter.Value} ");
            }
            return directory.Path;            
        }

    }
}
