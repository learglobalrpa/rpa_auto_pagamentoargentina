﻿using AzureServices;
using ImageProcessor;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Xobject;
using Lear.RpaMS.Model.ModelLayer.CloudServices;
using Lear.RpaMS.Model.ModelLayer.Ocr;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using TransactionData;

namespace ExtractPdfData
{
    public class InvoiceManipulation
    {

        private static string OPTIMIZATION_SUFIX = "_opt";

        public static List<FileOcrData> FindInvoices(string folder, TransactionItem item,
                                        Config.ConfigManager config,
                                        string[] invoiceOcrText,
                                        bool extractDataIfExists,
                                        ServiceResource azureService)
        {
            string tempFolder = Path.Combine(folder, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            List<FileOcrData> invoices = null;
            bool applyOptimization = false;
            //extract images from PDF file
            string familia2File = Path.Combine(folder, item.Despacho + "_Familia 2.pdf");
            if (File.Exists(familia2File))
            {
                string[] imgs = ExtractImages(folder, tempFolder, familia2File, item, applyOptimization, extractDataIfExists);
                if (imgs != null && imgs.Length > 0)
                {                    
                    List<FileOcrData> fileOcrData = ExtractImageTextAzureRecognizeText(
                        imgs, 
                        azureService,
                        extractDataIfExists);
                    invoices = FindInvoiceByText(fileOcrData, invoiceOcrText);
                    if (applyOptimization)
                    {
                        for (int i = 0; i < invoices.Count(); i++)
                        {
                            string invoiceName = invoices[i].FileName;
                            string fileName = Path.GetFileName(invoiceName).Replace(OPTIMIZATION_SUFIX, "");
                            invoices[i].FileName = Path.Combine(Path.GetDirectoryName(invoiceName), fileName);
                        }
                    }
                }
            }
            return invoices;
        }


        private static string[] ExtractImages(string folder, string tempFolder, 
                                            string pdfFile, TransactionItem item,                                            
                                            bool applyOptimization = false,
                                            bool extractDataIfExists = true)
        {
            string[] imgs = null;
            using (var reader = new PdfReader(pdfFile))
            {
                using (PdfDocument pdfDoc = new PdfDocument(reader))
                {
                    imgs = new string[pdfDoc.GetNumberOfPages()];
                    for (int i = 0; i < pdfDoc.GetNumberOfPages(); i++)
                    {
                        int pageNumber = i + 1;
                        PdfPage page = pdfDoc.GetPage(pageNumber);
                        PdfResources resources = page.GetResources();

                        PdfImageXObject image = null;
                        byte[] imgBytes = null;
                        foreach (var resourceName in resources.GetResourceNames())
                        {
                            image = resources.GetImage(resourceName);
                            if (image != null)
                            {
                                imgBytes = image.GetImageBytes();
                                if (imgBytes != null && imgBytes.Length > 0)
                                {
                                    break;
                                }
                            }
                        }                        

                        if (imgBytes != null)
                        {
                            string output = Path.Combine(tempFolder, item.Despacho + "_Page_" + pageNumber + ".jpg");

                            if (File.Exists(output) && !extractDataIfExists)
                            {
                                imgs[i] = output;
                                continue;
                            }

                            File.WriteAllBytes(output, imgBytes);
                            if (applyOptimization)
                            {
                                output = OptimizeImageForOCR(output);
                            }
                            imgs[i] = output;                            
                        }

                    }
                }
            }
            return imgs;
        }

        private static string OptimizeImageForOCR(string fileName)
        {
            ImageFactory imageFactory = new ImageFactory();
            imageFactory.Load(fileName);
            imageFactory.Filter(ImageProcessor.Imaging.Filters.Photo.MatrixFilters.BlackWhite);
            int height = imageFactory.Image.Height;
            int width = imageFactory.Image.Width;

            int left = 1;
            int top = 1;
            int right = width - 10;
            int bottom = height / 2;

            ImageProcessor.Imaging.CropLayer cropLayer = new ImageProcessor.Imaging.CropLayer(
                                        left, top, right, bottom, ImageProcessor.Imaging.CropMode.Pixels);
            imageFactory = imageFactory.Crop(cropLayer);
            string output = Path.Combine(Path.GetDirectoryName(fileName),
                            Path.GetFileNameWithoutExtension(fileName) + OPTIMIZATION_SUFIX + Path.GetExtension(fileName));
            imageFactory.Save(output);

            return output;
        }        

        private static List<FileOcrData> ExtractImageTextAzureRecognizeText(string[] imgs,
            ServiceResource azureService,
            bool extractDataIfExists = true)
        {
            ComputerVisionClient computerVisionClient = new ComputerVisionClient(azureService);

            List<FileOcrData> fileOrcList = new List<FileOcrData>();

            foreach (var img in imgs)
            {
                string output = Path.ChangeExtension(img, "txt");
                if (File.Exists(output) && !extractDataIfExists)
                {
                    string text = File.ReadAllText(output);
                    string[] readLines = File.ReadAllLines(output);
                    fileOrcList.Add(new FileOcrData()
                    {
                        FileName = img,
                        TextResult = text,
                        Lines = readLines
                    });
                    continue;
                }

                //RecognitionTextResult result = computerVisionClient.RecognizeText(img);
                //string[] lines = RecognizeTextHelper.GetGetRecognizeTextResultLinesText(result);

                ReadTextResult result = computerVisionClient.ReadText(img);
                string[] lines = RecognizeTextHelper.GetReadAnalyzeResultLinesText(result);
                if (lines != null && lines.Length > 0)
                {
                    string text = String.Join(Environment.NewLine, lines);
                    File.WriteAllText(output, text);
                    fileOrcList.Add(new FileOcrData()
                    {
                        FileName = img,
                        TextResult = text,
                        Lines = lines
                    });
                    //Save json content
                    //output = Path.ChangeExtension(img, "json");
                    //File.WriteAllText(output, result.json);
                }
                                
            }
            return fileOrcList;
        }


        private static List<FileOcrData> FindInvoiceByText(List<FileOcrData> fileOcrData, string[] invoiceOcrText)
        {
            List<FileOcrData> listInvoices = new List<FileOcrData>();
            foreach (FileOcrData file in fileOcrData)
            {
                foreach (string text in invoiceOcrText)
                {
                    if (file.TextResult.ToLower().Contains(text.ToLower()))
                    {
                        listInvoices.Add(file);
                        break;
                    }
                }
            }
            return listInvoices;
        }
        

        public static void ExtractInvoiceNumberSQL(List<FileOcrData> invoiceFiles, 
            OcrProjectDocument ocrInvoiceDocument)
        {
            List<OcrDocumentValueRexExp> regExpressions = ocrInvoiceDocument.RexExpressions;
            bool found = false;

            foreach (FileOcrData invoice in invoiceFiles)
            {
                try
                {
                    string invoiceNo = null;
                    found = false;

                    //Attempt 1 - Try to find using Regular Expressions
                    foreach (OcrDocumentValueRexExp exp in regExpressions)
                    {
                        Regex regex = new Regex(exp.RegExpression);
                        // Call Match on Regex instance.
                        Match match = regex.Match(invoice.TextResult);
                        // Test for Success.
                        if (match.Success)
                        {
                            String value = match.Value;
                            if (!String.IsNullOrEmpty(exp.RemoveTextMatchValue))
                            {
                                value = value.Replace(exp.RemoveTextMatchValue, "");
                            }
                            value = value.Replace(" ", "");

                            invoiceNo = value;
                            found = true;
                            continue;
                        }
                    }

                    if (!found)
                    {
                        //Attempt 2 - Try to find using the line position and content
                        List<OcrDocumentValueTextPos> textPosition = ocrInvoiceDocument.TextPos;
                        foreach (OcrDocumentValueTextPos textPos in textPosition)
                        {
                            if (found)
                            {
                                break;
                            }

                            if (invoice.TextResult.ToLower().Contains(textPos.TextDefinition.ToLower()))
                            {
                                for (int i = 0; i < invoice.Lines.Length; i++)
                                {
                                    string line = invoice.Lines[i];
                                    if (line.ToLower().Contains(textPos.TextDefinition.ToLower()))
                                    {
                                        int pos = 0;
                                        if (textPos.Direction == "UP")
                                        {
                                            pos = i - textPos.JumpsNo;
                                        }
                                        else if (textPos.Direction == "DOWN")
                                        {
                                            pos = i + textPos.JumpsNo;
                                        }
                                        if (pos < 0 || pos > invoice.Lines.Length - 1)
                                        {
                                            break;
                                        }
                                        invoiceNo = invoice.Lines[pos];
                                        int number = 0;
                                        if (!String.IsNullOrEmpty(invoiceNo))
                                        {
                                            if (!String.IsNullOrEmpty(textPos.RegExpression))
                                            {
                                                Regex regex = new Regex(textPos.RegExpression);
                                                // Call Match on Regex instance.
                                                Match match = regex.Match(invoiceNo);
                                                // Test for Success.
                                                if (match.Success)
                                                {
                                                    String value = match.Value;

                                                    if (!String.IsNullOrEmpty(textPos.RemoveTextMatchValue))
                                                    {
                                                        value = value.Replace(textPos.RemoveTextMatchValue, "");
                                                    }
                                                    value = value.Replace(" ", "");

                                                    if (textPos.MustContainOnlyValue)
                                                    {
                                                        string lineText = invoice.Lines[pos];
                                                        string aux = lineText.Replace(value, "").Trim();
                                                        if (aux.Length > 0)
                                                        {
                                                            invoiceNo = "";
                                                            found = false;
                                                        }
                                                        else
                                                        {
                                                            invoiceNo = value;
                                                            found = true;
                                                            break;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        invoiceNo = value;
                                                        found = true;
                                                        break;
                                                    }

                                                }
                                                else
                                                {
                                                    invoiceNo = "";
                                                    found = false;
                                                }

                                            }
                                            else
                                            {
                                                invoiceNo = invoiceNo.Replace(" ", "");
                                                bool isNumber = int.TryParse(invoiceNo, out number);
                                                if (isNumber)
                                                {
                                                    found = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                        }

                    }
                    if (found)
                    {
                        //Check if is number. If true remove left zeros
                        int number = 0;
                        bool isNumber = int.TryParse(invoiceNo, out number);
                        if (isNumber)
                        {
                            invoiceNo = number.ToString();
                        }

                        Console.WriteLine("Invoice Number: " + invoiceNo);
                        //Rename file
                        invoice.InvoiceNumber = invoiceNo;
                        invoice.OcrDataFound = true;                        
                    }
                    else
                    {
                        invoice.InvoiceNumber = null;
                        invoice.OcrDataFound = false;
                        Console.WriteLine("Invoice Number Not Found. File: " + invoice.FileName);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Unexpected error trying to get invoice number: " + ex.Message + " - " + ex.StackTrace);
                }
            }

        }


    }
}
