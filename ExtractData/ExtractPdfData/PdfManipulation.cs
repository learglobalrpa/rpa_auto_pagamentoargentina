﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using iText.IO.Image;
using iText.Kernel.Pdf;
using iText.Kernel.Utils;
using iText.Layout;
using iText.Layout.Element;

namespace ExtractPdfData
{
    public static class PdfManipulation
    {

        public static void ConvertImageToPDF(string input, string output)
        {
            // Creating a PdfWriter                   
            PdfWriter writer = new PdfWriter(output);
            // Creating a PdfDocument       
            PdfDocument pdf = new PdfDocument(writer);
            // Creating a Document        
            Document document = new Document(pdf);
            // Creating an ImageData object
            ImageData data = ImageDataFactory.Create(input);
            // Creating an Image object
            Image image = new Image(data);
            // Adding image to the document       
            document.Add(image);
            // Closing the document       
            document.Close();
        }

        public static void CreatePDF(string output, string[] inputFiles)
        {
            FileStream fileStream = new FileStream(output, FileMode.Create);
            // Creating a PdfWriter
            PdfWriter writer = new PdfWriter(fileStream);
            // Creating a PdfDocument       
            PdfDocument pdf = new PdfDocument(writer);
            // Creating a PdfMerger
            PdfMerger merger = new PdfMerger(pdf).SetCloseSourceDocuments(true);                       

            foreach (var file in inputFiles)
            {
                if (!string.IsNullOrEmpty(file))
                {
                    PdfReader reader = new PdfReader(file);
                    PdfDocument pdfDoc = new PdfDocument(reader);
                    merger.Merge(pdfDoc, 1, pdfDoc.GetNumberOfPages());
                }
            }

            // Closing the document       
            pdf.Close();
        }

        private static int TotalPageCount(string file)
        {
            using (StreamReader sr = new StreamReader(File.OpenRead(file)))
            {
                Regex regex = new Regex(@"/Type\s*/Page[^s]");
                MatchCollection matches = regex.Matches(sr.ReadToEnd());
                return matches.Count;
            }
        }

    }
}
