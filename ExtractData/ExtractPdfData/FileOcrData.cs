﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtractPdfData
{
    public class FileOcrData
    {

        public string FileName { get; set; }
        public string TextResult { get; set; }
        public string[] Lines { get; set; }
        public string InvoiceNumber { get; set; }
        public bool OcrDataFound { get; set; }

    }
}
