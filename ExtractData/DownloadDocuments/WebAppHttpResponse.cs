﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DownloadDocuments
{
    public class WebAppHttpResponse
    {
        public string PageSource { get; set; }
        public CookieContainer Cookies { get; set; }

        public WebAppHttpResponse()
        {
            this.Cookies = new CookieContainer();
        }

    }
}
