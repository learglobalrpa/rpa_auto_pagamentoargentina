﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DownloadDocuments
{
    public class DespachoWeb
    {


        public CookieContainer Login()
        {
            string userName = "dcosta@lear.com";
            string password = "costa874";

            ASCIIEncoding encoding = new ASCIIEncoding();
            string postData = "username=" + userName + "&pass=" + password;
            byte[] postDataBytes = encoding.GetBytes(postData);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://bank2.alpha2000.com.ar/WebUtilsLogin/Login/");

            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/x-www-form-urlencoded";
            httpWebRequest.ContentLength = postDataBytes.Length;
            httpWebRequest.AllowAutoRedirect = false;

            using (var stream = httpWebRequest.GetRequestStream())
            {
                stream.Write(postDataBytes, 0, postDataBytes.Length);
                stream.Close();
            }

            var cookieContainer = new CookieContainer();

            using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {

                // Display the status.  
                Console.WriteLine(httpWebResponse.StatusDescription);

                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    // Read the content.  
                    string responseFromServer = streamReader.ReadToEnd();
                    // Display the content.  
                    Console.WriteLine(responseFromServer);

                    foreach (Cookie cookie in httpWebResponse.Cookies)
                    {
                        cookieContainer.Add(cookie);
                    }
                }
            }

            return cookieContainer;
        }


        public bool Login2(string url, string postData, string referer,
                                  string contentType, string accept, CookieContainer cookies)
        {
            WebAppHttpResponse resp = HttpMethods.Post(url, postData, referer, contentType, accept, cookies);
            
            if (!String.IsNullOrEmpty(resp.PageSource))
            {
                return true;
            }

            return false;
        }

        public CookieContainer Download(CookieContainer cookieContainer)
        {
            string postData = @"{NroDespacho:'20001IC04006495N',Familia:2}";
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] postDataBytes = encoding.GetBytes(postData);

            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create("http://bank2.alpha2000.com.ar/Despachos/DescargarPDFDigitalizado");
            httpWebRequest.Method = "POST";
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.ContentLength = postDataBytes.Length;
            httpWebRequest.AllowAutoRedirect = false;
            httpWebRequest.CookieContainer = cookieContainer;
            httpWebRequest.KeepAlive = true;
            httpWebRequest.Referer = "http://bank2.alpha2000.com.ar/Despachos/Index";
            httpWebRequest.Accept = "application/json, text/javascript, */*; q=0.01";

            using (var stream = httpWebRequest.GetRequestStream())
            {
                stream.Write(postDataBytes, 0, postDataBytes.Length);
                stream.Close();
            }

            using (var httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                // Display the status.  
                Console.WriteLine(httpWebResponse.StatusDescription);
                using (var streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    // Read the content.  
                    string responseFromServer = streamReader.ReadToEnd();
                    // Display the content.  
                    Console.WriteLine(responseFromServer);

                    foreach (Cookie cookie in httpWebResponse.Cookies)
                    {
                        cookieContainer.Add(cookie);
                    }
                }
            }

            return cookieContainer;
        }


        public WebAppHttpResponse Download2(string url, string postData, string referer,
                                            string contentType, string accept, CookieContainer cookies)
        {
            WebAppHttpResponse resp = HttpMethods.Post(url, postData, referer, contentType, accept, cookies);
            return resp;
        }


    }
}
