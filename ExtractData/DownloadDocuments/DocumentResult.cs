﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DownloadDocuments
{
    public class DocumentResult
    {
        public bool Existe { get; set; }
        public string Ruta { get; set; }
        public string ArchivoBase64 { get; set; }
        public string NombreArchivoDescarga { get; set; }
        public string LinkDescarga { get; set; }

    }
}
