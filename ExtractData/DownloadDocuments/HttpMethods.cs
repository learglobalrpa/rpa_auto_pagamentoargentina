﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DownloadDocuments
{
    public class HttpMethods
    {

        public static string Get(string url, string referer, CookieContainer cookies)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "GET";
            req.CookieContainer = cookies;
            req.UserAgent = "";
            req.Referer = referer;

            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            cookies.Add(resp.Cookies);

            string pageSrc;
            using (var streamReader = new StreamReader(resp.GetResponseStream()))
            {
                pageSrc = streamReader.ReadToEnd();
            }

            return pageSrc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="referer"></param>
        /// <param name="contentType">application/json; charset=utf-8 | application/x-www-form-urlencoded</param>
        /// <param name="accept">*/* | application/json, text/javascript, */*; q=0.01</param>
        /// <param name="cookies"></param>
        /// <returns></returns>
        public static WebAppHttpResponse Post(string url, string postData, string referer, 
                                  string contentType, string accept,
                                  CookieContainer cookies)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.Method = "POST";
            req.CookieContainer = cookies;
            req.UserAgent = "";
            req.Referer = referer;
            req.ContentType = contentType;
            req.Accept = accept;
            req.Timeout = 1000 * 3600; //1 hora

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] postDataBytes = encoding.GetBytes(postData);

            using (var stream = req.GetRequestStream())
            {
                stream.Write(postDataBytes, 0, postDataBytes.Length);
                stream.Close();
            }

            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            cookies.Add(resp.Cookies);

            int totalBytes = (int) resp.ContentLength;

            string pageSrc;
            using (var streamReader = new StreamReader(resp.GetResponseStream()))
            {
                int receivedBytes = 0;
                byte[] buffer = new byte[totalBytes];
                MemoryStream ms = new MemoryStream();              
                int read = 0;
                while ((read = streamReader.BaseStream.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                    receivedBytes += read;
                }
                pageSrc = Encoding.Default.GetString(ms.ToArray());
            }
                       
            WebAppHttpResponse webAppResp = new WebAppHttpResponse();
            webAppResp.Cookies.Add(resp.Cookies);
            webAppResp.PageSource = pageSrc;

            return webAppResp;
        }

    }
}
