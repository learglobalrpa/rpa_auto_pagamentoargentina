'use strict';

const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../configManager");
const fs = require("fs");

let browser = null;
let page = null;

const init = async() => {
    browser = await puppeteer.launch({headless: false, args: ['--start-maximized']});
    const browserWSEndpoint = browser.wsEndpoint();
    console.log(browserWSEndpoint);
    
    fs.writeFile("session.txt", browserWSEndpoint, (err) => {
        if (err) console.log(err);
        console.log("Successfully Written to File.");
    });

    page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 600 });

    let config = new ConfigManager();
    config.init();

    const url = config.get("despachoWebApp.url");
    await page.goto(url, {waitUntil: 'load', timeout: 0});
    page.setDefaultNavigationTimeout(120000);

    const user = config.get("despachoWebApp.user");
    const pwd = config.get("despachoWebApp.pwd");
    await page.type('#UserName', user);
    await page.type('#Password', pwd);        
    await page.click('#divIniciar > button');

    await page.waitForNavigation();
    
    browser.disconnect();
};


(async() => {    
    await init();

    console.log("Browser started!!")
})();