const fs = require('fs')

let page = null;
let config = null;
let timeout = 0;

class DownloadPDFTest {

    setPage(_page) {
        page = _page;
    }

    setConfig(_config) {
        config = _config;
    }

    async init(_config, _page) {
        config = _config;
        page = _page;
        timeout = 1000 * 3600;
    }

    async download(item) {
        await page.setRequestInterception(true);
        page.on("request", request => {
            var data = {
                NroDespacho: item.despacho,
                Familia: 2
            }
            var options = {
                method: 'POST',
                headers: { ...request.headers(), "content-type": "application/json; charset=utf-8"},
                postData: JSON.stringify(data)
            };
            request.continue(options);
        });

        const response = await page.goto("http://bank2.alpha2000.com.ar/Despachos/DescargarPDFDigitalizado",
                                {waitUntil: 'networkidle0', timeout: timeout});
        let responseBody = await response.text();
        if (responseBody) {
            if (responseBody.indexOf("McAfee Web Gateway is downloading and scanning.") > 0) {
                console.log("McAfee Web Gateway was found...")
            } else {
                responseBody = JSON.parse(responseBody);
                if (responseBody.Existe) {
                    let fileName = "C:\\RPA\\Dev\\#Projects\\AutorizacaoPagamentoFaturaArgentina\\Data\\Output\\" + responseBody.NombreArchivoDescarga;
                    fs.writeFile(fileName, responseBody.ArchivoBase64, {encoding: 'base64'}, function(err) {
                        console.log('File created');
                    });
                } else {
    
                }
            }
        }
        page.removeAllListeners("request");
        await new Promise(resolve => setTimeout(resolve, 30000));
    }
    
}

module.exports = DownloadPDFTest;