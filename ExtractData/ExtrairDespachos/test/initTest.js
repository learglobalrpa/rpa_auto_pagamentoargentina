'use strict';

const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../configManager");
const ReadQueueDB = require("../dataLayer/readQueueDB");
const DespachosWebApp = require("../despachosWebApp");
const fs = require("fs");

let browser = null;
let page = null;
let config = null;
let queue = null;

const init = async() => {
    const browserWSEndpoint = await new Promise(function(resolve, reject) {
        fs.readFile("session.txt", "utf-8", (err, data) => {
            resolve(data);
        });
    });    
    // Use the endpoint to reestablish a connection
    browser = await puppeteer.connect({browserWSEndpoint, width: "1366", height: 600}, {});
    page = (await browser.pages())[0];
    await page.setViewport({ width: 1366, height: 600 });

    //console.log(await page.content());
};

const readInputDataDB = async() => {
    const readQueueDB = new ReadQueueDB();
    queue = await readQueueDB.getQueue(config);
}

(async() => {
    try {
        config = new ConfigManager();
        config.init();

        await init();

        try {            
            await readInputDataDB();
        } catch (error) {
            console.log("Error trying to get Queue: " + error);
        }
    
        //check if there are items to be processed
        if (!queue || queue.length == 0) {
            console.log("There are no items to be processed. Check the Queue excel file.")            
        }
        
        const despachoWeb = new DespachosWebApp();
        despachoWeb.setBrowser(browser);
        despachoWeb.setPage(page);
        despachoWeb.setConfig(config);
        await despachoWeb.getInfoDespachos(queue);       
    
        browser.disconnect();
    
        console.log("Test Finished!!")
        
    } catch (error) {
        console.log("Error: " + error.message + " - " + error.stack);
    }
})();