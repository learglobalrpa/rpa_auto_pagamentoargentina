'use strict';

const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../configManager");
const ReadQueue = require("../readQueue");
const DespachosWebApp = require("../despachosWebApp");
const DownloadPDFTest = require("./downloadPDFTest");

const fs = require("fs");

let browser = null;
let page = null;
let config = null;
let queue = null;

const init = async() => {
    const browserWSEndpoint = await new Promise(function(resolve, reject) {
        fs.readFile("session.txt", "utf-8", (err, data) => {
            resolve(data);
        });
    });    
    // Use the endpoint to reestablish a connection
    browser = await puppeteer.connect({browserWSEndpoint, width: "1366", height: 600}, {});
    page = (await browser.pages())[0];
    await page.setViewport({ width: 1366, height: 600 });

    //console.log(await page.content());
};

const readInputData = async() => {
    const readQueue = new ReadQueue();
    queue = await readQueue.getQueue(config);
}

const readDonlowadQueue = async() => {
    const readQueue = new ReadQueue();
    queue = await readQueue.getDownloadQueue(config);
}

const downloadDocuments = async () => {
    var loop = Array(10);
    try {
        const despachoWeb = new DespachosWebApp();
        despachoWeb.setBrowser(browser);
        despachoWeb.setPage(page);
        despachoWeb.setConfig(config);        
        
        let attempt = 0;

        for await (var i of loop) {
            try {
                attempt++;
                console.log(`Started attempt ${attempt} to download documents....`);
                try {
                    await readDonlowadQueue();
                } catch (error) {
                    queue = [];
                    console.log("Error trying to get Download Queue: " + error);
                }    
                //check if there are items to be processed
                if (!queue || queue.length == 0) {
                    console.log("Nao existem itens para realizar download dos documentos de despacho.")
                }
                await despachoWeb.downloadDocumentsNew(queue);
            } catch (error) {
                console.log("Error when trying to download." + error.message + " - " + error.stack);
            }
        }        
    } catch (error) {
        console.log("<< NEW >> Error trying to Download Documents: " + error.message + " - " + error.stack);
    }

}

(async() => {
    try {
        config = new ConfigManager();
        config.init();

        await init();

        await downloadDocuments();

        // try {
        //     await readInputData();
        // } catch (error) {
        //     console.log("Error trying to get Queue: " + error);
        // }    
                
        // const despachoWeb = new DespachosWebApp();
        // despachoWeb.setBrowser(browser);
        // despachoWeb.setPage(page);
        // despachoWeb.setConfig(config);
        
        // try {
        //     await readDonlowadQueue();
        // } catch (error) {
        //     console.log("Error trying to get Download Queue: " + error);
        // }    
        // //check if there are items to be processed
        // if (!queue || queue.length == 0) {
        //     console.log("Nao existem itens para realizar download dos documentos de despacho.")
        // }        
        // try {
        //     const downloadPDF = new DownloadPDFTest();
        //     downloadPDF.init(config, page);
        //     // let despacho = "19073IC04149369P";
        //     // console.log("donwloading despacho: " + despacho);
        //     // await downloadPDF.download(despacho);
        //     for await (const item of queue) {
        //         try {
        //             console.log("donwloading despacho: " + item.despacho);
        //             await downloadPDF.download(item);
        //         } catch (error) {
        //             console.log("Error trying to download descpaho: " + item.despacho);
        //         }
        //     }         
        // } catch (error) {
        //     console.log("Error trying to Download Documents: " + error.message + " - " + error.stack);
        // }
    
        browser.disconnect();
    
        console.log("Test Finished!!")
        
    } catch (error) {
        console.log("Error: " + error.message + " - " + error.stack);
    }
})();