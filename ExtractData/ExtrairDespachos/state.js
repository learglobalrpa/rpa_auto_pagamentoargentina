class State {

    static get WaitingShipmmentNo() {
        return "Aguardando numero do despacho";
    } 

    static get WaitingShipmmentNoID() {
        return 1;
    } 

    static get WaitingDocumentDownload() {
        return  "Aguardando download";
    }

    static get WaitingDocumentDownloadID() {
        return 2;
    }

    static get DownloadOk() {
        return "Download realizado com sucesso";
    }

    static get DownloadOkID() {
        return 3;
    }

    static get CheckManually() {
        return "Verificar Manualmente";
    }

    static get CheckManuallyID() {
        return 4;
    }

    static get WaitingBankDocument() {
        return "Aguardando documento para banco";
    }

    static get WaitingBankDocumentID() {
        return 5;
    }

    static get WaitingDocumentScanning() {
        return 9;
    }

}

module.exports = State;