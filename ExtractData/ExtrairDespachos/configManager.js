const fs = require('fs');
const configFile = "../../Data/Config/config.json";
var config = null;


class ConfigManager {

    
    init() {
        let rawdata = fs.readFileSync(configFile);
        config = JSON.parse(rawdata);
    }

    get(property) {

        const getImpl= function(object, property) {
            let t = this,
                elems = Array.isArray(property) ? property : property.split('.'),
                name = elems[0],
                value = object[name];
            if (elems.length <= 1) {
              return value;
            }
            // Note that typeof null === 'object'
            if (value === null || typeof value !== 'object') {
              return undefined;
            }
            return getImpl(value, elems.slice(1));
          };        

        return getImpl(config, property);
    }

    getInputFileName() {
      const inputFolder = this.get("inputFolder");
      const inputFile = this.get("excelLiberados");
      return "..\\..\\" + inputFolder + "\\" + inputFile;
    }

    getOutputFolder() {
      const outputFolder = this.get("outputFolder");      
      return "..\\..\\" + outputFolder;
    }

    getDownloadDocumentsFolder() {
      const downloadFolder = this.get("downloadDocumentsFolder");
      return "..\\..\\" + downloadFolder;
    }

}

module.exports = ConfigManager;