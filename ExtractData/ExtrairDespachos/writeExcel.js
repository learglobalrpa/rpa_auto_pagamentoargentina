const Excel = require("../../node_modules/exceljs/lib/exceljs.nodejs");

class WriteExcel {

    async updateFile(fileName, sheetName, cell, value) {
        var wb = await new Excel.Workbook().xlsx.readFile(fileName);
        const ws = wb.getWorksheet(sheetName);
        ws.getCell(cell).value = value;
        await wb.xlsx.writeFile(fileName);
    }

}

module.exports = WriteExcel;