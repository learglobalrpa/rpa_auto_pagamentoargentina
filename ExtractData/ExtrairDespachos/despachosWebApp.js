const puppeteer = require("../../node_modules/puppeteer");
const PaymentDB = require("./dataLayer/paymentDB");

let browser = null;
let page = null;
let config = null;
let paymentDB = null;
let appSettings = null;
let appSettingsCredential = null;

class DespachosWebApp {

    async init(configManager) {
        config = configManager;
        browser = await puppeteer.launch({ headless: appSettings.IsHeadless, args: ['--start-maximized'] });
        page = await browser.newPage();
        await page.setViewport({ width: 1366, height: 600 });
        const url = appSettings.Url;
        await page.goto(url, { waitUntil: 'load', timeout: 0 });
        page.setDefaultNavigationTimeout(240000);
    }

    setBrowser(_browser) {
        browser = _browser;
    }

    setPage(_page) {
        page = _page;
    }

    setConfig(_config) {
        config = _config;
    }

    setAppSettings(despachoWebApp) {
        appSettings = despachoWebApp;
    }

    setAppCredential(despachoWebCredential) {
        appSettingsCredential = despachoWebCredential;
    }

    async login() {
        const user = appSettingsCredential.UserID;
        const pwd = appSettingsCredential.UserPwd;
        await page.type('#UserName', user);
        await page.type('#Password', pwd);
        await page.click('#divIniciar > button');

        await page.waitForNavigation();
    }

    async close() {
        if (browser != null) {
            await browser.close();
        }
    }

    async getInfoDespachos(list) {
        if (list && list.length > 0) {
            
            paymentDB = new PaymentDB();
            paymentDB.init(config);

            await this.cleanOfficialDate();
            for await (const item of list) {
                await this.searchByNroReferencia(item);
            }
        }
    }


    async cleanOfficialDate() {        
        await new Promise(resolve => setTimeout(resolve, 1000));
        await page.click('#btnLimpiarCampoCampoBusquedaGrilla_6_Despachos');        
        await new Promise(resolve => setTimeout(resolve, 500));
    }

    async searchByNroReferencia(item) {
        let nrRef = item.ShipmentNumber;
        if (nrRef) {
            //click delete button
            await page.click('#btnLimpiarCampoCampoBusquedaGrilla_2_Despachos > i');            
            await new Promise(resolve => setTimeout(resolve, 500));
            await page.click("#CampoBusquedaGrilla_2_Despachos_tagsinput");
            await page.type("#CampoBusquedaGrilla_2_Despachos_tagsinput", nrRef);
            await new Promise(resolve => setTimeout(resolve, 500));
            await page.click("#CampoBusquedaGrilla_3_Despachos_tagsinput");

            //Find and click button Buscar
            var btnBuscar = await page.$("div[class='WebUtilsBoton '][onclick='BuscarGrillaDespachos();']");
            await btnBuscar.evaluate((element) => {
                element.click();
            });
            await new Promise(resolve => setTimeout(resolve, 1000 * 15));
            //Get results
            var result = await this.getResults();
            if (!result) {
                throw "Error trying to get shipment information. Check if is logged.";
            }
            
            await this.updateQueueDB(item, result);
        }

    }

    async getResults() {

        var result = {
            isFound: false,
            reasonNotFound: "",
            item: null
        };

        var divResults = await page.$("#divSinResultadosGrillaDespachos");
        if (divResults) {
            var isVisible = await divResults.evaluate((element) => {
                if (element.style.display != "none") {
                    return true;
                }
                return false;
            });
            if (isVisible) {
                result.isFound = false;
                result.reasonNotFound = "#NOTFOUND";                
                return result;

            } else {

                const data = await page.evaluate(() => {
                    var items = [];
                    const rows = document.querySelectorAll('#tableGrillaDespachos > tbody > tr');
                    debugger;
                    if (rows && rows.length > 0) {
                        for (let index = 1; index < rows.length; index++) {
                            const row = rows[index];
                            const cells = row.cells;
                            var item = {};
                            item.NroDespacho = cells[0].innerText;
                            item.FechaOficializacion = cells[1].innerText;
                            item.StatusAFIP = cells[cells.length - 1].innerText;
                            items.push(item);
                        }
                    }
                    return items;
                });
                if (data && data.length > 0) {
                    var item = data.filter(item => {
                        if (item.StatusAFIP == "DIGI" || item.StatusAFIP == "ENDO") {
                            return item;
                        }
                    });
                    if (item && item.length > 0) {
                        result.isFound = true;
                        result.reasonNotFound = "";
                        result.item = item[0];
                        return result;
                    } else {
                        result.isFound = false;
                        result.reasonNotFound = "#NOTDIGI";                        
                        return result;
                    }
                }
            }
        }
    }    

    async updateQueueDB(item, result) {
        await paymentDB.updateStatusSearchItem(item, result);        
    }   

}

module.exports = DespachosWebApp;