const ImportExcel = require("./readExcel");
const State = require("./state");

class ReadQueue {

  async readExcel(config) {
    let inputExcelLiberados = config.getInputFileName();
    const importExcel = new ImportExcel();
    let list = await importExcel.loadAsObject(inputExcelLiberados, null);
    //create the row index for excel
    list.forEach((element, index, array) => {
      element.row = index + 2;
    });

    return list;
  }


  async getQueue(config) {
    let list = await this.readExcel(config);
    let approvedColumnName = config.get("liberadosConfig.approvedColumnName").toLowerCase();
    let approvedText = config.get("liberadosConfig.approvedText").toLowerCase();
    let nroEmbarqueColumnName = config.get("excelInput.columnNameNroReferencia").toLowerCase();
    let situacaoColumnName = config.get("excelInput.columnNameSituacao").toLowerCase();
    //get all items that were approved to payment, have ship no and the status is waiting 
    //for ship no information
    let output = list.filter(item => {
      if (item[approvedColumnName]
        && item[approvedColumnName].toLowerCase() == approvedText
        && item[nroEmbarqueColumnName]
        && (!item[situacaoColumnName] || item[situacaoColumnName] == State.WaitingShipmmentNo)) {
        return item;
      }
    });
    return output;
  }

  async getDownloadQueue(config) {
    let list = await this.readExcel(config);
    let situacaoColumnName = config.get("excelInput.columnNameSituacao").toLowerCase();
    //Get all items that are in state waiting for download
    let output = list.filter(item => {
      if (item[situacaoColumnName].replace(/\s/g,'') == State.WaitingDocumentDownload.replace(/\s/g,'')) {
        return item;
      }
    });
    return output;
  }

}

module.exports = ReadQueue;