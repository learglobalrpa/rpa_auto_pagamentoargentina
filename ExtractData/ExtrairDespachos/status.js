class Status {

    static get NotFound() {
        return "Nao encontrado para download";
    } 
    
    static get DownloadOk() {
        return  "OK";
    }

    static get MoreThanOneDocument() {
        return  "Erro: Mais de um documento encontrado";
    }

    static get NotFoundInvoice() {
        return  "Erro: Invoice nao encontrada no arquivo Familia 2";
    }    

}

module.exports = Status;