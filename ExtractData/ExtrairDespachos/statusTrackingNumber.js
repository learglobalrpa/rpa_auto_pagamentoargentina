class StatusTrackingNumber {

    static get TrackingNotFound() {
        return "Falta numero Despacho";
    }

    static get TrackingNotFoundID() {
        return 1;
    }
    
    static get TrackingNotScanned() {
        return  "Nao esta digitalizado";
    }

    static get TrackingNotScannnedID() {
        return 2;
    }

    static get TrackingFound() {
        return "Numero Despacho encontrado";
    }

    static get TrackingFoundID() {
        return 3;
    }

}

module.exports = StatusTrackingNumber;