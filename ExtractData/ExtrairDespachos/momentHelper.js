const moment = require("moment");

class MomentHelper {

    static getDateAndTime() {
        moment.locale("pt-br");
        return moment().format('L') + " " + moment().format('LTS')
    }

    static getDateAsFolderName() {
        moment.locale("pt-br");
        return moment().format("YYYYMMDD");   
    }


}

module.exports = MomentHelper;