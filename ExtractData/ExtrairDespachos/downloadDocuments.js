const fs = require('fs')
//const MomentHelper = require("./momentHelper");
const WriteExcel = require("./writeExcel");
const State = require("./state");
const Status = require("./status");

let page = null;
let config = null;
let timeout = 0;


class DownloadDocuments {

    setPage(_page) {
        page = _page;
    }

    setConfig(_config) {
        config = _config;
    }

    async init(_config, _page) {
        config = _config;
        page = _page;

        timeout = 1000 * 3600;
    }

    async download(item) {
        const columnName = config.get("excelInput.columnNameNroDespacho").toLowerCase();
        let nroDespacho = item[columnName];
        await this.searchByNroDespacho(item, nroDespacho);
        var result = await this.clickLinkDespacho(item, nroDespacho);
        if (result && result == "OK") {
            await this.clickDigitalizacion();
            await new Promise(resolve => setTimeout(resolve, 1000));
            await this.downloadFamilia2(item, nroDespacho);
            await new Promise(resolve => setTimeout(resolve, 1000));
            await this.closeDialog();
            await new Promise(resolve => setTimeout(resolve, 1000));
        } else {

        }
    }

    async searchByNroDespacho(item, nroDespacho) {
        if (nroDespacho) {
            //click delete button of Nro. Despacho	
            await page.click('#btnLimpiarCampoCampoBusquedaGrilla_1_Despachos > i');
            await new Promise(resolve => setTimeout(resolve, 500));
            await page.click("#CampoBusquedaGrilla_1_Despachos_tagsinput");
            await page.type("#CampoBusquedaGrilla_1_Despachos_tagsinput", nroDespacho);
            await new Promise(resolve => setTimeout(resolve, 500));
            await page.click("#CampoBusquedaGrilla_3_Despachos_tagsinput");

            //Find and click button Buscar
            var btnBuscar = await page.$("div[class='WebUtilsBoton '][onclick='BuscarGrillaDespachos();']");
            await btnBuscar.evaluate((element) => {
                element.click();
            });
            await new Promise(resolve => setTimeout(resolve, 5000));
        }
    }

    async clickLinkDespacho(item, nroDespacho) {
        var linkDespacho = await page.$("#Despacho-NroDespacho");
        var result = await linkDespacho.evaluate((element, nroDespacho) => {
            if (element.innerText == nroDespacho) {
                element.click();
                return "OK";
            }
            return null;
        }, nroDespacho);
        await new Promise(resolve => setTimeout(resolve, 5000));
        return result;
    }

    async clickDigitalizacion() {
        var linkDespacho = await page.$("#btnTabTabsDespachoDigitalizacion");
        await linkDespacho.evaluate((element) => {
            element.click();
        });
    }

    async downloadFamilia2(item, nroDespacho) {
        var linkFamilia2 = await page.$("div[class*='WebUtilsBotonDescargaIconoTexto3'][onclick='DescargarPDFDigitalizado(2);']");
        await linkFamilia2.evaluate((element) => {
            element.click();
        });

        let downloadFileName = config.get("despachoWebApp.downloadFolder");
        //19084IC05004215V_Familia 2.pdf
        downloadFileName += nroDespacho + "_Familia 2.pdf";

        let index = 1;
        while (!fs.existsSync(downloadFileName) && index < 120000) {
            await new Promise(resolve => setTimeout(resolve, 5000));
            index++;
        }
        await this.closeDescargandoDialog();
    }

    async downloadFamilia3(item) {

    }

    async closeDescargandoDialog() {
        var closeButton = await page.$("body > div:nth-child(104) > div.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix > a > span");
        await closeButton.evaluate((element) => {
            element.click();
        });
    }

    async closeDialog() {
        var closeButton = await page.$("body > div.ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-draggable.ui-resizable > div.ui-dialog-titlebar.ui-widget-header.ui-corner-all.ui-helper-clearfix > a > span");
        await closeButton.evaluate((element) => {
            element.click();
        });
    }


    // async donwloadNew(queue) {

    //     for await (const item of queue) {
    //         console.log(`Donwloading files to despacho: ${item.despacho}`);
    //         try {
    //             let statusDocInvoiceColumnName = config.get("excelInput.columnNameStatusDocInvoice").toLowerCase();
    //             if (!item[statusDocInvoiceColumnName]) {
    //                 let result = await this.donwloadFamilia(item, queue, 2);
    //                 if (result == "OK") {
    //                     console.log("Download file Familia 2 realizado com sucesso!");
    //                     await this.updateDownloadStatus(item, {
    //                         fileType: "INVOICE",
    //                         status: Status.DownloadOk
    //                     });
    //                 } else if (result == "#NOTFOUND") {
    //                     console.log("File Familia 2 not found!");
    //                     await this.updateDownloadStatus(item, {
    //                         fileType: "INVOICE",
    //                         status: Status.NotFound
    //                     });
    //                 }
    //             } else {
    //                 console.log(`File Familia 2 already download or not found.`);
    //             }

    //         } catch (error) {
    //             console.log("Error trying to download documento Familia 2:" + error.message + " - " + error.stack);
    //             page.removeAllListeners("request");
    //         }

    //         try {
    //             let statusDocCteColumnName = config.get("excelInput.columnNameStatusDocCTE").toLowerCase();
    //             if (!item[statusDocCteColumnName]) {
    //                 let result = await this.donwloadFamilia(item, queue, 3);
    //                 if (result == "OK") {
    //                     console.log("Download file Familia 3 realizado com sucesso!");
    //                     await this.updateDownloadStatus(item, {
    //                         fileType: "CTE",
    //                         status: Status.DownloadOk
    //                     });
    //                 } else if (result == "#NOTFOUND") {
    //                     console.log("File Familia 3 not found!");
    //                     await this.updateDownloadStatus(item, {
    //                         fileType: "CTE",
    //                         status: Status.NotFound
    //                     });
    //                 }
    //             } else {
    //                 console.log(`File Familia 3 already download or not found.`);
    //             }

    //         } catch (error) {
    //             console.log("Error trying to download documento Familia 3:" + error.message + " -" + error.stack);
    //             page.removeAllListeners("request");
    //         }


    //         try {
    //             let statusDocInvoiceColumnName = config.get("excelInput.columnNameStatusDocInvoice").toLowerCase();
    //             let statusDocCteColumnName = config.get("excelInput.columnNameStatusDocCTE").toLowerCase();
    //             if (item[statusDocInvoiceColumnName] == "OK" &&
    //                 item[statusDocCteColumnName] == "OK") {

    //                 await this.updateState(item, {
    //                     state: State.DownloadOk
    //                 });

    //             } else {

    //                 if (item[statusDocInvoiceColumnName] == Status.NotFound &&
    //                     item[statusDocCteColumnName] == Status.NotFound) {

    //                     await this.updateState(item, {
    //                         state: State.CheckManually
    //                     });
    //                 }
    //             }

    //         } catch (error) {
    //             console.log("Error trying to update 'Situacao':" + error.message + " -" + error.stack);
    //         }
    //     }
    // }

    async donwloadNew(queue) {
        let documentList = [];
        //INVOICE
        documentList.push({
            fileType: "INVOICE",
            family: 2,
            statusDocColumnName: config.get("excelInput.columnNameStatusDocInvoice").toLowerCase()
        });
        //CONHECIMENTO DE TRANSPORTE
        documentList.push({
            fileType: "CTE",
            family: 3,
            statusDocColumnName: config.get("excelInput.columnNameStatusDocCTE").toLowerCase()
        });

        for await (const item of queue) {
            console.log(`Donwloading files to despacho: ${item.despacho}`);
            //Browse all documents to download
            for await (const doc of documentList) {
                try {
                    let statusDocColumnName = doc.statusDocColumnName;

                    //Check if file has already been downloaded
                    let folderName = this.getDonwloadFolder(item);
                    let fileName = folderName + "\\" + `${item.despacho}_Familia ${doc.family}.pdf`;
                    if (fs.existsSync(fileName)) {
                        console.log(`File ${fileName} has alerady been downloaded!`);
                        continue;
                    }

                    if (!item[statusDocColumnName]) {
                        let result = await this.donwloadFamilia(item, queue, doc.family);
                        if (result == "OK") {
                            console.log(`Download file Familia ${doc.family} realizado com sucesso!`);
                            await this.updateDownloadStatus(item, {
                                fileType: doc.fileType,
                                status: Status.DownloadOk
                            });
                        } else if (result == "#NOTFOUND") {
                            console.log(`File Familia ${doc.family} not found!`);
                            await this.updateDownloadStatus(item, {
                                fileType: doc.fileType,
                                status: Status.NotFound
                            });
                        }
                    } else {
                        console.log(`File Familia ${doc.family} already download or not found.`);
                    }

                } catch (error) {
                    console.log(`Error trying to download documento Familia ${doc.family}:` + error.message + " - " + error.stack);
                    page.removeAllListeners("request");
                }
            }

            try {
                let statusDocInvoiceColumnName = config.get("excelInput.columnNameStatusDocInvoice").toLowerCase();
                let statusDocCteColumnName = config.get("excelInput.columnNameStatusDocCTE").toLowerCase();
                if (item[statusDocInvoiceColumnName] == "OK" &&
                    item[statusDocCteColumnName] == "OK") {

                    await this.updateState(item, {
                        state: State.DownloadOk
                    });

                } else {

                    if (item[statusDocInvoiceColumnName] == Status.NotFound &&
                        item[statusDocCteColumnName] == Status.NotFound) {

                        await this.updateState(item, {
                            state: State.CheckManually
                        });
                    }
                }

            } catch (error) {
                console.log("Error trying to update 'Situacao':" + error.message + " -" + error.stack);
            }
        }
    }

    async donwloadFamilia(item, queue, family) {
        let result = null;
        await page.setRequestInterception(true);
        page.on("request", request => {
            var data = {
                NroDespacho: item.despacho,
                Familia: family
            }
            var options = {
                method: 'POST',
                headers: { ...request.headers(), "content-type": "application/json; charset=utf-8" },
                postData: JSON.stringify(data)
            };
            request.continue(options);
        });

        const response = await page.goto("http://bank2.alpha2000.com.ar/Despachos/DescargarPDFDigitalizado",
            { waitUntil: 'networkidle0', timeout: timeout });
        let responseBody = await response.text();
        if (responseBody) {
            if (responseBody.indexOf("McAfee Web Gateway is downloading and scanning.") > 0) {
                console.log("McAfee Web Gateway was found...");
                result = "ERROR: MCAFEEPAGE";

                //DescargarPDFDigitalizado

                //document.querySelector("body > div > div.maincontent > div.content > div > b:nth-child(10) > a")

            } else if (responseBody.indexOf('<div class="divLogin">') > 0) {
                console.log("Login page...");
                result = "ERROR: LOGINPAGE";
            } else {
                responseBody = JSON.parse(responseBody);
                if (responseBody.Existe) {
                    //let columnNameCodFornecedor = config.get("excelInput.columnNameCodFornecedor").toLowerCase();
                    // let folderName = config.getOutputFolder() + "\\" + MomentHelper.getDateAsFolderName() + "\\" +
                    //     item.planta + "\\" + item[columnNameCodFornecedor];
                    // let folderName = config.getDownloadDocumentsFolder() + "\\" +
                    //     item.planta + "\\" + item[columnNameCodFornecedor] + "\\" + item.despacho;
                    let folderName = this.getDonwloadFolder(item);
                    if (!fs.existsSync(folderName)) {
                        fs.mkdir(folderName, { recursive: true }, (err) => {
                            if (err) throw err;
                        });
                    }
                    let fileName = folderName + "\\" + responseBody.NombreArchivoDescarga;
                    fs.writeFile(fileName, responseBody.ArchivoBase64, { encoding: 'base64' }, function (err) {                        
                        //Check if the file was really created
                        if (fs.existsSync(fileName)) {
                            console.log('File created: ' + fileName);
                            result = "OK";
                        } else {
                            //Just to keep trying to download the file
                            result = "ERROR: MCAFEEPAGE";
                        }
                        
                    });
                } else {
                    result = "#NOTFOUND";
                }
            }
        }
        page.removeAllListeners("request");
        if (result == "ERROR: MCAFEEPAGE") {
            await new Promise(resolve => setTimeout(resolve, 45000));
        } else {
            await new Promise(resolve => setTimeout(resolve, 5000));
        }
        return result;
    }

    async updateDownloadStatus(item, data) {
        const writeExcel = new WriteExcel();
        const fileName = config.getInputFileName();
        const sheet = config.get("excelInput.sheetName");
        let cellUpdate = "";
        if (data.fileType == "INVOICE") {
            cellUpdate = config.get("excelInput.cellStatusINVOICE") + item.row;
            await writeExcel.updateFile(fileName, sheet, cellUpdate, data.status);

        } else if (data.fileType == "CTE") {
            cellUpdate = config.get("excelInput.cellStatusCTE") + item.row;
            await writeExcel.updateFile(fileName, sheet, cellUpdate, data.status);
        }
    }

    async updateState(item, data) {
        const writeExcel = new WriteExcel();
        const fileName = config.getInputFileName();
        const sheet = config.get("excelInput.sheetName");
        let cellSituacao = config.get("excelInput.cellSituacao") + item.row;
        await writeExcel.updateFile(fileName, sheet, cellSituacao, data.state);
    }

    getDonwloadFolder(item) {
        let columnNameCodFornecedor = config.get("excelInput.columnNameCodFornecedor").toLowerCase();
        return config.getDownloadDocumentsFolder() + "\\" +
                item.planta + "\\" + item[columnNameCodFornecedor] + "\\" + item.despacho;
    }

}

module.exports = DownloadDocuments;