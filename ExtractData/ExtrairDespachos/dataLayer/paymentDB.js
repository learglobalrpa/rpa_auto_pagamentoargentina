const os = require('os');
const sql = require('mssql');
const State = require("../state");
const StatusTrackingNumber = require("../statusTrackingNumber");
const MomentHelper = require("../../../SharedJS/momentHelper");

let config = null;

class PaymentDB {

    init(configManager) {
        config = configManager;
    }

    /**
     * Update item after try to search item by shipment number
     * @param {*} item 
     * @param {*} isFound 
     * @param {*} reasonNotFound 
     */
    async updateStatusSearchItem(item, result) {

        let query = "UPDATE AP_PaymentInProgress SET TrackingStatusID = @TrackingStatusID ";
        let trackingStatusID = 0;
        let paymentStatusID = 0;

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();

        if (result.isFound) {

            if (result.item.StatusAFIP == "DIGI") {
                trackingStatusID = StatusTrackingNumber.TrackingFoundID;
                paymentStatusID = State.WaitingDocumentDownloadID;
            } else {
                trackingStatusID = StatusTrackingNumber.TrackingNotScannnedID;
                paymentStatusID = State.WaitingDocumentScanning;
            }

            query += ", TrackingNumber = @TrackingNumber, DepartureDate = @DepartureDate, PaymentStateID = @PaymentStateID";
            request.input('TrackingNumber', sql.VarChar, result.item.NroDespacho);
            const departureDate = MomentHelper.convertDate(result.item.FechaOficializacion, "DD/MM/YYYY", "MM/DD/YYYY");
            request.input('DepartureDate', sql.DateTime, departureDate);
            request.input('PaymentStateID', sql.Int, paymentStatusID);
            
        } else {

            if (result.reasonNotFound == "#NOTFOUND") {
                trackingStatusID = StatusTrackingNumber.TrackingNotFoundID;

            } else {
                trackingStatusID = StatusTrackingNumber.TrackingNotScannnedID;
            }
        }

        request.input('TrackingStatusID', sql.Int, trackingStatusID);

        query += ",UserCodeUpdate = @UserCodeUpdate, DateTimeUpdate = GETDATE(), TrackingNumberUpdate = GETDATE() "
        query += "WHERE ID = @ID";

        request.input('UserCodeUpdate', sql.VarChar, os.userInfo().username);
        request.input('ID', sql.Int, item.ID);

        // Perform Database update
        let resultRequest = await request.query(query);
        if (resultRequest) {

        }
    }


}

module.exports = PaymentDB;