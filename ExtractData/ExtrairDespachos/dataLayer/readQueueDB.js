const sql = require('mssql');
const State = require("../state");

class ReadQueueDB {

    async getQueue(config, listIDs) {
        let pool = await sql.connect(config.get("connectionString"));

        let listID = '';
        if (listIDs && listIDs.length > 0) {
            for (let index = 0; index < listIDs.length; index++) {
                const id = listIDs[index];
                if (listID.length > 0) {
                    listID += "," + id;
                } else {
                    listID += id;
                }
            }
        }

        let result = await pool.request()
            .input('TranslationLanguageID', sql.Int, 2) //portuguese
            .input('Company', sql.Int, 5000) //Argentina
            .input('PaymentStateID', sql.Int, 0)
            .input('ShipmentNumber', sql.VarChar, '')
            .input('TrackingNumber', sql.VarChar, '')
            .input('ListID', sql.VarChar, listID)
            .input('ReferenceBank', sql.VarChar, '')
            .input('ListPaymentStateID', sql.VarChar, '')
            .execute('SP_AP_GetPaymentInProgress');

        if (result && result.recordset && result.recordset.length > 0) {            
            let output = result.recordset.filter(item => {
                if (item.ShipmentNumber
                  && (!item.PaymentStateName || item.PaymentStateID == State.WaitingShipmmentNoID ||
                       item.PaymentStateID == State.WaitingDocumentScanning)) {
                  return item;
                }
              });
              return output;
        }

        return null;
    }

    async getQueueItem(config, queueID) {
        let query = "SELECT ID, TransactionItem, TransactionItemType ";
        query += "FROM RPA_QueueItem ";
        query += "WHERE ID = @ID";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('ID', sql.Int, queueID);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset[0];
        }
        return null;
    }

}

module.exports = ReadQueueDB;