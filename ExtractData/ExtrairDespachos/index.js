'use strict';

const MomentHelper = require("./momentHelper");
const ReadQueueDB = require("./dataLayer/readQueueDB");
const ConfigManager = require("./configManager");
const DespachosWebApp = require("./despachosWebApp");
const AppSettingsDB = require('../../SharedJS/db/appSettingsDB');

let config = null;
let queue = null;
let despachoWeb = null;
let despachoWebApp = null;
let despachoWebCredential = null;

const initDespachoWeb = async () => {
    despachoWeb = new DespachosWebApp();
    despachoWeb.setAppSettings(despachoWebApp);
    despachoWeb.setAppCredential(despachoWebCredential);
    await despachoWeb.init(config);    
    await despachoWeb.login();
}

const readInputDataDB = async(listIDs) => {
    const readQueueDB = new ReadQueueDB();
    queue = await readQueueDB.getQueue(config, listIDs);
}

const initAppSettings = async() => {
    const appSettingsDB = new AppSettingsDB();
    appSettingsDB.init(config);
    despachoWebApp = await appSettingsDB.loadByCode("DespachoWebAppArgentina");
    despachoWebCredential = await appSettingsDB.getCredential(despachoWebApp.ID);
}

(async() => {

    try {
        console.log(MomentHelper.getDateAndTime());

        config = new ConfigManager();
        config.init();

        const args = process.argv.slice(2);
        const queueID = args[0];
        let queueItem = null;
        let ids = [];
        if (queueID && queueID > 0) {
            const readQueueDB = new ReadQueueDB();
            queueItem = await readQueueDB.getQueueItem(config, queueID);
            if (queueItem) {
                ids = JSON.parse(queueItem.TransactionItem);
            }
        }
    
        try {
            await readInputDataDB(ids);
        } catch (error) {
            console.log("Error trying to get Queue: " + error);
        }
    
        //check if there are items to be processed
        if (!queue || queue.length == 0) {
            console.log("Nao existem itens para obter Nro. Despacho.");

        } else {

            await initAppSettings();

            if (despachoWebApp) {
                await initDespachoWeb();
                await despachoWeb.getInfoDespachos(queue);
            } else {
                console.log("Nao foi possivel obter AppSettings para Despacho WEB.");
            }
        }

        if (despachoWeb != null) {
            despachoWeb.close();
        }
        console.log("Process finished!!")

    } catch (error) {
        console.log("Error: " + error.message + " - " + error.stack);
        if (despachoWeb != null) {
            despachoWeb.close();
        }
    }

})();