﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServices
{
    public class ErrorResult
    {

        public Error error { get; set; }

        public class Error
        {
            public string code { get; set; }
            public string message { get; set; }
        }

    }

    
}
