﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureServices
{
    public class RecognitionTextResult
    {

        public string status { get; set; }
        public RecognitionResult recognitionResult { get; set; }

        public string json { get; set; }

        public class RecognitionResult
        {
            public Line[] lines { get; set; }


            public class Line
            {
                public int[] boundingBox { get; set; }
                public string text { get; set; }
                public Word[] words { get; set; }

                public class Word
                {
                    public int[] boundingBox { get; set; }
                    public string text { get; set; }
                }
            }
        }
    }
}
