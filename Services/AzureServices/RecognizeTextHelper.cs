﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AzureServices.ReadTextResult;

namespace AzureServices
{
    public class RecognizeTextHelper
    {

        public static string[] GetGetRecognizeTextResultLinesText(RecognitionTextResult result)
        {
            var texts = from text in result.recognitionResult.lines
                        select text.text;
            return texts.ToArray();
        }

        public static string[] GetReadAnalyzeResultLinesText(ReadTextResult result)
        {
            List<Line> lines = new List<Line>();
            foreach (var item in result.analyzeResult.ReadResults)
            {
                foreach (var line in item.Lines)
                {
                    lines.Add(line);
                }
            }
            var texts = from line in lines
                        select line.Text;
            return texts.ToArray();
        }

    }
}
