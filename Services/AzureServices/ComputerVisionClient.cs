﻿using Lear.RpaMS.Model.ModelLayer.CloudServices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AzureServices
{
    public class ComputerVisionClient
    {
        private ServiceResource azureService = null;
             
        public ComputerVisionClient(ServiceResource azureService)
        {
            this.azureService = azureService;
        }

        public RecognitionTextResult RecognizeText(string inputFile, string mode = "Printed")
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers        
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", azureService.ServiceKey);

            // Request parameters
            queryString["mode"] = mode;
            var uri = azureService.ServiceUrl + "?" + queryString;

            HttpResponseMessage response;

            // Request body
            byte[] byteData = this.GetImageBytes(inputFile);

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = client.PostAsync(uri, content).Result;
            }

            RecognitionTextResult result = null;

            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                string operationLocation = response.Headers.GetValues("Operation-Location").FirstOrDefault();
                result = this.GetRecognizeTextOperationResult(operationLocation);
            }
            else
            {
                string json = response.Content.ReadAsStringAsync().Result;
                ErrorResult errorResult = JsonConvert.DeserializeObject<ErrorResult>(json);
                //Get error message
                throw new Exception("Azure Error: Recognize Text. Message: " +
                    errorResult.error.code + " - " + errorResult.error.message);
            }

            return result;
        }

        public RecognitionTextResult GetRecognizeTextOperationResult(string operationLocation)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers           
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", azureService.ServiceKey);

            // Request parameters
            var uri = operationLocation + "?" + queryString;
            HttpResponseMessage response;

            RecognitionTextResult result = null;

            while (true)
            {
                response = client.GetAsync(uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {

                    string json = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<RecognitionTextResult>(json);
                   if (result.status == "Running" || result.status == "NotStarted")
                    {
                        //Wait for 2 seconds for the next attempt
                        System.Threading.Thread.Sleep(2000);
                        continue;
                    }                    

                    break;
                }
                else
                {

                    string json = response.Content.ReadAsStringAsync().Result;
                    ErrorResult errorResult = JsonConvert.DeserializeObject<ErrorResult>(json);
                    //get error message
                    throw new Exception("Azure Error: Recognize Text Operation Result. Message: " +
                        errorResult.error.code + " - " + errorResult.error.message);
                }
            }

            return result;
        }

        private byte[] GetImageBytes(string input)
        {
            return System.IO.File.ReadAllBytes(input);
        }

        /// <summary>
        /// Upgrade from Recognize Text to Read
        /// </summary>
        /// <param name="inputFile"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        public ReadTextResult ReadText(string inputFile)
        {
            var client = new HttpClient();            
            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", azureService.ServiceKey);
            var uri = azureService.ServiceUrl;

            HttpResponseMessage response;

            // Request body
            byte[] byteData = this.GetImageBytes(inputFile);

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = client.PostAsync(uri, content).Result;
            }

            ReadTextResult result = null;

            if (response.StatusCode == System.Net.HttpStatusCode.Accepted)
            {
                string operationLocation = response.Headers.GetValues("Operation-Location").FirstOrDefault();
                result = this.GetReadTextResult(operationLocation);
            }
            else
            {
                string json = response.Content.ReadAsStringAsync().Result;
                ErrorResult errorResult = JsonConvert.DeserializeObject<ErrorResult>(json);
                //Get error message
                throw new Exception("Azure Error: Recognize Text. Message: " +
                    errorResult.error.code + " - " + errorResult.error.message);
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationLocation"></param>
        /// <returns></returns>
        public ReadTextResult GetReadTextResult(string operationLocation)
        {
            var client = new HttpClient();

            // Request headers           
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", azureService.ServiceKey);

            // Request parameters
            var uri = operationLocation;
            HttpResponseMessage response;

            ReadTextResult result = null;

            while (true)
            {
                response = client.GetAsync(uri).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string json = response.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<ReadTextResult>(json);
                    result.json = json;
                    if (result.Status.ToLower() == "running" || result.Status.ToLower() == "notstarted")
                    {
                        //Wait for 2 seconds for the next attempt
                        System.Threading.Thread.Sleep(2000);
                        continue;
                    }

                    break;
                }
                else
                {

                    string json = response.Content.ReadAsStringAsync().Result;
                    ErrorResult errorResult = JsonConvert.DeserializeObject<ErrorResult>(json);
                    //get error message
                    throw new Exception("Azure Error: Read Analyze Results. Message: " +
                        errorResult.error.code + " - " + errorResult.error.message);
                }
            }

            return result;
        }


    }
}
