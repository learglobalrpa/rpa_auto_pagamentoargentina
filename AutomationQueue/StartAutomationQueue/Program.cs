﻿using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Queue;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Queue;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StartAutomationQueue
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("<<< INICIADO AUTOMACAO PAGAMENTOS ARGENTINA - QUEUE ITEMS >>>");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Console.WriteLine("Desenvolvido por: RPA Team Brazil");
            Console.WriteLine("Autor: Fabricio J. Reif - freif@lear.com");
            Console.WriteLine("Versao: " + assembly.GetName().Version.ToString());
            
            try
            {
                // Change current culture
                CultureInfo culture = CultureInfo.CreateSpecificCulture("pt-BR");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                Initialize(args);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Erro inesperado: " + ex.Message + " - " + ex.StackTrace);
            }

            Console.WriteLine("Finished!!");
        }

        private static void Initialize(string[] args)
        {
            string inputConfigFile = null;            
            string queueID = null;
            if (args != null && args.Length > 0)
            {
                inputConfigFile = args[0];
                queueID = args[1];
            }

            if (String.IsNullOrEmpty(queueID))
            {
                throw new Exception("Argument QueueID not found!!");
            }

            Config.ConfigManager config = new Config.ConfigManager();
            config.Init(inputConfigFile);
            config.Get().GenerateDocuments = true;

            Console.WriteLine($"<<< EXECUTANDO QUEUE ITEM: {queueID} >>>");

            //#########################################################################################
            // Set App global settings
            Lear.RpaMS.AppLayer.AppSettings.Instance.ConnectionString = config.Get().connectionString;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName = Environment.MachineName;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser = new User() { UserName = Environment.UserName };
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentTranslationLanguage = TranslationLanguage.PORTUGUESE;
            // Load project settings
            Project project = ProjectService.LoadProjectByCode(config.Get().projectCode);
            if (project == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Project '{config.Get().projectCode}' not found in RPA_Project Table.");
                return;
            }
            project.Parameters = ProjectService.LoadParameters(project.Id);
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject = project;

            QueueItem queueItem = QueueService.LoadById(Convert.ToInt32(queueID));
            if (queueItem == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Queue Item '{queueID}' not found in RPA_QueueItem Table.");
                return;
            }
            //#########################################################################################


            AutomationFlow.FlowManager flow = new AutomationFlow.FlowManager(config);
            flow.OnMessageHandler += Flow_OnMessageHandler;            
            flow.StartWithQueueItem(queueItem);
        }

        private static void Flow_OnMessageHandler(string msgForCaller, AutomationFlow.FlowManager.MessageEventArgs e)
        {
            if (e.type == AutomationFlow.FlowManager.MessageType.Informative)
            {
                Console.WriteLine(msgForCaller);
            }
            else if (e.type == AutomationFlow.FlowManager.MessageType.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;

            }
            else if (e.type == AutomationFlow.FlowManager.MessageType.BusinessRule)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

    }
}
