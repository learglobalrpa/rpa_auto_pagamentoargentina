﻿using Lear.RpaMS.Data.DataServiceLayer;
using Lear.RpaMS.Data.DataServiceLayer.Queue;
using Lear.RpaMS.Model.ModelLayer;
using Lear.RpaMS.Model.ModelLayer.Queue;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace StartAutomationComex
{
    class Program
    {
        private static readonly string PROCESS_CODE = "PagArgentinaUploadData";

        static void Main(string[] args)
        {
            Console.WriteLine("<<< INICIADO AUTOMACAO UPLOAD ARQUIVOS PAGAMENTOS ARGENTINA >>>");
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            Console.WriteLine("Desenvolvido por: RPA Team Brazil");
            Console.WriteLine("Autor: Fabricio J. Reif - freif@lear.com");
            Console.WriteLine("Versao: " + assembly.GetName().Version.ToString());

            try
            {
                // Change current culture
                CultureInfo culture = CultureInfo.CreateSpecificCulture("pt-BR");
                Thread.CurrentThread.CurrentCulture = culture;
                Thread.CurrentThread.CurrentUICulture = culture;

                Initialize(args);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Erro inesperado: " + ex.Message + " - " + ex.StackTrace);
            }

            Console.WriteLine("Finished!!");
            Console.ReadLine();
        }

        private static void Initialize(string[] args)
        {
            string inputConfigFile = null;
            string queueID = null;
            if (args != null && args.Length > 0)
            {
                inputConfigFile = args[0];
                if (args.Length > 1)
                {
                    queueID = args[1];
                }
            }
            Config.ConfigManager config = new Config.ConfigManager();
            config.Init(inputConfigFile);

            //#########################################################################################
            // Set App global settings
            Lear.RpaMS.AppLayer.AppSettings.Instance.ConnectionString = config.Get().connectionString;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName = Environment.MachineName;
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser = new User() { UserName = Environment.UserName };
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentTranslationLanguage = TranslationLanguage.PORTUGUESE;
            // Load project settings
            Project project = ProjectService.LoadProjectByCode(config.Get().projectCode);
            if (project == null)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Project '{config.Get().projectCode}' not found in RPA_Project Table.");
                return;
            }
            project.Parameters = ProjectService.LoadParameters(project.Id);
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject = project;
            project.Processes = ProjectService.LoadProcesses(project.Id);
            project.VerifyDynamicVariables();
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject = project;
            ProjectProcess projectProcess = project.GetProcessByCode(PROCESS_CODE);
            Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProcess = projectProcess;

            QueueItem queueItem = null;
            if (queueID != null)
            {
                Console.WriteLine($"<<< EXECUTANDO QUEUE ITEM: {queueID} >>> ");

                queueItem = QueueService.LoadById(Convert.ToInt32(queueID));
                if (queueItem == null)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine($"Queue Item '{queueID}' not found in RPA_QueueItem Table.");
                    return;
                }
            }
            //#########################################################################################

            AutomationFlowComex.FlowManagerComex flow = new AutomationFlowComex.FlowManagerComex(config);
            flow.OnMessageHandler += Flow_OnMessageHandler;
            flow.Start(queueItem);

        }

        private static void Flow_OnMessageHandler(string msgForCaller, AutomationFlowComex.FlowManagerComex.MessageEventArgs e)
        {
            if (e.type == AutomationFlowComex.FlowManagerComex.MessageType.Informative)
            {
                Console.WriteLine(msgForCaller);
            }
            else if (e.type == AutomationFlowComex.FlowManagerComex.MessageType.Error)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;

            }
            else if (e.type == AutomationFlowComex.FlowManagerComex.MessageType.BusinessRule)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(msgForCaller);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

    }
}
