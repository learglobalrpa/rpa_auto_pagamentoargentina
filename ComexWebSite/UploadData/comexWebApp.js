const puppeteer = require("../../node_modules/puppeteer");
const currencyHelper = require("./../../SharedJS/currencyHelper");
const AppSettingsHelper = require("./../../SharedJS/appSettingsHelper");
const { triggerAsyncId } = require("async_hooks");

let browser = null;
let page = null;
let config = null;
let appSettings = null;
let appSettingsCredential = null;
let project = null;

const CURRENCY = new Map();
CURRENCY.set("USD", "USD - DOLARES USA DIVISA");
CURRENCY.set("SEK", "SEK - CORONA SUECA");
CURRENCY.set("NOK", "NOK - CORONA NORUEGA");
CURRENCY.set("JPY", "JPY - YEN");
CURRENCY.set("GBP", "GBP - LIBRA ESTERLINA");
CURRENCY.set("EUR", "EUR - EUROS");
CURRENCY.set("DKK", "DKK - CORONA DANESA");
CURRENCY.set("CNY", "CNY - YUAN RENMINBI CONTINENTAL");
CURRENCY.set("CNH", "CNH - YUAN RENMINBI HONG KONG");
CURRENCY.set("CHF", "CHF - FRANCO SUIZO");
CURRENCY.set("CAD", "CAD - DOLAR CANADIENSE");
CURRENCY.set("BRL", "BRL - REAL");
CURRENCY.set("AUD", "AUD - DOLARES AUSTRAL");
CURRENCY.set("ARS", "ARS - PESOS");

const TIPO_REFERENCIA = "Otro tipo Operación";

class ComexWebApp {
  setBrowser(_browser) {
    browser = _browser;
  }

  setPage(_page) {
    page = _page;
  }

  setConfig(_config) {
    config = _config;
  }

  setAppSettings(despachoWebApp) {
    appSettings = despachoWebApp;
  }

  setAppCredential(despachoWebCredential) {
    appSettingsCredential = despachoWebCredential;
  }

  setProject(_project) {
    project = _project;
  }

  async init(configManager) {
    config = configManager;
    browser = await puppeteer.launch({
      headless: appSettings.IsHeadless,
      ignoreDefaultArgs: ["--enable-automation"],
      args: [
        "--disable-infobar",
        "--no-sandbox",
        "--start-maximized",
        "--disable-default-apps",
      ],
    });
    page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 650 });
    const url = appSettings.Url;
    await page.goto(url, { waitUntil: "load", timeout: 0 });
    page.setDefaultNavigationTimeout(240000);
  }

  async login() {
    const user = appSettingsCredential.UserID;
    const pwd = appSettingsCredential.UserPwd;
    await page.type("#userid", user);
    await page.type("#viewPass", pwd);
    await page.click("#buttonSave");

    await page.waitForNavigation();
    await new Promise((resolve) => setTimeout(resolve, 2000));

    await this.goToComexCiti();
    await this.acceptAfterLogin();

    await this.selectCompany();
  }

  async goToComexCiti() {
    const parameter = AppSettingsHelper.getParameterByName(
      appSettings.Parameters,
      "UrlComex"
    );
    const url = parameter.Value;
    await page.goto(url, { waitUntil: "load", timeout: 0 });
  }

  async acceptAfterLogin() {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 10));

    //Find and click button Accept
    var btnAccept = await page.$("#disclaimerForm\\:btnContinuar");
    if (btnAccept != null) {
      await btnAccept.evaluate((element) => {
        element.click();
      });
    }
    await page.waitForNavigation();
    await new Promise((resolve) => setTimeout(resolve, 1000 * 10));
  }

  async selectCompany() {
    const parameter = AppSettingsHelper.getParameterByName(
      appSettings.Parameters,
      "CompanyCode"
    );
    const company = parameter.Value;

    await new Promise((resolve) => setTimeout(resolve, 1000 * 2));
    await page.evaluate((company) => {
      let ele = document.querySelector("#empresaForm\\:empresaSelector_input");
      if (ele) {
        let options = ele.options;
        let index = 0;
        if (options && options.length > 0) {
          for (var i = 0; i < options.length; i++) {
            if (options[i].text == company) {
              index = i;
              break;
            }
          }
        }

        if (index > 0) {
          ele.selectedIndex = index;
          ele.dispatchEvent(new Event("change"));
          document.querySelector("#empresaForm").submit();
        }
      }
    }, company);

    await page.waitForNavigation();
    await this.waitPanelVisibleFalse();

    //await new Promise((resolve) => setTimeout(resolve, 1000 * 10));
  }

  async goToGiroExterior() {
    //await new Promise((resolve) => setTimeout(resolve, 1000 * 5));
    await page.click("#menuForm\\:menuHorizontal > ul > li:nth-child(1) > a");
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    await page.click("#menuForm\\:tdu-super-maker-online-inicioOperacionGiro");
    await page.waitForNavigation();
  }

  async isOperationPanelOpen() {
    let selector = "#wfOperationPanel";
    let panel = await page.$(selector);
    if (!panel) {
      return false;
    }

    let offset = await page.evaluate((_selector) => {
      let _offset = 0;
      let ele = document.querySelector(_selector);
      if (ele) {
        _offset = ele.offsetTop;
      }
      return _offset;
    }, selector);

    if (offset == 0) {
      return false;
    }

    return true;
  }

  async fillGiroExterior(data) {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 2));
    data.ReferenceBank = await this.getReferenceBank();

    let parameter = AppSettingsHelper.getParameterByName(
      appSettings.Parameters,
      "Concept"
    );
    let value = parameter.Value;

    //Cod. Concepto:
    let selectorOpenList = "#wfContainerForm\\:concepto_label";
    let selectorList = "#wfContainerForm\\:concepto_items";
    let itemFound = await this.setComboboxValue(
      selectorOpenList,
      selectorList,
      value
    );
    if (!itemFound) {
      throw `Erro: Concepto '${value}' nao encontrado.`;
    }
    console.log("Filled 'Concepto' ...");

    //Tipo de Referencia
    selectorOpenList = "#wfContainerForm\\:tipoReferencia_label";
    selectorList = "#wfContainerForm\\:tipoReferencia_items";
    itemFound = await this.setComboboxValue(
      selectorOpenList,
      selectorList,
      TIPO_REFERENCIA
    );
    if (!itemFound) {
      throw `Erro: Referencia '${TIPO_REFERENCIA}' nao encontrado.`;
    }
    console.log("Filled 'Tipo de Referencia' ...");

    //Nro de Cuenta
    parameter = AppSettingsHelper.getParameterByName(
      appSettings.Parameters,
      "ContaNo"
    );
    value = parameter.Value;

    selectorOpenList = "#wfContainerForm\\:cuentaId_label";
    selectorList = "#wfContainerForm\\:cuentaId_items";
    itemFound = await this.setComboboxValue(
      selectorOpenList,
      selectorList,
      value
    );
    if (!itemFound) {
      throw `Erro: Cuenta '${value}' nao encontrado.`;
    }
    console.log("Filled 'Cuenta' ...");

    //Cod. Moneda
    value = CURRENCY.get(data.Currency);
    selectorOpenList = "#wfContainerForm\\:moneda_label";
    selectorList = "#wfContainerForm\\:moneda_items";
    itemFound = await this.setComboboxValue(
      selectorOpenList,
      selectorList,
      value
    );
    if (!itemFound) {
      throw `Erro: Moneda '${value}' nao encontrado.`;
    }
    console.log("Filled 'Moneda' ...");

    //Observaciones
    let comment = data.Despacho;
    await page.type("#wfContainerForm\\:observacionesId", comment);
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    console.log("Filled 'Observaciones' ...");

    //Importe
    await page.type(
      "#wfContainerForm\\:importe",
      currencyHelper.getValueARS(data.Amount)
    );
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    console.log("Filled 'Importe' ...");

    //Referencia Cliente
    parameter = AppSettingsHelper.getParameterByName(
      appSettings.Parameters,
      "UploadTextMessage"
    );
    value = parameter.Value;
    let refCustomer = data.CodFornecedor + " - " + data.CustomerReference;
    if (value) {
      refCustomer = value + " " + refCustomer;
    }
    await page.type("#wfContainerForm\\:referenciaCliente", refCustomer);
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    console.log("Filled 'Referencia Cliente' ...");

    //Beneficiario
    value = data.Vendor.VendorCodeReference;
    selectorOpenList = "#wfContainerForm\\:beneficiario_select_label";
    selectorList = "#wfContainerForm\\:beneficiario_select_items";
    itemFound = await this.setComboboxValue(
      selectorOpenList,
      selectorList,
      value
    );
    if (!itemFound) {
      throw `Beneficiario '${value}' nao encontrado na lista ...`;
    }
    console.log("Filled 'Beneficiario' ...");

    //Gastos bancarios:
    parameter = AppSettingsHelper.getParameterByName(
      appSettings.Parameters,
      "BankingExpenses"
    );
    value = parameter.Value;
    selectorOpenList = "#wfContainerForm\\:gastosBancarios_label";
    selectorList = "#wfContainerForm\\:gastosBancarios_items";
    itemFound = await this.setComboboxValue(
      selectorOpenList,
      selectorList,
      value
    );
    if (!itemFound) {
      throw `Erro: Gastos bancarios '${value}' nao encontrado.`;
    }
    console.log("Filled 'Gastos bancarios' ...");

    //attach PDF file
    await this.attachFile(data);

    //Click Guardar button
    await this.clickSaveButton();

    //Click Enviar a Firmantes button
    await this.clickSendSigners();

    data.finished = await this.isButtonFinalizarEnabled();

    //Click send to signers Confirmation
    //TODO: uncomment this line for production
    await this.clickConfirmationSendSigners();

    //Finalizar
    //await this.clickFinishButton();

    //Confirm operation
    //await this.clickConfirmation();
  }

  async getReferenceBank() {
    const selectorInput =
      "#wfContainer_content > table:nth-child(3) > tbody > tr > td.operacion_header_column_desc";
    let reference = await page.evaluate((_selectorInput) => {
      let ele = document.querySelector(_selectorInput);
      let _reference = null;
      if (ele) {
        _reference = ele.innerText;
      }
      return _reference;
    }, selectorInput);

    return reference;
  }

  async setComboboxValue(selectorOpenList, selectorListItems, value) {
    await page.evaluate((_selectorOpenList) => {
      let ele = document.querySelector(_selectorOpenList);
      if (ele) {
        ele.click();
      }
    }, selectorOpenList);

    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));

    let isFound = await page.evaluate(
      (_selectorListItems, _value) => {
        let found = false;
        let ele = document.querySelector(_selectorListItems);
        if (ele) {
          if (ele.hasChildNodes()) {
            let nodes = ele.children;
            for (let index = 0; index < nodes.length; index++) {
              const item = nodes[index];
              if (
                item.innerText.replace(/ /g, "") == _value.replace(/ /g, "")
              ) {
                item.click();
                found = true;
                break;
              }
            }
          }
        }
        return found;
      },
      selectorListItems,
      value
    );
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));

    return isFound;
  }

  async attachFile(data) {
    const fileUploaders = await page.$$("input[type=file]");
    await fileUploaders[0].uploadFile(data.File);
    await fileUploaders[0].evaluate((ele) => {
      ele.dispatchEvent(new Event("change"));
    });
    await new Promise((resolve) => setTimeout(resolve, 1000 * 5));

    console.log("Waiting to attach file ...");
    await this.waitPanelDownload();

    // debugger;

    //Adjuntar
    //await page.click("#wfContainerForm\\:operationAttachmentUpload > div.ui-fileupload-buttonbar.ui-widget-header.ui-corner-top > button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-icon-left.ui-fileupload-upload");

    await this.waitSelectorVisible("#wfContainerForm\\:editAttachmentTable");

    console.log("Document attached!");
    // console.log("Waiting for 15 secodns ...");
    // await new Promise((resolve) => setTimeout(resolve, 1000 * 15));

    //await this.waitPanelVisibleFalse();
  }

  async clickSaveButton() {
    console.log("Start Button Guardar...");

    //Click Guardar button
    await page.evaluate((_selector) => {
      let ele = document.querySelector(_selector);
      if (ele) {
        ele.click();
      }
    }, "#wfContainerForm\\:saveTaskCmd");

    //await page.click("#wfContainerForm\\:saveTaskCmd");
    console.log("Button Guardar clicked...");
    //console.log("Waiting for 45 seconds...");
    //await new Promise((resolve) => setTimeout(resolve, 1000 * 45));

    await new Promise((resolve) => setTimeout(resolve, 1000 * 10));
    await this.waitPanelVisibleFalse();
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    await this.waitPanelVisibleFalse();
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));

    // try {
    //   page.waitForSelector("#finalizarOperacionCmd", { visible: true });
    // } catch (error) {}
  }

  async clickFinishButton() {
    console.log("Start Button Finalizar...");

    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    await this.waitPanelVisibleFalse();

    await page.evaluate((_selector) => {
      let ele = document.querySelector(_selector);
      if (ele) {
        ele.click();
      }
    }, "#finalizarOperacionCmd");

    //await page.click("#finalizarOperacionCmd");
    console.log("Button Finalizar clicked...");
    //console.log("Waiting for 10 seconds...");
    //await new Promise((resolve) => setTimeout(resolve, 1000 * 10));

    await new Promise((resolve) => setTimeout(resolve, 1000 * 3));
    await this.waitPanelVisibleFalse();
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    await this.waitPanelVisibleFalse();
    await new Promise((resolve) => setTimeout(resolve, 1000 * 3));
  }

  async clickSendSigners() {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    await this.waitPanelVisibleFalse();

    await page.evaluate((_selector) => {
      let ele = document.querySelector(_selector);
      if (ele) {
        ele.click();
      }
    }, "#enviarFirmanteCmd");

    //await page.click("#enviarFirmanteCmd");
    console.log("Button Enviar a Firmantes clicked...");
    // console.log("Waiting for 10 seconds...");
    // await new Promise((resolve) => setTimeout(resolve, 1000 * 10));

    await this.waitPanelVisibleFalse();
    await new Promise((resolve) => setTimeout(resolve, 1000 * 3));
  }

  async clickConfirmation() {
    await page.click("#wfConfirmationForm\\:j_idt307");
    console.log("Confirmation clicked...");
    console.log("Waiting for 10 seconds...");
    await new Promise((resolve) => setTimeout(resolve, 1000 * 10));
  }

  async clickConfirmationSendSigners() {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    await this.waitPanelVisibleFalse();

    await page.evaluate((_selector) => {
      let ele = document.querySelector(_selector);
      if (ele && ele.innerText == "Aceptar") {
        ele.click();
      }
    }, "button[id*='wfConfirmationForm\\:j_idt']");
    //await page.click('#wfConfirmationForm\\:j_idt274');
    console.log("Confirmation Send to Signers clicked...");
    console.log("Waiting for 3 seconds...");
    await new Promise((resolve) => setTimeout(resolve, 1000 * 3));

    await this.waitPanelVisibleFalse();
  }

  async waitPanelDownload() {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    const selector = "#modalWaitPanel_content";
    try {
      let visible = await page.evaluate((_selector) => {
        let ele = document.querySelector(_selector);
        if (ele) {
          return ele.offsetParent != null;
        }
        return false;
      }, selector);

      if (visible) {
        console.log("Panel download still visible...");
        await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
        await this.waitPanelVisibleFalse();
      }

      console.log("Panel download is not visible...");

      await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    } catch (error) {
      console.log("Error waitPanelDownload: " + error.message);
    }
  }

  async waitPanelVisibleFalse() {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    const selector = "#waitPanel";
    try {
      let visible = await page.evaluate((_selector) => {
        // let ele = document.querySelector(_selector);
        // if (ele) {
        //   return ele.offsetParent != null;
        // }
        // return false;

        //JQuery
        return $(_selector).is(":visible");
      }, selector);

      if (visible) {
        console.log("Panel still visible...");
        await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
        await this.waitPanelVisibleFalse();
      }

      console.log("Panel is not visible...");

      await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    } catch (error) {
      console.log("Error waitPanelVisibleFalse: " + error.message);
    }
  }

  async waitSelectorVisible(selector) {
    await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    try {
      let visible = await page.evaluate((_selector) => {
        //JQuery
        return $(_selector).is(":visible");
      }, selector);

      if (!visible) {
        console.log("Selector still not visible...");
        await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
        await this.waitSelectorVisible(selector);
      }

      console.log("Selector is visible...");

      await new Promise((resolve) => setTimeout(resolve, 1000 * 1));
    } catch (error) {
      console.log("Error waitSelectorVisible: " + error.message);
    }
  }

  async isButtonFinalizarEnabled() {
    const selector = "#finalizarOperacionCmd";
    try {
      let enabled = await page.evaluate((_selector) => {
        let ele = document.querySelector(_selector);
        if (ele) {
          return !ele.disabled;
        }
        return false;
      }, selector);

      return enabled;
    } catch (error) {
      console.log("Error isButtonFinalizarEnabled: " + error.message);
    }

    return false;
  }

  async close() {
    if (browser != null) {
      await browser.close();
    }
  }
}

module.exports = ComexWebApp;
