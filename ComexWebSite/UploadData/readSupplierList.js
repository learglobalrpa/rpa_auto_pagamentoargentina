const ImportExcel = require("./readExcel");

class ReadSupplierList {

    async readExcel(config) {
        let excelFileName = config.get("excelListaFornecedores.fileName");
        const importExcel = new ImportExcel();
        let list = await importExcel.loadAsObject(excelFileName, null);
        //create the row index for excel
        list.forEach((element, index, array) => {
            element.row = index + 2;
        });
        return list;
    }

    // async getQueue(config) {
    //     let list = await this.readExcel(config);
    //     let situacaoColumnName = config.get("excelInput.columnNameSituacao").toLowerCase();


    //     return groupItems;
    // }

}


module.exports = ReadSupplierList;