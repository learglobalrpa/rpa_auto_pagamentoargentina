const xlsxj = require("../../node_modules/xlsx-to-json-lc");

class ImportExcel {

    loadAsObject(inputFile, outputFile) {

      return new Promise((resolve, reject) => {

        xlsxj({
          input: inputFile, 
          output: outputFile,
          lowerCaseHeaders:true //converts excel header rows into lowercase as json keys
        }, function(err, result) {
            if(err) {
              reject(err);
            }else {
              resolve(result);
            }
        });
      });      
      
    }
}

module.exports = ImportExcel;