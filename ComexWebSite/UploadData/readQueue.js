const ImportExcel = require("./readExcel");
const State = require("../../SharedJS/state");

class ReadQueue {

  async readExcel(config) {
    let inputExcelLiberados = config.getInputFileName();
    const importExcel = new ImportExcel();
    let list = await importExcel.loadAsObject(inputExcelLiberados, null);
    //create the row index for excel
    list.forEach((element, index, array) => {
      element.row = index + 2;
    });

    return list;
  }

  async getQueue(config) {
    let list = await this.readExcel(config);
    let situacaoColumnName = config.get("excelInput.columnNameSituacao").toLowerCase();
    //Get all items that are in state document created
    let items = list.filter(item => {
      if (item[situacaoColumnName].replace(/\s/g, '') == State.DocumentCreated.replace(/\s/g, '')) {
        return item;
      }
    });

    //Group items by Fornecedor, Planta and Despacho
    let groupItems = new Map();
    if (items && items.length > 0) {
      let codFornecedorColumnName = config.get("excelInput.columnNameCodFornecedor").toLowerCase();
      let despachoColumnName = config.get("excelInput.columnNameNroDespacho").toLowerCase();
      let plantaColumnName = config.get("excelInput.columnNamePlanta").toLowerCase();

      for (const item of items) {
        let key = item[codFornecedorColumnName] + item[despachoColumnName] + item[plantaColumnName];
        let values = [];
        if (!groupItems.has(key)) {
          values.push(item);
        } else {
          values = groupItems.get(key);
          values.push(item);
        }
        groupItems.set(key, values);
      }
    }

    return groupItems;
  }

}

module.exports = ReadQueue;