const os = require('os');
const sql = require('mssql');
const State = require("../../../SharedJS/state");

class ReadQueueDB {

    async getQueue(config) {
        let pool = await sql.connect(config.get("connectionString"));
        let result = await pool.request()
            .input('TranslationLanguageID', sql.Int, 2) //portuguese
            .input('Company', sql.Int, 5000) //Argentina
            .input('PaymentStateID', sql.Int, State.DocumentCreatedID)
            .input('ShipmentNumber', sql.VarChar, '')
            .input('TrackingNumber', sql.VarChar, '')
            .input('ListID', sql.VarChar, '')
            .input('ReferenceBank', sql.VarChar, '')
            .input('ListPaymentStateID', sql.VarChar, '')
            .execute('SP_AP_GetPaymentInProgress');

        let items = null;
        let documents = null;
        if (result && result.recordset && result.recordset.length > 0) {
            items = result.recordset;
            let listPayments = '';
            items.forEach(element => {
                if (listPayments.length == 0) {
                    listPayments += element.ID;
                } else {
                    listPayments += "," + element.ID;
                }
            });
            documents = await this.loadDocumentsByPaymentID(config, listPayments);
        }

        //Group items by Fornecedor, Planta and Despacho
        let groupItems = new Map();
        if (documents && documents.length > 0) {
            for (const doc of documents) {
                let key = doc.DocumentID;
                //Find payment data
                const payment = items.find(item => item.ID == doc.PaymentID);
                payment.File = doc.FullName;
                let values = [];
                if (!groupItems.has(key)) {
                    values.push(payment);
                } else {
                    values = groupItems.get(key);
                    values.push(payment);
                }
                groupItems.set(key, values);
            }
        }

        return groupItems;
    }

    async loadDocumentsByPaymentID(config, listPayments) {
        let query = 
            `SELECT Max(PaymentDoc.DocumentID) AS DocumentID 
                    ,PaymentDoc.PaymentID
                    ,Max(Doc.Name) As Name
                    ,Max(Doc.FullNameInsert) As FullNameInsert
                    ,dbo.UDF_AP_GetDirectoryFolderByProject(1) + '\\' 
                            + FORMAT(Max(Doc.DateTimeInsert), 'yyyyMM') + '\\'
                            + Max(Payment.PlantName) + '\\' 
                            + Max(Payment.VendorCode) + '\\' 
                            + Max(Doc.Name) As FullName
                    ,Doc.DocumentTypeID
                    FROM [AP_PaymentInProgressDocument] PaymentDoc
                    INNER JOIN RPA_Document Doc ON PaymentDoc.DocumentID = Doc.ID
                    INNER JOIN AP_PaymentInProgress Payment ON PaymentDoc.PaymentID = Payment.ID
                    WHERE Doc.DocumentTypeID = 3
                    AND PaymentDoc.PaymentID IN (${listPayments})
                    and DOC.Active = 1
                    and PaymentDoc.Active = 1
                  GROUP BY PaymentDoc.PaymentID
                        ,Doc.DocumentTypeID
                  order by Max(PaymentDoc.DocumentID)
        `;

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        
        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset;
        }
        return null;
    }

    async getQueueItem(config, queueID) {
        let query = "SELECT ID, TransactionItem, TransactionItemType, CurrentTransactionItem ";
        query += "FROM RPA_QueueItem ";
        query += "WHERE ID = @ID";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('ID', sql.Int, queueID);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset[0];
        }
        return null;
    }

    async updateQueueItem(config, queueID, queueItem) {
        let query = "UPDATE RPA_QueueItem ";
        query += "SET CurrentTransactionItem = @CurrentTransactionItem, ";
        query += "ExecutionFileName = @ExecutionFileName, ";
        query += "MachineName = @MachineName, ";
        query += "UserCodeUpdate = @UserCodeUpdate, DateTimeUpdate = GETDATE() ";
        query += "WHERE ID = @ID";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('CurrentTransactionItem', sql.VarChar, queueItem);
        request.input('ExecutionFileName', sql.VarChar, process.argv[1]);
        request.input('MachineName', sql.VarChar, os.hostname());
        request.input('UserCodeUpdate', sql.VarChar, os.userInfo().username);
        request.input('ID', sql.Int, queueID);

        // Perform Database update
        await request.query(query);
    }

}

module.exports = ReadQueueDB;