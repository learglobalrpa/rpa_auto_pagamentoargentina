const sql = require('mssql');

let config = null;

class VendorDB {

    init(configManager) {
        config = configManager;
    }

    async loadCrossTable(crossTableID) {
        let query = `SELECT ID, CrossTableID, VendorCode, VendorCodeReference, Active 
                     FROM RPA_VendorCrossTable
                     WHERE CrossTableID = @CrossTableID`;

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        request.input('CrossTableID', sql.Int, crossTableID);

        // Perform Database select
        let resultRequest = await request.query(query);
        if (resultRequest) {
            return resultRequest.recordset;
        }
        return null;
    }


}

module.exports = VendorDB;