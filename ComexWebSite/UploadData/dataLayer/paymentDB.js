const os = require('os');
const sql = require('mssql');
const State = require("../../../SharedJS/state");

let config = null;

class PaymentDB {

    init(configManager) {
        config = configManager;
    }

    /**
     * Update list of items after uploaded file to citi bank web site
     * @param {*} items
     */
    async updateStatusSentToBank(items, referenceBank, 
        stateDocument, message, paymentState) {

        let listID = "";
        for (const item of items) {
            if (listID.length == 0) {
                listID = item.ID;
            } else {
                listID += "," + item.ID;
            }
        }

        let query = "UPDATE AP_PaymentInProgress SET  ";
        if (paymentState == State.DocumentSentToBankID) {
            query += "ReferenceBank = @ReferenceBank, DateTimeSentToBank = GETDATE() ";
        } else {
            query += "SentToBankMessage = @SentToBankMessage, DateTimeSentToBank = NULL ";
        }
        query += ", PaymentStateID = @PaymentStateID, SentToBankDocStatusID = @SentToBankDocStatusID ";
        query += ",UserCodeUpdate = @UserCodeUpdate, DateTimeUpdate = GETDATE() "
        query += "WHERE ID IN (" + listID + ")";

        let pool = await sql.connect(config.get("connectionString"));
        let request = await pool.request();
        if (paymentState == State.DocumentSentToBankID) {
            request.input('ReferenceBank', sql.VarChar, referenceBank);
        } else {
            request.input('SentToBankMessage', sql.VarChar, message);
        }
        request.input('PaymentStateID', sql.Int, paymentState);
        request.input('SentToBankDocStatusID', sql.Int, stateDocument);
        request.input('UserCodeUpdate', sql.VarChar, os.userInfo().username);

        // Perform Database update
        await request.query(query);
    }


}

module.exports = PaymentDB;