"use strict";

const fs = require("fs");
const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../../../SharedJS/configManager");
const ReadQueueDB = require("../dataLayer/readQueueDB");
const VendorDB = require("../dataLayer/vendorDB");
const PaymentDB = require("../dataLayer/paymentDB");
const MomentHelper = require("../../../SharedJS/momentHelper");
const ArrayHelper = require("../../../SharedJS/arrayHelper");
const Status = require("../../../SharedJS/status");
const State = require("../../../SharedJS/state");
const ComexWebApp = require("../comexWebApp");
const AppSettingsDB = require("../../../SharedJS/db/appSettingsDB");
const ProjectDB = require("../../../SharedJS/db/projectDB");

let config = null;
let browser = null;
let queue = null;
let vendorList = null;
let comexWeb = null;
let despachoWebApp = null;
let despachoWebCredential = null;
let project = null;

const initAppSettings = async () => {
  const appSettingsDB = new AppSettingsDB();
  appSettingsDB.init(config);
  despachoWebApp = await appSettingsDB.loadByCode("CitiFxWebAppArgentina");
  despachoWebApp.Parameters = await appSettingsDB.loadParameters(
    despachoWebApp.ID
  );
  despachoWebCredential = await appSettingsDB.getCredential(despachoWebApp.ID);
};

const initProject = async () => {
  const projectDB = new ProjectDB();
  projectDB.init(config);
  const projectCode = config.get("projectCode");
  project = await projectDB.loadByCode(projectCode);
  project.Parameters = await projectDB.loadParameters(project.ID);
};

const initComexWeb = async () => {
  const browserWSEndpoint = await new Promise(function (resolve, reject) {
    fs.readFile("session.txt", "utf-8", (err, data) => {
      resolve(data);
    });
  });
  // Use the endpoint to reestablish a connection
  browser = await puppeteer.connect({
    browserWSEndpoint,
    width: 1366,
    height: 650,
  });
  let page = (await browser.pages())[0];
  await page.setViewport({ width: 1366, height: 650 });

  comexWeb = new ComexWebApp();
  comexWeb.setBrowser(browser);
  comexWeb.setPage(page);
  comexWeb.setConfig(config);
  comexWeb.setAppSettings(despachoWebApp);
  comexWeb.setAppCredential(despachoWebCredential);
  comexWeb.setProject(project);
};

const readInputDataDB = async (listIDs) => {
  const readQueueDB = new ReadQueueDB();
  queue = await readQueueDB.getQueue(config);

  if (listIDs && listIDs.length > 0) {
    let newQueue = new Map();
    for (const id of listIDs) {
      let items = queue.get(id);
      if (items) {
        newQueue.set(id, items);
      }
    }
    queue = newQueue;
  }
};

const readVendorCrossTable = async () => {
  const vendorDB = new VendorDB();
  vendorDB.init(config);
  vendorList = await vendorDB.loadCrossTable(1);
};

const initGiroExterior = async () => {
  await comexWeb.goToGiroExterior();
};

const fillFormCiti = async (queueID, listIDs) => {
  try {
    const paymentDB = new PaymentDB();
    paymentDB.init(config);
    for await (const entry of queue.entries()) {
      let documentID = entry[0];
      let values = entry[1];
      let item = values[0];
      let fillData = true;

      let isOpenPanel = await comexWeb.isOperationPanelOpen();
      if (!isOpenPanel) {
        await initGiroExterior();
      }

      try {
        let data = {};
        data.CodFornecedor = item.VendorCode;
        data.Plant = item.PlantCode;
        data.Currency = item.DocumentCurrency;
        data.Despacho = item.TrackingNumber;
        data.Invoices = [];
        let amount = 0;
        for (const value of values) {
          data.Invoices.push(value.ReferenceImportation);
          let valorME = parseFloat(value.ImportDocumentValue);
          amount += valorME;
        }
        data.Amount = amount;
        data.File = item.File;

        //Check if vendor exists in the list
        let vendor = vendorList.filter(
          (sup) => sup.VendorCode == data.CodFornecedor
        );

        if (vendor == null || vendor.length == 0) {
          const msg = `Erro: Nao foi possivel encontrar fornecedor na lista com codigo: ${data.CodFornecedor}`;
          console.log(msg);
          await paymentDB.updateStatusSentToBank(
            values,
            null,
            Status.SentToBankErrorID,
            msg,
            State.DocumentCreatedID
          );

          fillData = false;
          //continue;
        } else if (vendor.length > 1) {
          const msg = `Erro: Mais de um fornecedor encontrado na lista com codigo: ${data.CodFornecedor}`;
          console.log(msg);
          await paymentDB.updateStatusSentToBank(
            values,
            null,
            Status.SentToBankErrorID,
            msg,
            State.DocumentCreatedID
          );

          fillData = false;
          //continue;
        }

        if (fillData) {
          data.Vendor = vendor[0];
          data.CustomerReference = item.VendorName;
          await comexWeb.fillGiroExterior(data);

          if (data.ReferenceBank) {
            //Update items
            await paymentDB.updateStatusSentToBank(
              values,
              data.ReferenceBank,
              Status.SentToBankOkID,
              "",
              State.DocumentSentToBankID
            );
          }
        }
      } catch (error) {
        console.log(
          "Error trying to upload file: " + error.message + " - " + error.stack
        );
        try {
          await paymentDB.updateStatusSentToBank(
            values,
            null,
            Status.SentToBankErrorID,
            error.message,
            State.DocumentCreatedID
          );
        } catch (error) {}
      }

      try {
        //Remove the item from the array
        if (listIDs && listIDs.length > 0) {
          ArrayHelper.deleteItemByValue(listIDs, documentID);
          let queueItem = null;
          if (listIDs && listIDs.length > 0) {
            queueItem = JSON.stringify(listIDs);
          }
          const readQueueDB = new ReadQueueDB();
          await readQueueDB.updateQueueItem(config, queueID, queueItem);
        }
      } catch (error) {
        console.log("Error trying to update queue item: " + error.message);
      }
    }
  } catch (error) {}
};

(async () => {
  try {
    console.log(MomentHelper.getDateAndTime());

    config = new ConfigManager();
    config.init("../../Data/Config/config.json");

    const args = process.argv.slice(2);
    const queueID = args[0];
    let queueItem = null;
    let ids = [];
    if (queueID && queueID > 0) {
      const readQueueDB = new ReadQueueDB();
      queueItem = await readQueueDB.getQueueItem(config, queueID);
      if (queueItem) {
        if (queueItem.CurrentTransactionItem) {
          ids = JSON.parse(queueItem.CurrentTransactionItem);
        } else {
          ids = JSON.parse(queueItem.TransactionItem);
        }
      }
    }

    await initAppSettings();
    console.log("App Settings loaded...");
    await initProject();
    console.log("Project [" + config.get("projectCode") + "] loaded...");

    try {
      await readInputDataDB(ids);
    } catch (error) {
      console.log("Error trying to get Queue: " + error);
    }

    //check if there are items to be processed
    if (!queue || queue.size == 0) {
      console.log("Nao existem itens para Upload no Site Comex Citi Bank.");
    }

    if (queue && queue.size > 0) {
      await readVendorCrossTable();
      await initComexWeb();
      //await comexWeb.selectCompany();
      //await initGiroExterior();
      await fillFormCiti(queueID, ids);
      //await comexWeb.clickSaveButton();
    }

    console.log("Process finished!!");

    if (browser != null) {
      browser.disconnect();
    }
  } catch (error) {
    console.log("Error: " + error.message + " - " + error.stack);
    if (browser != null) {
      browser.disconnect();
    }
  }
})();
