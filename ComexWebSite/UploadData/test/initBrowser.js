'use strict';

const puppeteer = require("../../../node_modules/puppeteer");
const ConfigManager = require("../../../SharedJS/configManager");
const fs = require("fs");
const ComexWebApp = require("../comexWebApp");
const AppSettingsDB = require('../../../SharedJS/db/appSettingsDB');
const ProjectDB = require('../../../SharedJS/db/projectDB');

let config = null;
let browser = null;
let page = null;
let comexWeb = null;
let despachoWebApp = null;
let despachoWebCredential = null;
let project = null;

const initAppSettings = async() => {
    const appSettingsDB = new AppSettingsDB();
    appSettingsDB.init(config);
    despachoWebApp = await appSettingsDB.loadByCode("CitiFxWebAppArgentina");
    despachoWebApp.Parameters = await appSettingsDB.loadParameters(despachoWebApp.ID);
    despachoWebCredential = await appSettingsDB.getCredential(despachoWebApp.ID);
}

const initProject = async() => {
    const projectDB = new ProjectDB();
    projectDB.init(config);
    const projectCode = config.get("projectCode");
    project = await projectDB.loadByCode(projectCode);
    project.Parameters = await projectDB.loadParameters(project.ID);
}

const init = async() => {
    browser = await puppeteer.launch({ headless: despachoWebApp.IsHeadless, 
        ignoreDefaultArgs: ['--enable-automation'],
        args: ['--disable-infobar', '--no-sandbox', '--start-maximized', '--disable-default-apps'] });
    const browserWSEndpoint = browser.wsEndpoint();
    console.log(browserWSEndpoint);
    
    fs.writeFile("session.txt", browserWSEndpoint, (err) => {
        if (err) console.log(err);
        console.log("Successfully Written to File.");
    });

    page = await browser.newPage();
    await page.setViewport({ width: 1366, height: 650 });

    const url = despachoWebApp.Url;
    await page.goto(url, { waitUntil: 'load', timeout: 0 });
    page.setDefaultNavigationTimeout(240000);

    comexWeb = new ComexWebApp();
    comexWeb.setBrowser(browser);
    comexWeb.setPage(page);
    comexWeb.setConfig(config);
    comexWeb.setAppSettings(despachoWebApp);
    comexWeb.setAppCredential(despachoWebCredential);
    comexWeb.setProject(project);

    await comexWeb.login();
    
    
    browser.disconnect();
};


(async() => {    

    config = new ConfigManager();
    config.init("../../Data/Config/config.json");

    await initAppSettings();
    console.log("App Settings loaded...");
    await initProject();
    console.log("Project [" + config.get("projectCode") + "] loaded...");

    await init();

    console.log("Browser started!!")
})();