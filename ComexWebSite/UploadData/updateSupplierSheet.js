const fs = require('fs');
const Excel = require("../../node_modules/exceljs/lib/exceljs.nodejs");
const MomentHelper = require("../../SharedJS/momentHelper");


(async () => {

    try {
        console.log(MomentHelper.getDateAndTime());

        //Read json
        var encoding = {
            encoding: 'latin1'
        };
        let rawData = fs.readFileSync("./beneficiarios.json", encoding);
        if (rawData != null) {
            let data = JSON.parse(rawData);
            if (data) {
                let fileName = "../../Data/Config/ListaFornecedores.xlsx";
                var wb = await new Excel.Workbook().xlsx.readFile(fileName);
                const ws = wb.getWorksheet("fornecedores");
                let row = 2;
                for (const item of data.clientSelectItems) {
                    ws.getCell("B" + row).value = item.label;
                    ws.getCell("C" + row).value = item.value;
                    // console.log(item.label);
                    // console.log(item.value);
                    row++;
                }
                await wb.xlsx.writeFile(fileName);
            }
            
        }


        console.log("Process finished!!")

    } catch (error) {
        console.log("Error: " + error.message + " - " + error.stack);
    }

})();