﻿using Lear.RpaMS.Model.ModelLayer.Queue;
using RunExtractData;
using System;
using System.Diagnostics;
using TransactionData;

namespace AutomationFlowComex
{
    public class FlowManagerComex
    {

        private Config.ConfigManager config = null;
        private int maxRetryNumber = -1;
        private int attempts = 0;

        public enum MessageType
        {
            Informative = 1,
            Error = 2,
            BusinessRule = 3
        }

        public delegate void MessageHandler(string msgForCaller, MessageEventArgs e);
        public event MessageHandler OnMessageHandler;

        public class MessageEventArgs : EventArgs
        {
            public MessageType type;
        }

        public FlowManagerComex(Config.ConfigManager config)
        {
            this.config = config;
        }

        private void RaiseInformativeMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.Informative;
            OnMessageHandler(message, e);
        }

        private void RaiseErrorMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.Error;
            OnMessageHandler(message, e);
        }

        private void RaiseBusinessRuleMessage(string message)
        {
            MessageEventArgs e = new MessageEventArgs();
            e.type = MessageType.BusinessRule;
            OnMessageHandler(message, e);
        }

        public void Start(QueueItem queueItem)
        {
            try
            {
                if (maxRetryNumber < 0)
                {
                    maxRetryNumber = Convert.ToInt32(Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.MaxRetryNumber);
                    attempts = 1;
                }

                RaiseInformativeMessage("Comex Web Site: Automation Flow Started: " + DateTime.Now.ToString());
                RaiseInformativeMessage("Attempt: " + attempts);
                this.Init(queueItem);


            }
            catch (SystemError ex)
            {
                RaiseErrorMessage(ex.Message + " -" + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.Start(queueItem);
                }
            }
            catch (BusinessRuleException ex)
            {

                throw;
            }
            catch (Exception ex)
            {
                RaiseErrorMessage(ex.Message + " - " + ex.StackTrace);
                if (attempts < maxRetryNumber)
                {
                    attempts++;
                    RaiseInformativeMessage("Trying again. Attempt: " + attempts);
                    //try again
                    this.Init(queueItem);
                }
            }

        }

        private void Init(QueueItem queueItem)
        {
            this.RunUploadFile(queueItem);            
        }

        public void RunUploadFile(QueueItem queueItem)
        {            
            RaiseInformativeMessage("===============================================================");
            RaiseInformativeMessage("Iniciado execucao da rotina para UPLOAD dos Arquivos no Site COMEX Citi Bank");
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            string filename = "node.exe";
            string arguments = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProcess.Path + @"\UploadData\index.js";
            if (queueItem != null)
            {
                arguments += " \"" + queueItem.Id + "\"";
            }
            string workingDirectory = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProcess.Path + @"\UploadData";
            string output = ExecuteProcessManager.Run(filename, arguments, workingDirectory);
            stopwatch.Stop();
            RaiseInformativeMessage("Finalizado rotina para UPLOAD dos arquivos: " + stopwatch.Elapsed);
            RaiseInformativeMessage("Retorno da execucao:");
            RaiseInformativeMessage(output);
            RaiseInformativeMessage("===============================================================");
            if (output.Contains("Error:"))
            {
                throw new SystemError();
            }

        }



    }
}
