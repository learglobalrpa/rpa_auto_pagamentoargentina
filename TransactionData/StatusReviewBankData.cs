﻿using Lear.RpaMS.Model.ModelLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class StatusReviewBankData
    {

        private Config.ConfigManager config = null;

        public StatusReviewBankData(Config.ConfigManager config)
        {
            this.config = config;
        }

        public List<StatusBase> LoadAll()
        {
            SqlCommand command = null;
            List<StatusBase> listStatus = null;
            try
            {
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.SELECT);
                query.AddFields("ID, Code, Name_EN, Name_PT, Name_ES, Active");
                query.AddTable("RPA_StatusBankReview");
                command = DataConnectionBase.GetCommand(config);
                command.CommandText = query.GetQuery();
                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    listStatus = new List<StatusBase>();
                    while (reader.Read())
                    {
                        StatusBase status = new StatusBase();
                        status.Id = Convert.ToInt32(reader["ID"]);
                        status.Code = reader["Code"].ToString();
                        status.Name = reader["Name_ES"].ToString();
                        status.Active = Convert.ToBoolean(reader["Active"]);
                        listStatus.Add(status);
                    }
                }
            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
            return listStatus;
        }


    }
}
