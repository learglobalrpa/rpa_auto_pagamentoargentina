﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace TransactionData
{
    class ExcelHelper
    {

        public static object GetValue(Excel.Range value, object defaultValue)
        {
            try
            {
                return value.Value2;
            }
            catch (Exception)
            {
                if (defaultValue != null)
                {
                    return default;
                }
            }
            return null;
        }


    }
}
