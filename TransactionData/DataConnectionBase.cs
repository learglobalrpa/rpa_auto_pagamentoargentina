﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class DataConnectionBase
    {

        public static SqlCommand GetCommand(Config.ConfigManager config)
        {
            string connectionString = config.Get().connectionString;
            var connection = new SqlConnection(connectionString);
            var command = new SqlCommand();
            command.Connection = connection;
            return command;
        }

    }
}
