﻿using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace TransactionData
{
    public class Update
    {

        private Config.ConfigManager config = null;

        public Update(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void UpdateItemDB(List<TransactionItem> items)
        {
            SqlCommand command = null;
            try
            {
                command = DataConnectionBase.GetCommand(config);

                QueryBuilder query = null;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection.Open();

                foreach (TransactionItem item in items)
                {
                    command.Parameters.Clear();
                    query = this.GetUpdateItemQueryDefault();

                    //Status
                    command.Parameters.Add("@TrackingStatusID", System.Data.SqlDbType.Int);
                    command.Parameters["@TrackingStatusID"].Value = item.TrackingStatusID;

                    //Status documento invoice
                    command.Parameters.Add("@InvoiceDocStatusID", System.Data.SqlDbType.Int);
                    command.Parameters["@InvoiceDocStatusID"].Value = item.IdStatusDocumentoInvoice;

                    //Status documento embarque
                    command.Parameters.Add("@ShippingDocStatusID", System.Data.SqlDbType.Int);
                    command.Parameters["@ShippingDocStatusID"].Value = item.IdStatusDocumentoEmbarque;

                    //Status carta
                    command.Parameters.Add("@LetterDocStatusID", System.Data.SqlDbType.Int);
                    if (item.LetterDocStatusID > 0)
                    {
                        command.Parameters["@LetterDocStatusID"].Value = item.LetterDocStatusID;
                    }
                    else
                    {
                        command.Parameters["@LetterDocStatusID"].Value = DBNull.Value;
                    }

                    //Situacao
                    command.Parameters.Add("@PaymentStateID", System.Data.SqlDbType.Int);
                    command.Parameters["@PaymentStateID"].Value = item.PaymentStateID;

                    if (!String.IsNullOrEmpty(item.Message))
                    {
                        query.AddValue("MessageText = @MessageText");
                        command.Parameters.Add("@MessageText", System.Data.SqlDbType.VarChar);
                        command.Parameters["@MessageText"].Value = item.Message;
                    }

                    command.Parameters.Add("@UserCodeUpdate", System.Data.SqlDbType.VarChar);
                    command.Parameters["@UserCodeUpdate"].Value = Environment.UserName;
                    command.Parameters.Add("@ID", System.Data.SqlDbType.Int);
                    command.Parameters["@ID"].Value = item.Id;

                    command.CommandText = query.GetQuery();
                    command.ExecuteNonQuery();
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

        }

        public void AssignDocuments(
            List<TransactionItem> items,
            List<Document> documents,
            bool forceLoad = false)
        {
            foreach (TransactionItem item in items)
            {
                List<Document> newDocuments = new List<Document>();

                if (item.Documents == null || item.Documents.Count == 0 || forceLoad)
                {
                    item.Documents = PaymentItemService.LoadPaymentInProgressDocuments(
                        item.Id,
                        Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.Id);

                    if (item.Documents == null || item.Documents.Count == 0)
                    {
                        newDocuments.AddRange(documents);
                    }
                    else
                    {
                        foreach (var document in documents)
                        {
                            if (item.Documents.Find(doc => doc.Id == document.Id) == null)
                            {
                                newDocuments.Add(document);
                            }
                        }
                    }
                }

                if (newDocuments.Count > 0)
                {
                    PaymentItemService.InsertDocumentPaymentInProgress(item.Id, newDocuments);
                }                
            }
        }

        private QueryBuilder GetUpdateItemQueryDefault()
        {
            QueryBuilder query = new QueryBuilder();
            query.SetQueryType(QueryBuilder.QueryType.UPDATE);
            query.AddValues("TrackingStatusID = @TrackingStatusID, InvoiceDocStatusID = @InvoiceDocStatusID");
            query.AddValues("ShippingDocStatusID = @ShippingDocStatusID, LetterDocStatusID = @LetterDocStatusID");
            query.AddValues("PaymentStateID = @PaymentStateID");
            //Audit
            query.AddValues("UserCodeUpdate = @UserCodeUpdate, DateTimeUpdate = GETDATE()");
            query.AddTable("AP_PaymentInProgress");
            query.AddWhereConditions("ID = @ID");

            return query;
        }

    }
}
