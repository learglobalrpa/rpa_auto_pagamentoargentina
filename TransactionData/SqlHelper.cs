﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class SqlHelper
    {

        public static object GetValue(object value, object defaultValue)
        {
            if (value != null && value.GetType() != typeof(System.DBNull))
            {
                return value;
            }
            else if (defaultValue != null)
            {
                return defaultValue;
            }

            return null;
        }

        public static DateTime? GetNullableDate(object value, object defaultValue)
        {
            object dateValue = SqlHelper.GetValue(value, defaultValue);
            if (dateValue != null)
            {
                return Convert.ToDateTime(dateValue);
            }
            return null;
        }

    }
}
