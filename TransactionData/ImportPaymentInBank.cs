﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class ImportPaymentInBank
    {
        private static readonly string TEMP_TABLE_NAME = "AP_TempImportPaymentInBank";

        private Config.ConfigManager config = null;

        public ImportPaymentInBank(Config.ConfigManager config)
        {
            this.config = config;
        }

        public void ImportData(DataTable table, int dataImportExecutionID)
        {
            SqlCommand command = null;
            try
            {
                // 1 - First delete Temp Table
                QueryBuilder query = new QueryBuilder();
                query.SetQueryType(QueryBuilder.QueryType.DELETE);
                query.AddTable(TEMP_TABLE_NAME);
                query.AddWhereConditions("UserCodeInsert = @UserCodeInsert AND MachineName = @MachineName");

                command = DataConnectionBase.GetCommand(config);
                command.CommandText = query.GetQuery();
                command.Parameters.Add("@UserCodeInsert", SqlDbType.VarChar);
                command.Parameters["@UserCodeInsert"].Value = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentUser.UserName;
                command.Parameters.Add("@MachineName", SqlDbType.VarChar);
                command.Parameters["@MachineName"].Value = Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentMachineName;

                command.Connection.Open();
                command.ExecuteNonQuery();

                // 2 - Insert data into Temp Table
                SqlBulkCopy bulk = new SqlBulkCopy(command.Connection);
                bulk.DestinationTableName = TEMP_TABLE_NAME;
                //ADD COLUMN MAPPING
                foreach (DataColumn col in table.Columns)
                {
                    bulk.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }
                bulk.WriteToServer(table);

                // 3 - Execute stored procedure
                command.Parameters.Clear();
                command.CommandText = "SP_AP_ImportPaymentInBank";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param.ParameterName = "@DataImportExecutionID";
                param.Value = dataImportExecutionID;
                param.Direction = ParameterDirection.Input;
                command.Parameters.Add(param);
                command.ExecuteNonQuery();

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }
        }


    }
}
