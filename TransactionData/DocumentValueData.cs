﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class DocumentValueData
    {

        private Config.ConfigManager config = null;

        public DocumentValueData(Config.ConfigManager config)
        {
            this.config = config;
        }


        public List<DocumentPageDataItem> LoadPageValuesByFileName(string fileName)
        {
            SqlCommand command = null;
            List<DocumentPageDataItem> items = new List<DocumentPageDataItem>();

            try
            {
                command = DataConnectionBase.GetCommand(config);
                StringBuilder sql = new StringBuilder();
                sql.Append("SELECT doc.ID, doc.DocumentTypeID,doc.Name,Page.ID As PageID,Page.Name As PageFileName, Data.Value ");
                sql.Append("FROM RPA_Document doc ");
                sql.Append("INNER JOIN RPA_DocumentPage Page on doc.ID = Page.DocumentID ");
                sql.Append("INNER JOIN RPA_DocumentPageData Data on Page.ID = Data.DocumentPageID ");
                sql.Append("WHERE Doc.DocumentTypeID = 1 and doc.Name = @FileName");

                command.Parameters.Add("@FileName", System.Data.SqlDbType.VarChar);
                command.Parameters["@FileName"].Value = fileName;

                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = sql.ToString();
                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        DocumentPageDataItem item = new DocumentPageDataItem();
                        item.ID = Convert.ToInt32(reader["ID"]);                        
                        item.DocumentTypeID = Convert.ToInt32(SqlHelper.GetValue(reader["DocumentTypeID"], 0));
                        item.Name = SqlHelper.GetValue(reader["Name"], string.Empty).ToString();
                        item.PageID = Convert.ToInt32(SqlHelper.GetValue(reader["PageID"], 0));
                        item.PageFileName = SqlHelper.GetValue(reader["PageFileName"], string.Empty).ToString();                        
                        item.Value = SqlHelper.GetValue(reader["Value"], string.Empty).ToString();                        
                        items.Add(item);
                    }
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return items;
        }

    }
}
