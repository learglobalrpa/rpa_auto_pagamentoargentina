﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace TransactionData
{
    public class TransactionDataManager
    {

        private Config.ConfigManager config = null;

        public TransactionDataManager(Config.ConfigManager config)
        {
            this.config = config;
        }


        public List<TransactionItem> GetTransactionSqlData(int[] listIDs)
        {
            SqlCommand command = null;
            List<TransactionItem> items = new List<TransactionItem>();

            try
            {
                command = DataConnectionBase.GetCommand(config);
                command.CommandText = "SP_AP_GetPaymentInProgress";
                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.Parameters.Add("@TranslationLanguageID", System.Data.SqlDbType.Int);
                command.Parameters["@TranslationLanguageID"].Value = 2;
                command.Parameters.Add("@Company", System.Data.SqlDbType.Int);
                command.Parameters["@Company"].Value = 5000;
                command.Parameters.Add("@PaymentStateID", System.Data.SqlDbType.Int);
                command.Parameters["@PaymentStateID"].Value = 0;
                command.Parameters.Add("@ShipmentNumber", System.Data.SqlDbType.VarChar);
                command.Parameters["@ShipmentNumber"].Value = string.Empty;
                command.Parameters.Add("@TrackingNumber", System.Data.SqlDbType.VarChar);
                command.Parameters["@TrackingNumber"].Value = string.Empty;
                command.Parameters.Add("@ListID", System.Data.SqlDbType.VarChar);
                if (listIDs == null || listIDs.Length == 0)
                {
                    command.Parameters["@ListID"].Value = string.Empty;
                } else
                {
                    string listID = null;
                    foreach (var id in listIDs)
                    {
                        if (listID != null && listID.Length > 0)
                        {
                            listID += "," + id;
                        } else
                        {
                            listID += id;
                        }
                    }
                    command.Parameters["@ListID"].Value = listID;
                }
                command.Parameters.Add("@ReferenceBank", System.Data.SqlDbType.VarChar);
                command.Parameters["@ReferenceBank"].Value = string.Empty;

                string listPaymentStateID = "1, 2, 3, 4, 5, 6, 9";
                command.Parameters.Add("@ListPaymentStateID", System.Data.SqlDbType.VarChar);
                command.Parameters["@ListPaymentStateID"].Value = listPaymentStateID;
                command.Parameters["@ListPaymentStateID"].IsNullable = true;

                command.Connection.Open();

                using (var reader = command.ExecuteReader())
                {                    
                    while (reader.Read())
                    {
                        TransactionItem item = new TransactionItem();
                        item.Id = Convert.ToInt32(reader["ID"]);
                        item.CodForn = SqlHelper.GetValue(reader["VendorCode"], string.Empty).ToString();
                        item.Fornecedor = SqlHelper.GetValue(reader["VendorName"], string.Empty).ToString();
                        item.DocProvisao = SqlHelper.GetValue(reader["DocumentProvision"], string.Empty).ToString();
                        item.RefImportacao = SqlHelper.GetValue(reader["ReferenceImportation"], string.Empty).ToString();
                        item.NroEmbarque = SqlHelper.GetValue(reader["ShipmentNumber"], string.Empty).ToString();
                        item.Planta = SqlHelper.GetValue(reader["PlantName"], string.Empty).ToString();
                        item.ValorME = Convert.ToDecimal(SqlHelper.GetValue(reader["ImportDocumentValue"], 0));
                        item.Moeda = SqlHelper.GetValue(reader["DocumentCurrency"], string.Empty).ToString();
                        item.DataDocumento = Convert.ToDateTime(reader["InvoiceDocumentDate"]);
                        item.Observacoes = "Ok, para pagar.";
                        item.Despacho = SqlHelper.GetValue(reader["TrackingNumber"], string.Empty).ToString();
                        item.DataEmbarque = SqlHelper.GetValue(reader["DepartureDate"], string.Empty).ToString();
                        item.Status = SqlHelper.GetValue(reader["StatusTrackingName"], string.Empty).ToString();
                        item.TrackingStatusID = Convert.ToInt32(SqlHelper.GetValue(reader["TrackingStatusID"], 0));
                        item.StatusDocumentoInvoice = SqlHelper.GetValue(reader["InvoiceDocStatusName"], string.Empty).ToString();
                        item.IdStatusDocumentoInvoice = Convert.ToInt32(SqlHelper.GetValue(reader["InvoiceDocStatusID"], 0));
                        item.StatusDocumentoEmbarque = SqlHelper.GetValue(reader["ShippingDocStatusName"], string.Empty).ToString();
                        item.IdStatusDocumentoEmbarque = Convert.ToInt32(SqlHelper.GetValue(reader["ShippingDocStatusID"], 0));
                        item.StatusCarta = SqlHelper.GetValue(reader["LetterDocStatusName"], string.Empty).ToString();
                        item.LetterDocStatusID = Convert.ToInt32(SqlHelper.GetValue(reader["LetterDocStatusID"], 0));
                        item.Situacao = SqlHelper.GetValue(reader["PaymentStateName"], string.Empty).ToString();
                        item.PaymentStateID = Convert.ToInt32(SqlHelper.GetValue(reader["PaymentStateID"], 0));
                        items.Add(item);
                    }
                }

            }
            finally
            {
                if (command != null)
                {
                    if (command.Connection != null)
                    {
                        command.Connection.Close();
                        command.Connection.Dispose();
                    }
                    command.Dispose();
                }
            }

            return items;
        }

    }
}
