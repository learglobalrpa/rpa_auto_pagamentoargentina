﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class DocumentPageDataItem
    {
        public int ID;
        public int DocumentTypeID;
        public string Name;
        public int PageID;
        public string PageFileName;
        public string Value;
    }
}
