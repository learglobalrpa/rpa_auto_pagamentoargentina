﻿namespace TransactionData
{
    public static class Status
    {
        public static readonly string NotFound = "Nao encontrado para download";
        public static readonly string DownloadOk = "OK";
        public static readonly string MoreThanOneDocument = "Erro: Mais de um documento encontrado";
        public static readonly string NotFoundInvoice = "Erro: Invoice nao encontrada no arquivo Familia 2";
        public static readonly string LetterOk = "OK";
        public static readonly string LetterError = "Erro: Nao foi possivel gerar carta para Citi Bank";

        public static readonly int NotFoundID = 1;
        public static readonly int DownloadOkID = 2;
        public static readonly int MoreThanOneDocumentID = 3;
        public static readonly int NotFoundInvoiceID = 4;
        public static readonly int LetterOkID = 5;
        public static readonly int LetterErrorID = 6;

    }
}
