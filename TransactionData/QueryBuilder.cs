﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class QueryBuilder
    {

        public enum QueryType
        {
            SELECT = 1,
            INSERT = 2,
            UPDATE = 3,
            DELETE = 4
        }

        private StringBuilder sql = null;
        private StringBuilder fields = null;
        private StringBuilder values = null;
        private string _table = null;
        private string _conditions = null;
        private QueryType _type = QueryType.SELECT;
        private bool _IsScopeIdentity = false;

        public QueryBuilder()
        {
            sql = new StringBuilder();
            fields = new StringBuilder();
            values = new StringBuilder();
        }

        public void SetQueryType(QueryType type)
        {
            this._type = type;
        }

        public bool IsScopeIdentity
        {
            get { return _IsScopeIdentity; }
            set
            {
                _IsScopeIdentity = value;
            }
        }

        public void AddField(string field)
        {
            if (this.fields.Length > 0)
            {
                this.fields.Append($" ,{field}");
            }
            else
            {
                this.fields.Append($" {field}");
            }
        }

        /// <summary>
        /// List of fields with comma separator
        /// </summary>
        /// <param name="field"></param>
        /// <example> ID, Name, Value, .... </example>
        public void AddFields(string fields)
        {
            this.AddField(fields);
        }

        public void AddValue(string value)
        {
            if (this.values.Length > 0)
            {
                this.values.Append($" ,{value}");
            }
            else
            {
                this.values.Append($" {value}");
            }
        }

        public void AddValues(string values)
        {
            this.AddValue(values);
        }        

        public void AddTable(string tableName)
        {
            _table = tableName;
        }

        public void AddTable(string prefix, string tableName)
        {
            _table = $"[{prefix}.{tableName}]";
        }

        public void AddWhereConditions(string conditions)
        {
            _conditions = conditions;
        }

        public string GetQuery()
        {
            switch (_type)
            {
                // Sql Command
                case QueryType.SELECT:
                    sql.Append("SELECT ");
                    // Fields
                    sql.Append(fields.ToString());
                    // From
                    if (!String.IsNullOrEmpty(_table))
                    {
                        sql.Append($" FROM {_table} ");
                    }
                    // Where
                    if (!String.IsNullOrEmpty(_conditions))
                    {
                        sql.Append($" WHERE {_conditions} ");
                    }
                    break;

                case QueryType.INSERT:
                    sql.Append("INSERT INTO ");
                    // Table
                    sql.Append(_table);
                    // Fields
                    sql.Append($" ({fields.ToString()}) ");
                    // Values
                    sql.Append($" Values ({values.ToString()}); ");
                    // Scope Identity
                    if (this.IsScopeIdentity)
                    {
                        sql.Append("SELECT SCOPE_IDENTITY();");
                    }
                    break;

                case QueryType.UPDATE:
                    sql.Append("UPDATE ");
                    // Table
                    sql.Append(_table);
                    // Values
                    sql.Append($" SET {values}");
                    // Where condition
                    if (!String.IsNullOrEmpty(_conditions))
                    {
                        sql.Append($" WHERE {_conditions} ");
                    }
                    break;

                case QueryType.DELETE:
                    sql.Append("DELETE ");
                    // Table
                    sql.Append($"FROM {_table}");
                    // Where condition
                    if (!String.IsNullOrEmpty(_conditions))
                    {
                        sql.Append($" WHERE {_conditions} ");
                    }
                    break;
            }

            return sql.ToString();
        }

        public void Clear()
        {
            sql.Clear();
            fields.Clear();
            values.Clear();
            _table = string.Empty;
            _conditions = string.Empty;
        }

    }

}
