﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class BusinessRuleException : Exception
    {
        public TransactionItem Item { get; set; }
        public string ErrorCode { get; set; }

        public BusinessRuleException() { }
        public BusinessRuleException(string message) : base(message) { }
        public BusinessRuleException(string message, Exception innerException) : base(message, innerException) { }
        public BusinessRuleException(string message, Exception innerException, TransactionItem item) : base(message, innerException) {
            this.Item = item;
        }
        public BusinessRuleException(string message, Exception innerException, 
            TransactionItem item, string errorCode) : base(message, innerException)
        {
            this.Item = item;
            this.ErrorCode = errorCode;
        }
        public BusinessRuleException(string message, TransactionItem item, string errorCode) : base(message)
        {
            this.Item = item;
            this.ErrorCode = errorCode;
        }
        public BusinessRuleException(string message, string errorCode) : base(message)
        {
            this.ErrorCode = errorCode;
        }

    }
}
