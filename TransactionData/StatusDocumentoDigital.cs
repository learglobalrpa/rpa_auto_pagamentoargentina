﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public enum StatusDocumentoDigital
    {

        NaoEncontrado = 1,
        Encontrrado = 2,
        MoreThanOneDocumentFound = 3
    }
}
