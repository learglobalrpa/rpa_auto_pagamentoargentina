﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class SystemError : Exception
    {        
        public SystemError()
        {}


        public SystemError(string message) : base(message)
        {}


        public SystemError(string message, Exception innerException) : base(message, innerException)
        {}
    }
}
