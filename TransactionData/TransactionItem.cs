﻿using Lear.RpaMS.Data.DataServiceLayer.Finance.AP;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class TransactionItem
    {
        public int Id { get; set; }
        public int Row { get; set; }
        public string CodForn { get; set; }
        public string Fornecedor { get; set; }
        public string DocProvisao { get; set; }
        public string RefImportacao { get; set; }
        public string NroEmbarque { get; set; }
        public string Planta { get; set; }
        public string Moeda { get; set; }
        public DateTime DataDocumento { get; set; }

        public Decimal ValorME { get; set; }
        public string Observacoes { get; set; }
        public string Despacho { get; set; }
        public string DataEmbarque { get; set; }
        public int TrackingStatusID { get; set; }
        public string Status { get; set; }        
        public string StatusDocumentoInvoice { get; set; }
        public int IdStatusDocumentoInvoice { get; set; }
        public string StatusDocumentoEmbarque { get; set; }
        public int IdStatusDocumentoEmbarque { get; set; }
        public string StatusCarta { get; set; }
        public int LetterDocStatusID { get; set; }
        public string Situacao { get; set; }
        public int PaymentStateID { get; set; }
        public string InvoiceFileName { get; set; }
        public string DocTransporteFileName { get; set; }
        public string LetterDocFileName { get; set; }
        public string Message { get; set; }
        public int MessageID { get; set; }

        public List<Document> Documents;



        public Document GetDocument(int documentTypeID)
        {
            this.LoadDocuments();            
            return this.Documents.Find(item => item.DocumentTypeID == documentTypeID);
        }

        public void LoadDocuments()
        {
            if (this.Documents == null || this.Documents.Count == 0)
            {
                this.Documents = PaymentItemService.LoadPaymentInProgressDocuments(this.Id,
                    Lear.RpaMS.AppLayer.AppSettings.Instance.CurrentProject.Id);
            }
        }
        
    }
}
