﻿using Lear.RpaMS.Data.DataServiceLayer.Document;
using Lear.RpaMS.Model.ModelLayer.RPA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionData
{
    public class DocumentManager
    {

        private Config.ConfigManager config = null;

        public DocumentManager(Config.ConfigManager config)
        {
            this.config = config;
        }

        /// <summary>
        /// Create a new document or get one already created
        /// </summary>
        /// <param name="item"></param>
        /// <param name="fileName"></param>
        /// <param name="documentType"></param>
        /// <returns></returns>
        public Document CreateDocument(
            TransactionItem item,
            string fileName,
            int documentType)
        {
            Document document = new Document();
            document.Name = System.IO.Path.GetFileName(fileName);
            document.FullName = fileName;
            document.FullNameInsert = fileName;
            document.DocumentTypeID = documentType;            
            document.Id = DocumentService.GetOrCreateDocument(document);
            return document;
        }


    }
}
