﻿namespace TransactionData
{
    public static class State
    {
        public static readonly string WaitingShipmmentNo = "Aguardando numero do despacho";
        public static readonly string WaitingDocumentDownload = "Aguardando download";
        public static readonly string DownloadOk = "Download realizado com sucesso";
        public static readonly string CheckManually = "Verificar Manualmente";
        public static readonly string WaitingBankDocument = "Aguardando documento para banco";
        public static readonly string DocumentCreated = "Documento gerado com sucesso";
        public static readonly string DocumentSentToBank = "Documento enviado para banco com sucesso";

        public static readonly int WaitingShipmmentNoID = 1;
        public static readonly int WaitingDocumentDownloadID = 2;
        public static readonly int DownloadOkID = 3;
        public static readonly int CheckManuallyID = 4;
        public static readonly int WaitingBankDocumentID = 5;
        public static readonly int DocumentCreatedID = 6;
        public static readonly int DocumentSentToBankID = 7;

    }
}
